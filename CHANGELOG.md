# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.6.0](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/compare/v1.5.0...v1.6.0) (2023-05-23)


### Features

* Add external id in import from ENA ([6a9e880](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/6a9e880c7f64cb07b1c448ab7649671b8dc6a4c9))
* connect zenodo ([5a2e82d](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/5a2e82d0002a69e3557bc78ad64e34e3ca8e6a53))
* Create an OPIDoR plan ([7d13ae1](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/7d13ae11a2806c50d2469779665b71f4188843f2))
* Create tool app ([7014d97](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/7014d979412b5c234c44272253e04a157a40474c))
* Databrokering dashboard ([9272852](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/927285250dc27d8675fe2a3de943180caea99d85))
* Note and todo list for isa elements ([87b2e9c](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/87b2e9c94195bfb1d6f3faf05726fe912a9095f4))
* taxon & scientific name checkers ([02021eb](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/02021ebf5915ede4231e84499bd92d7fd548adc1))
* unit gestion for ENA and update submitted data ([def66f9](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/def66f91e2e123ae0c1053dc421a14675779e25d))

## [1.5.0](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/compare/v1.4.0...v1.5.0) (2023-04-11)


### Features

* Creat FAQ app ([766c47c](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/766c47cdb3c0e41ea2cecbc3ea7f8e663dcdf617))
* Create a databrokering app ([4541c3d](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/4541c3d94ee83f804a702fe16e86bad0750b5fad))
* create run and experiment file ([8029a1b](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/8029a1bfc3812dfc01923089c251ef7b70b017e1))
* Get, set and delete vault secret ([b1f7438](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/b1f74383a583fc9e096a69bd9de58e75a59fd211))
* Import from OPIDoR ([a14772a](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/a14772ae37e2397a6098c7496dc3be8f1875165d))
* init async task with celery ([f7e9694](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/f7e96944062ae9fe3d6f5cf818a80c0d664ddcf1))
* Metadata checker ([6890307](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/689030749d0b787f54629653f6d996eeb87a565a))
* Send sample to ENA ([92410b2](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/92410b2cac65a2093af201260ceb60c0282ff245))
* Vault init, get and set secreat ([8a4603d](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/8a4603dbe529e7417b817153025cf5e70b56b62a))
* web message ([db6ab6f](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/db6ab6fd31a6678eff1ba131140d181b172330a6))

## [1.4.0](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/compare/v1.3.0...v1.4.0) (2023-03-17)


### Features

* Import ENA metadata ([752f43c](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/752f43c9e6b6397c54933b0612beaa2d996a5d9a))

## [1.3.0](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/compare/v1.2.0...v1.3.0) (2023-03-15)


### Features

* Create ENA app ([b3db203](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/b3db203daabc446c387a87ee74433b727efbd683))


### Bug Fixes

* creation datafile ([1b9419c](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/1b9419ceca03a414c7ed6cb3981d9c73677f0cb9))

## [1.2.0](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/compare/v1.1.0...v1.2.0) (2023-03-14)


### Features

* Add CI actions in main branch: test, coverage, build ([c7b47f1](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/c7b47f1d239ec3334e8b4a6b7a82df820698b21f))


### Bug Fixes

* Change docker compose name in CI ([6b5bd40](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/6b5bd405e9a76578f98921b5c51bc83d25ac593b))

## [1.1.0](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/compare/v1.0.0...v1.1.0) (2023-03-08)


### Features

* JWT authentification ([7d6afbe](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/7d6afbe521b630f000da013a957bb68dc1b8af5b))

## 1.0.0 (2023-03-06)


### Features

* Init API project ([d891bc3](https://gitlab.com/ifb-elixirfr/fair/metark/metark-api/commit/d891bc30062c50ed770b72bd6bcb6b5e922564d5))

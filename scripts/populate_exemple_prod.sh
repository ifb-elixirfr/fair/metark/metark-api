#!/bin/sh

docker-compose -f docker-compose.prod.yml exec backend python manage.py migrate
# docker-compose -f docker-compose.prod.yml exec web python manage.py load_countries
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_init_vault
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_app_users
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_library
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_investigation
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_investigation_contact
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_GISAID
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_ENA
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_opidor
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_cluster_ifb
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_zenodo
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_isa_json
docker-compose -f docker-compose.prod.yml exec backend python manage.py import_faq
docker-compose -f docker-compose.prod.yml exec backend python manage.py load_about
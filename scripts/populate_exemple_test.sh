#!/bin/sh

docker-compose exec backend python manage.py migrate
# docker-compose exec web python manage.py load_countries
docker-compose exec backend python manage.py load_init_vault
docker-compose exec backend python manage.py load_app_users
docker-compose exec backend python manage.py load_library
docker-compose exec backend python manage.py load_investigation
docker-compose exec backend python manage.py load_investigation_contact
docker-compose exec backend python manage.py load_GISAID
docker-compose exec backend python manage.py load_ENA
docker-compose exec backend python manage.py load_opidor
docker-compose exec backend python manage.py load_cluster_ifb
docker-compose exec backend python manage.py load_zenodo
docker-compose exec backend python manage.py load_isa_json
docker-compose exec backend python manage.py import_faq
docker-compose exec backend python manage.py load_about
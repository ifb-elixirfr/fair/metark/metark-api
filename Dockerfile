# pull official base image
FROM python:3.10


# create directory for the app user
RUN mkdir -p /home/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
# create the appropriate directories
ENV HOME=/home/app
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/staticfiles
RUN mkdir $APP_HOME/mediafiles
WORKDIR $APP_HOME

# install dependencies

RUN apt-get update && apt-get install -y netcat graphviz graphviz-dev
RUN apt-get install -y libsasl2-dev python-dev libldap2-dev libssl-dev
RUN apt-get install -y latexmk texlive-latex-extra texlive-lang-french

# Install requirements
COPY requirements.txt $APP_HOME
RUN pip install -r requirements.txt

# copy project
COPY . $APP_HOME

# Generate doc

# RUN cd /home/app/web/docs && make latexpdf && cp /home/app/web/docs/build/latex/metark-api.pdf /home/app/web/docs/source/_static/pdf
# RUN cd /home/app/web/docs && make epub && cp /home/app/web/docs/build/epub/metark-api.epub /home/app/web/docs/source/_static/epub
RUN cd /home/app/web/docs && make html

# run entrypoint.sh
ENTRYPOINT ["/home/app/web/entrypoint.sh"]
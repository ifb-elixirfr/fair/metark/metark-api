from django.urls import path

from apps.data_brokering.apis import SubmissionMessageManagement, \
    ExternalIDManagement, SubmissionManagement

urlpatterns = [
    # ======================
    # Submission message
    # ======================
    path('api/submission/of/study/<slug:pk>/message/list/',
         SubmissionMessageManagement.as_view({'get': 'list_from_study'}),
         name="SubmissionMessageManagement"),

    # ======================
    # External ID
    # ======================
    path('api/external/ID/for/<str:model_name>/<str:id>/',
         ExternalIDManagement.as_view({
             'get': 'retrieve', }),
         name="ExternalIDManagement"),

    path('api/external/ID/from/investigation/<str:id>/',
         ExternalIDManagement.as_view({
             'get': 'retrieve_from_inv', }),
         name="ExternalIDManagement"),

    # ======================
    # Submission
    # ======================
    path('api/submission/create/',
         SubmissionManagement.as_view({'post': 'create'}),
         name="SubmissionManagement"),

    path('api/submission/update/',
         SubmissionManagement.as_view({'put': 'update'}),
         name="SubmissionManagement"),

    path('api/submission/get/list/by/<str:study_id>/',
         SubmissionManagement.as_view({'get': 'get_list_external_id_by_study'}
                                      ),
         name="SubmissionManagement"),

    path('api/submission/sample/for/study/'
         '<str:study_id>/in/<str:db>/',
         SubmissionManagement.as_view({
             'get': 'get_list_assay_submission_status_stats', }),
         name="SubmissionManagement"),

    path('api/submission/sample/<str:status>/for/study/'
         '<str:study_id>/in/<str:db>/',
         SubmissionManagement.as_view({
             'get': 'get_list_assay_submission_status', }),
         name="SubmissionManagement"),




]

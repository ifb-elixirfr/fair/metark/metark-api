from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.conf import settings
from django.utils import timezone

from apps.metadata.models import Database


class Submission(models.Model):
    STATUS = (
        ("pending", "pending"),
        ("to submit", "to submit"),
        ("to update", "to update"),
        ("in progress", "in progress"),
        ("submitted", "submitted"),
        ("failed", "failed"),
    )

    content_type = models.ForeignKey(
        ContentType, on_delete=models.CASCADE,
        related_name="content_type_submission_status")

    object_id = models.TextField()

    content_object = GenericForeignKey('content_type', 'object_id')

    db = models.ForeignKey(Database, on_delete=models.CASCADE)

    status = models.CharField(
        max_length=100,
        choices=STATUS,
        default="",
    )

    creation_date = models.DateTimeField(default=timezone.now,
                                         help_text="Created date")

    submission_date = models.DateTimeField(default=None,
                                           help_text="Submission date",
                                           blank=True, null=True)

    last_update_date = models.DateTimeField(default=None,
                                            help_text="Submission date",
                                            blank=True, null=True)

    assigned_to = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        help_text="Data submitter",
        blank=True, null=True,
        related_name='assigned_to'
    )

    external_id = models.JSONField(help_text="External ID",
                                   blank=True, null=True)

    class Meta:
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]


class SubmissionMessage(models.Model):
    MESSAGE_TYPE = (
        ("comment", "comment"),
        ("create", "create"),
        ("open", "open"),
        ("close", "close"),
        ("assign", "assign"),
        ("submission", "submission"),
        ("submission_results", "submission_results"),
        ("modification", "modification"),
    )

    sub = models.ForeignKey(Submission, on_delete=models.CASCADE)

    date = models.DateTimeField(default=timezone.now,
                                help_text="Date du commentaire")

    message = models.JSONField(help_text="Project notepad",
                               blank=True,
                               null=True,
                               )
    message_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        help_text="Commentator",
        blank=True,
        null=True,
    )

    message_type = models.CharField(
        max_length=200,
        choices=MESSAGE_TYPE,
        default="",
    )


class ExternalID(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     related_name="content_type_external_id")
    object_id = models.TextField()
    content_object = GenericForeignKey('content_type', 'object_id')
    db = models.ForeignKey(Database, on_delete=models.CASCADE)
    external_id = models.JSONField(help_text="External ID",
                                   blank=True,
                                   null=True, )

    class Meta:
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]

import json
from datetime import datetime

from django.db.models import Count, DateField
from django.db.models.functions import Cast
from django.http import JsonResponse, Http404
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.exceptions import ParseError

from django.contrib.contenttypes.models import ContentType

from apps.data_brokering.models import SubmissionMessage, ExternalID, \
    Submission
from apps.data_brokering.serializers import SubmissionMessageSerializer, \
    ExternalIDSerializer, SubmissionSerializer, SubmissionCreateSerializer
from apps.isa.models import Study, Investigation, Assay, DataFile
from apps.isa.serializers import AssaySerializerResume
from apps.metadata.apis import check_completness
from apps.metadata.models import Database, Metadata, Field


@extend_schema(
    operation_id="api_submission_messages",
    description="Manage protocol",
    tags=["Data brokering"],
)
@permission_classes([IsAuthenticated])
class SubmissionMessageManagement(ModelViewSet):
    serializer_class = SubmissionMessageSerializer
    queryset = SubmissionMessage.objects.all()

    def list_from_study(self, request, pk):
        q = get_object_or_404(Study, pk=pk)
        serializer = SubmissionMessageSerializer(
            SubmissionMessage.objects.filter(
                sub__content_type__model="study",
                sub__object_id=str(q.pk)
            ).order_by('date'),
            many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id="api_external_id",
    description="External ID",
    tags=["Data brokering"],
)
@permission_classes([IsAuthenticated])
class ExternalIDManagement(ModelViewSet):
    serializer_class = ExternalIDSerializer
    queryset = ExternalID.objects.all()

    def retrieve(self, request, model_name, id):
        item = ExternalID.objects.filter(content_type__model=model_name,
                                         object_id=id)
        serializer = ExternalIDSerializer(item, many=True)
        return Response(serializer.data)

    def retrieve_from_inv(self, request, id):
        inv = get_object_or_404(Investigation, pk=id)
        item = ExternalID.objects.filter(
            content_type__model="investigation",
            object_id=str(inv.pk)) | ExternalID.objects.filter(
            content_type__model="study", object_id__in=list(
                inv.study_set.all().values_list("pk", flat=True)))
        serializer = ExternalIDSerializer(item, many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id="api_external_id",
    description="External ID",
    tags=["Data brokering"],
)
@permission_classes([IsAuthenticated])
class SubmissionManagement(ModelViewSet):
    serializer_class = SubmissionSerializer
    queryset = Submission.objects.all()

    def get_list_external_id_by_study(self, request, study_id):
        study = get_object_or_404(Study, pk=study_id)
        ss = Submission.objects.filter(content_type__model="study",
                                       object_id=str(study.pk))
        sa = Submission.objects.filter(content_type__model="assay",
                                       object_id__in=list(Assay.objects.filter(
                                           study=study).values_list("pk",
                                                                    flat=True))
                                       )
        sdf = Submission.objects.filter(content_type__model="datafile",
                                        object_id__in=list(
                                           DataFile.objects
                                           .filter(assay__study=study)
                                           .values_list('pk', flat=True)))

        matches = ss | sa | sdf

        serializer = SubmissionSerializer(matches, many=True)
        return Response(serializer.data)

    def get_list_assay_submission_status_stats(self, request, study_id, db):
        q = get_object_or_404(Study, pk=study_id)
        assay = Assay.objects.filter(study=q)
        db = Database.objects.get(name=db)

        Submission.objects.filter(
            content_type__model="study",
            object_id=str(q.pk),
            db=db
        )

        sub_assays = Submission.objects.filter(
            content_type__model="assay",
            object_id__in=list(assay.values_list("pk", flat=True)),
            db=db
        )

        data = dict()
        data["status"] = list()
        data["status"].append({
            "value": Assay.objects.filter(
                study=q,
                pk__in=list(
                    sub_assays.filter(status='submitted')
                              .values_list("object_id", flat=True))
            ).count(),
            "name": "Submitted"
        })

        data["status"].append({
            "value": Assay.objects.filter(
                study=q,
                pk__in=list(
                    sub_assays.filter(status='to submit')
                              .values_list("object_id", flat=True))
            ).count(),
            "name": "To submit"
        })

        data["status"].append({
            "value": Assay.objects.filter(study=q).exclude(
                pk__in=list(
                    sub_assays.values_list("object_id", flat=True))
            ).count(),
            "name": "Pending"
        })

        temp = dict()
        qs = list(
            sub_assays.exclude(submission_date__isnull=True)
            .annotate(submissionDate=Cast('submission_date', DateField()))
            .values('submissionDate', "submission_date__year").annotate(
                total=Count('submissionDate')).order_by('submissionDate'))

        max = 0
        for q in qs:
            if q['submission_date__year'] not in temp.keys():
                temp[q['submission_date__year']] = list()
            temp[q['submission_date__year']].append([
                q['submissionDate'], q['total']])

            if q['total'] > max:
                max = q['total']

        data["submission date"] = temp
        data["maxSubmission"] = max

        data["year"] = list(
            sub_assays.exclude(submission_date__isnull=True).
            values_list("submission_date__year", flat=True).distinct()
        )

        data['divHeight'] = 140 + 120 * len(data["year"])

        return JsonResponse(data, safe=False)

    def get_list_assay_submission_status(self, request, study_id, status, db):
        if status not in ["submitted", "to_submit", "pending", "to_update"]:
            raise ParseError('status is not "submitted","to_submit", '
                             '"to_update" or "pending"')
        q = get_object_or_404(Study, pk=study_id)
        assay = Assay.objects.filter(study=q)
        db = Database.objects.get(name=db)

        Submission.objects.filter(
            content_type__model="study",
            object_id=str(q.pk),
            db=db
        )

        sub_assays = Submission.objects.filter(
            content_type__model="assay",
            object_id__in=list(assay.values_list("pk", flat=True)),
            db=db
        )

        if status == "submitted":
            sub_assays_extract = Assay.objects.filter(
                study=q,
                pk__in=list(
                    sub_assays.filter(status='submitted')
                              .values_list("object_id", flat=True)
                )
            )
        elif status == "to_submit":
            sub_assays_extract = Assay.objects.filter(
                study=q,
                pk__in=list(
                    sub_assays.filter(status='to submit')
                              .values_list("object_id", flat=True)
                )
            )
        elif status == "to_update":
            sub_assays_extract = Assay.objects.filter(
                study=q,
                pk__in=list(
                    sub_assays.filter(status='to update')
                              .values_list("object_id", flat=True)
                )
            )
        elif status == "pending":
            sub_assays_extract = Assay.objects.filter(study=q).exclude(
                pk__in=list(
                    sub_assays.exclude(status="pending").
                    values_list("object_id", flat=True)
                )
            )

        serializer = AssaySerializerResume(sub_assays_extract, many=True)
        return JsonResponse(serializer.data, safe=False)

    def retrieve(self, request, model_name, id, db_name):
        db = get_object_or_404(Database, name=db_name)
        try:
            item = Submission.objects.get(
                content_type__model=model_name,
                object_id=id,
                db=db)
        except Submission.DoesNotExist:
            return Http404
        serializer = SubmissionSerializer(item)
        return Response(serializer.data)

    def update(self, request):
        data = JSONParser().parse(request)

        if 'headers' in data:  # pragma: no cover
            del data['headers']

        db = get_object_or_404(Database, name=data['db'])

        if data['status'] == "pending":
            arrival_status = "pending"
            departure_status = "to submit"
        elif data['status'] == "to update":
            arrival_status = "to update"
            departure_status = "submitted"
        else:
            raise ParseError("error status: 'pending' or 'to update'")

        m_original_departure = Metadata.objects.get(
            study=data['study_pk'], checklist__database=db,
            status=departure_status)

        if isinstance(m_original_departure.table, str):
            table_departure = json.loads(m_original_departure.table)
        else:
            table_departure = m_original_departure.table

        try:
            m_original_arrival = Metadata.objects.get(
                study=data['study_pk'], checklist__database=db,
                status=arrival_status)
        except Metadata.DoesNotExist:
            m_original_arrival = Metadata.objects.create(
                checklist=m_original_departure.checklist,
                study=Study.objects.get(pk=data['study_pk']),
                created_by=request.user.username,
                last_modification_by=request.user.username,
                status=arrival_status
            )

        if m_original_arrival.table is None or m_original_arrival.table == "":
            m_original_arrival.table = dict()

        if isinstance(m_original_arrival.table, str):
            table_arrival = json.loads(m_original_arrival.table)
        else:
            table_arrival = m_original_arrival.table

        fields = Field.objects.filter(
            checklist=m_original_arrival.checklist)

        if m_original_arrival.checklist.database.name == "ENA":
            fields = fields | Field.objects.filter(
                checklist__name="MIP_ENA")

        for id in data['assays_id']:
            sub = Submission.objects.get(content_type__model="assay",
                                         db=db, object_id=id,
                                         status=departure_status)
            sub.status = arrival_status
            sub.last_update_date = datetime.now()
            sub.save()

            table_arrival[str(id)] = table_departure[str(id)]

            del table_departure[str(id)]

        m_original_arrival.table = table_arrival
        check_result = check_completness(table_arrival, fields)
        m_original_arrival.mandatory = check_result['mandatory']
        m_original_arrival.recommended = check_result['recommended']
        m_original_arrival.optional = check_result['optional']
        m_original_arrival.save()

        m_original_departure.table = table_departure
        check_result = check_completness(table_departure, fields)
        m_original_departure.mandatory = check_result['mandatory']
        m_original_departure.recommended = check_result['recommended']
        m_original_departure.optional = check_result['optional']
        m_original_departure.save()

        return JsonResponse({'message': 'Done'}, status=200)

    def create(self, request):
        data = JSONParser().parse(request)

        if 'headers' in data:  # pragma: no cover
            del data['headers']

        db = Database.objects.get(name=data['db'])
        m_original = Metadata.objects.get(study__pk=data['study_pk'],
                                          checklist__database=db,
                                          status="pending")

        if isinstance(m_original.table, str):
            table = json.loads(m_original.table)
        else:
            table = m_original.table

        for id in data['assays_id']:
            assay = get_object_or_404(Assay, pk=id)
            try:
                sub = Submission.objects.get(content_type__model="assay",
                                             db=db, object_id=id)
                sub.status = "to submit"
                sub.last_update_date = datetime.now()
                sub.save()
            except Submission.DoesNotExist:
                inter = dict()
                inter['content_type'] = ContentType.objects.get(
                    model="assay").pk
                inter['object_id'] = assay.pk
                inter['content_object'] = assay
                inter['db'] = db.pk
                inter['creation_date'] = datetime.now()
                inter['assigned_to'] = request.user.pk
                inter['status'] = "to submit"

                serializer = SubmissionCreateSerializer(data=inter)

                if serializer.is_valid():
                    serializer.save()
                else:
                    print(serializer.errors)
                    return JsonResponse(serializer.errors, status=400)

            try:
                m = Metadata.objects.get(study=assay.study,
                                         checklist__database=db,
                                         status="to submit")
            except Metadata.DoesNotExist:
                m = Metadata.objects.create(
                    checklist=m_original.checklist,
                    study=assay.study,
                    created_by=request.user.username,
                    last_modification_by=request.user.username,
                    status="to submit"
                )

            fields = Field.objects.filter(
                checklist=m.checklist)

            if m.checklist.database.name == "ENA":
                fields = fields | Field.objects.filter(
                    checklist__name="MIP_ENA")

            if m.table is None or m.table == "":
                m.table = dict()

            if isinstance(m.table, str):
                temp = json.loads(m.table)
            else:
                temp = m.table

            temp[str(assay.pk)] = table[str(assay.pk)]
            m.table = temp

            check_result = check_completness(temp, fields)
            m.mandatory = check_result['mandatory']
            m.recommended = check_result['recommended']
            m.optional = check_result['optional']
            m.save()

            del table[str(assay.pk)]
            m_original.table = table
            check_result = check_completness(table, fields)
            m_original.mandatory = check_result['mandatory']
            m_original.recommended = check_result['recommended']
            m_original.optional = check_result['optional']
            m_original.save()

        return JsonResponse({'message': 'Done'}, status=200)

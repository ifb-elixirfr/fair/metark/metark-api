from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from apps.data_brokering.models import SubmissionMessage, ExternalID, \
    Submission


class SubmissionCreateSerializer(ModelSerializer):
    class Meta:
        model = Submission
        fields = "__all__"


class SubmissionSerializer(ModelSerializer):
    db = SerializerMethodField()
    model = SerializerMethodField()

    class Meta:
        model = Submission
        fields = "__all__"

    def get_db(self, obj):
        return obj.db.name

    def get_model(self, obj):
        return obj.content_type.model


class SubmissionMessageSerializer(ModelSerializer):
    message_type = SerializerMethodField()
    message_by = SerializerMethodField()

    class Meta:
        model = SubmissionMessage
        fields = "__all__"

    def get_message_type(self, obj):
        return obj.get_message_type_display()

    def get_message_by(self, obj):
        return obj.message_by.username


class ExternalIDSerializer(ModelSerializer):
    db = SerializerMethodField()

    class Meta:
        model = ExternalID
        fields = "__all__"

    def get_db(self, obj):
        return obj.db.name

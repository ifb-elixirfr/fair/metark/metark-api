from django.contrib import admin

from apps.data_brokering.models import SubmissionMessage, ExternalID, \
    Submission


@admin.register(SubmissionMessage)
class submission_messageAdmin(admin.ModelAdmin):
    fs = [
        f.name for f in SubmissionMessage._meta.fields if f.name != "id"
    ]
    list_display = fs

    # Disbale add functionality
    def has_add_permission(self, request):
        return False


@admin.register(Submission)
class submissionAdmin(admin.ModelAdmin):
    fs = [
        f.name for f in Submission._meta.fields if f.name != "id"
    ]
    list_display = fs

    # Disbale add functionality
    def has_add_permission(self, request):
        return False


@admin.register(ExternalID)
class ExternalIDAdmin(admin.ModelAdmin):
    fs = [
        f.name for f in ExternalID._meta.fields if f.name != "id"
    ]
    list_display = fs

    # Disbale add functionality
    def has_add_permission(self, request):
        return False

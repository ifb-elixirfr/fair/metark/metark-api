from django.apps import AppConfig


class DataBrokeringConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.data_brokering"

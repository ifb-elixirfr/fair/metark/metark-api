# Generated by Django 4.1.5 on 2023-05-02 09:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        (
            "data_brokering",
            "0002_rename_creation_pending_submission_creation_date_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="submission",
            name="external_id",
            field=models.JSONField(blank=True, help_text="External ID", null=True),
        ),
        migrations.AlterField(
            model_name="submission",
            name="status",
            field=models.CharField(
                choices=[
                    ("pending", "pending"),
                    ("to submit", "to submit"),
                    ("in progress", "in progress"),
                    ("submitted", "submitted"),
                    ("failed", "failed"),
                ],
                default="",
                max_length=100,
            ),
        ),
    ]

import json

import hvac
from django.contrib.auth.models import User, Group
from django.http import JsonResponse, HttpResponse
from drf_spectacular.types import OpenApiTypes

from drf_spectacular.utils import extend_schema, OpenApiResponse
from rest_framework import viewsets, permissions
from rest_framework.decorators import permission_classes, api_view
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.permissions import BasePermission, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.isa.serializers import UserSerializer
from apps.users.models import Profile
from apps.users.serializers import GroupSerializer, UserSerializerInfo, \
    RegisterSerializer

from knox.views import LoginView as KnoxLoginView
from apps.users.utils import JWTAuthentication
from rest_framework.exceptions import ParseError


class IsSuperUser(BasePermission):
    """
    API permission : test if super_user
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class LoginView(KnoxLoginView):
    authentication_classes = [JWTAuthentication]

    def get_post_response_data(self, request, token, instance):
        UserSerializer = self.get_user_serializer_class()

        # Vault
        u = User.objects.get(username=request.user)
        p, created = Profile.objects.get_or_create(
            user=u)

        client = hvac.Client(url='http://vault:8200')
        if client.sys.is_sealed():
            with open('vault_id.json', 'r') as openfile:
                result = json.load(openfile)

            keys = result['keys']
            client.sys.submit_unseal_keys(keys)

        client = hvac.Client(url='http://vault:8200')

        clientInfo = client.auth.userpass.login(
            username=u.username,
            password=u.password.split("$")[-1],
        )

        p.vault_id = clientInfo['auth']['entity_id']
        p.save()

        vaultToken = clientInfo['auth']['client_token']
        data = {
            'expiry': self.format_expiry_datetime(instance.expiry),
            'token': token,
            'vaultToken': vaultToken
        }

        if UserSerializer is not None:
            data["user"] = UserSerializer(
                request.user,
                context=self.get_context()
            ).data
        return data


@permission_classes([IsAuthenticated])
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


@extend_schema(
    operation_id="api_create_user",
    description="Manage User",
    tags=["User"],
)
class RegisterManagement(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer

    def create(self, request):
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = RegisterSerializer(data=data)
        if serializer.is_valid():
            user = serializer.save()

            # Create vault entry
            client = hvac.Client(url='http://vault:8200')
            with open('vault_id.json', 'r') as openfile:
                result = json.load(openfile)

            if client.sys.is_sealed():
                keys = result['keys']
                client.sys.submit_unseal_keys(keys)
            else:
                root_token = result['root_token']
                client = hvac.Client(url='http://vault:8200',
                                     token=root_token)

            client.auth.userpass.create_or_update_user(
                username=user.username,
                password=user.password.split("$")[-1],
                policies="secret-writer"
            )

            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)


@extend_schema(
    operation_id="api_manage_User",
    description="Manage User",
    tags=["User"],
)
class UserManagement(ModelViewSet):
    serializer_class = UserSerializerInfo
    queryset = User.objects.all()

    def retrieve(self, request, username):
        q = get_object_or_404(User, username=username)
        serializer = UserSerializerInfo(q)
        return Response(serializer.data)


#
# @extend_schema(
#     operation_id="api_autocomplete_username",
#     description="Autocomplete API username",
#     tags=["Users"],
#     responses={
#         200: OpenApiResponse(
#             response=OpenApiTypes.OBJECT, description="Data in json format."
#         ),
#         404: OpenApiResponse(description="No results found."),
#     },
# )
# @csrf_exempt
# @api_view(['GET'])
# def api_autocomplete_username(request):
#     """
#     GET username list
#     """
#
#     q = request.GET.get('query', '')
#     result = list(User.objects.filter(
#         username__icontains=q).values_list('username',
#                                            flat=True))
#     data = json.dumps({"query": "query", "suggestions": result},
#                       cls=DjangoJSONEncoder)
#
#     return JsonResponse(data, safe=False)
#
#
# class LogoutView(APIView):
#     def get(self, request):
#         try:
#             token = RefreshToken.for_user(request.user)
#             token.blacklist()
#             django_logout(request)
#             return Response(status=status.HTTP_205_RESET_CONTENT)
#         except Exception:
#             return Response(status=status.HTTP_400_BAD_REQUEST)


@extend_schema(
    operation_id='get_vault_secret',
    description="Get vault secret",
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'secretName': {
                    'description': 'secretName',
                    'type': 'str',
                    'required': 'true',
                },
                'vaultToken': {
                    'description': 'vaultToken',
                    'type': 'str',
                    'required': 'true',
                },
            },
        },
    },
    tags=["VAULT"],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def get_vault_secret(request):
    """SUMMARY LINE

    Get vault secret
    """
    if request.method == 'POST':
        if 'vaultToken' not in request.data:
            raise ParseError("Empty secretName")
        if 'secretName' not in request.data:
            raise ParseError("Empty vaultToken")

        client = hvac.Client(url='http://vault:8200',
                             token=request.data["vaultToken"])

        p = get_object_or_404(Profile, user=request.user)

        try:
            read_response = client.secrets.kv.read_secret_version(
                path=p.vault_id + "/" + request.data["secretName"])
            return JsonResponse(read_response)
        except Exception:
            return HttpResponse(status=404)


@extend_schema(
    operation_id='set_vault_secret',
    description="Set vault secret",
    tags=["VAULT"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'secretName': {
                    'description': 'secretName',
                    'type': 'str',
                    'required': 'true',
                },
                'vaultToken': {
                    'description': 'vaultToken',
                    'type': 'str',
                    'required': 'true',
                },
                'secretContent': {
                    'description': 'secretContent',
                    'type': 'str',
                    'required': 'true',
                },
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def set_vault_secret(request):
    """SUMMARY LINE

    Set vault secret
    """
    if request.method == 'POST':
        if 'vaultToken' not in request.data:
            raise ParseError("Empty vaultToken")
        if 'secretName' not in request.data:
            raise ParseError("Empty secretName")
        if 'secretContent' not in request.data:
            raise ParseError("Empty secretContent")

        try:
            secretContent = json.loads(request.data["secretContent"])
        except Exception:
            secretContent = request.data["secretContent"]

        client = hvac.Client(url='http://vault:8200',
                             token=request.data["vaultToken"])

        p = get_object_or_404(Profile, user=request.user)

        try:
            client.secrets.kv.create_or_update_secret(
                path=p.vault_id + "/" + request.data["secretName"],
                secret=secretContent)
            return HttpResponse(status=204)
        except Exception as e:
            print(e)
            return HttpResponse(status=400)


@extend_schema(
    operation_id='list_vault_secret',
    description="List vault secret",
    tags=["VAULT"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'vaultToken': {
                    'description': 'vaultToken',
                    'type': 'str',
                    'required': 'true',
                }
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def list_vault_secret(request):
    """SUMMARY LINE

    Set vault secret
    """
    if request.method == 'POST':
        if 'vaultToken' not in request.data:
            raise ParseError("Empty vaultToken")

        client = hvac.Client(url='http://vault:8200',
                             token=request.data["vaultToken"])

        p = get_object_or_404(Profile, user=request.user)

        try:
            list_secrets_result = client.secrets.kv.v1.list_secrets(
                path='data/' + p.vault_id)
            return JsonResponse(list_secrets_result)
        except Exception:
            return HttpResponse(status=404)


@extend_schema(
    operation_id='delete_vault_secret',
    description="Delete vault secret",
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'secretName': {
                    'description': 'secretName',
                    'type': 'str',
                    'required': 'true',
                },
                'vaultToken': {
                    'description': 'vaultToken',
                    'type': 'str',
                    'required': 'true',
                },
            },
        },
    },
    tags=["VAULT"],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def delete_vault_secret(request):
    """SUMMARY LINE

    Delete vault secret
    """
    if request.method == 'POST':
        if 'vaultToken' not in request.data:
            raise ParseError("Empty secretName")
        if 'secretName' not in request.data:
            raise ParseError("Empty vaultToken")

        client = hvac.Client(url='http://vault:8200',
                             token=request.data["vaultToken"])

        p = get_object_or_404(Profile, user=request.user)

        try:
            client.secrets.kv.v1.delete_secret(
                path='data/' + p.vault_id + "/" + request.data["secretName"])
            return HttpResponse(status=202)
        except Exception:
            return HttpResponse(status=404)

import json

import hvac

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Init vault'

    def handle(self, *args, **kwargs):
        client = hvac.Client(url='http://vault:8200')
        if not client.sys.is_initialized():
            shares = 5
            threshold = 3

            result = client.sys.initialize(shares, threshold)

            keys = result['keys']
            client.sys.submit_unseal_keys(keys)

            # Serializing json
            json_object = json.dumps(result, indent=4)

            # Writing to sample.json
            with open("vault_id.json", "w") as outfile:
                outfile.write(json_object)

        with open('vault_id.json', 'r') as openfile:
            result = json.load(openfile)

        if client.sys.is_sealed():
            keys = result['keys']
            client.sys.submit_unseal_keys(keys)
        else:
            root_token = result['root_token']
            client = hvac.Client(url='http://vault:8200', token=root_token)

            client.sys.enable_auth_method(
                method_type='userpass'
            )

            client.sys.enable_secrets_engine(
                backend_type='kv',
                path='secret/data',
            )
            client.secrets.kv.configure(
                max_versions=20,
                mount_point='secret/data',
            )

            policy = """
path "sys" {
  capabilities = ["deny"]
}

path "secret/{{identity.entity.id}}/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "secret/data/{{identity.entity.id}}/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}
            """

            client.sys.create_or_update_policy(
                name='secret-writer',
                policy=policy,
            )

            client.logout()

        self.stdout.write("Done")

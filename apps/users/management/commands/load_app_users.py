import csv
import json
from distutils.util import strtobool
import hvac

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Reload regions data'

    def handle(self, *args, **kwargs):

        client = hvac.Client(url='http://vault:8200')
        with open('vault_id.json', 'r') as openfile:
            result = json.load(openfile)

        if client.sys.is_sealed():
            keys = result['keys']
            client.sys.submit_unseal_keys(keys)
        else:
            root_token = result['root_token']
            client = hvac.Client(url='http://vault:8200', token=root_token)

        with open('apps/users/static/data/applicationUsers.csv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                try:
                    u = User.objects.create_user(
                        username=row[0],
                        first_name=row[1],
                        last_name=row[2],
                        email=row[3],
                        password=row[4],
                        is_superuser=strtobool(row[5]),
                        is_staff=strtobool(row[6])
                    )

                    client.auth.userpass.create_or_update_user(
                        username=u.username,
                        password=u.password.split("$")[-1],
                        policies="secret-writer"
                    )
                except Exception as e:
                    print(e)

        self.stdout.write("Done")

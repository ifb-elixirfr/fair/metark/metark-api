from django.urls import path, include, re_path
from rest_framework import routers
from knox import views as knox_views
from rest_framework_simplejwt.views import TokenRefreshView, TokenVerifyView

from apps.users.apis import UserManagement, UserViewSet, GroupViewSet, \
    LoginView, get_vault_secret, set_vault_secret, list_vault_secret, \
    delete_vault_secret, RegisterManagement
from apps.users.views import RegisterUserAPIView, MyObtainTokenPairView

router = routers.DefaultRouter()
router.register(r"users", UserViewSet)
router.register(r"groups", GroupViewSet)

urlpatterns = [

    path("", include(router.urls)),
    re_path(r"api/login/", LoginView.as_view(),
            name="knox_login"),
    re_path(r"api/logout/", knox_views.LogoutView.as_view(),
            name="knox_logout"),
    re_path(r"api/logoutall/", knox_views.LogoutAllView.as_view(),
            name="knox_logoutall"),

    path('api/register/', RegisterUserAPIView.as_view(),
         name='RegisterUserAPIView'),

    path('api/user/<str:username>/', UserManagement.as_view(
        {'get': 'retrieve', }),
         name='UserManagement'),
    path('api/user/token/obtain/', MyObtainTokenPairView.as_view(),
         name='token_obtain_pair'),
    path('api/user/token/refresh/', TokenRefreshView.as_view(),
         name='token_refresh'),
    path('api/user/token/verify/', TokenVerifyView.as_view(),
         name='token_verify'),
    path('api/user/<str:username>/get/profile/', UserManagement.as_view(
        {'get': 'retrieve', }),
         name='UserManagement'),

    path('api/create/new/user/', RegisterManagement.as_view(
        {'post': 'create', }),
         name='RegisterManagement'),

    path('api/get/vault/secret/', get_vault_secret,
         name='get_vault_secret'),
    path('api/set/vault/secret/', set_vault_secret,
         name='set_vault_secret'),
    path('api/list/vault/secret/', list_vault_secret,
         name='list_vault_secret'),
    path('api/delete/vault/secret/', delete_vault_secret,
         name='delete_vault_secret'),
]

from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    photo = models.ImageField(upload_to='userPhoto',
                              default='userPhoto/default.png')

    vault_id = models.CharField(max_length=100, blank=True)

    def set_vaultid(self, id):
        self.vault_id = id
        return self

    def get_vaultid(self):
        return self.vault_id

    def __str__(self):
        return str(self.user)

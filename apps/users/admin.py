from django.contrib import admin
from apps.users.models import Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """Adding the User table in the Django admin and customizing the
       rendering"""
    search_fields = ['user__username']

    # Disable add functionality
    def has_add_permission(self, request):
        return False

    # Disable delete functionality
    def has_delete_permission(self, request, obj=None):
        return False

from django.contrib.auth.models import User
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication
import jwt


class JWTAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        # validator = PyJwtVerifier(key, auto_verify=False)
        try:
            payload = jwt.decode(key, options={"verify_signature": False})
            email = payload["email"]
            # user, created = User.objects.get_or_create(email=email)
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed("user not registered")
        except Exception:
            raise exceptions.AuthenticationFailed("invalid JWT")
        return user, None

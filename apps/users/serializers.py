import json

from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from apps.users.models import Profile
import hvac


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):

        try:
            request = self.context["request"]

            p, created = Profile.objects.get_or_create(
                user=User.objects.get(username=request.data['username']))

            client = hvac.Client(url='http://vault:8200')
            if client.sys.is_sealed():
                with open('vault_id.json', 'r') as openfile:
                    result = json.load(openfile)

                keys = result['keys']
                client.sys.submit_unseal_keys(keys)

            client = hvac.Client(url='http://vault:8200')

            clientInfo = client.auth.userpass.login(
                username=request.data['username'],
                password=request.data['password'],
            )

            p.vault_id = clientInfo['auth']['entity_id']
            p.save()
        except KeyError:
            pass
        finally:
            return super().validate(attrs)

    @classmethod
    def get_token(cls, user):
        print(cls)
        token = super(MyTokenObtainPairSerializer, cls).get_token(user)

        token['username'] = user.username
        token['first_name'] = user.first_name
        token['last_name'] = user.last_name
        token['is_staff'] = user.is_staff
        token['is_superuser'] = user.is_superuser
        token['email'] = user.email
        return token


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class UserSerializerInfo(ModelSerializer):
    name = SerializerMethodField()

    class Meta:
        model = User
        fields = [
            "username",
            "name",
            "first_name",
            "last_name",
            "email",
            "is_staff",
            "is_superuser"
        ]

    def get_name(self, arg_obj):
        return arg_obj.get_full_name()


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'password2',
                  'email', 'first_name', 'last_name')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True}
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})
        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

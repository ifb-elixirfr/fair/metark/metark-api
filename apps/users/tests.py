import json

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse


class TestWebAnalytic(TestCase):
    def setUp(self):
        self.client = Client()

        User.objects.create_user(
            username='superUser',
            first_name='Super',
            last_name='User',
            email='superUser@mail.com',
            password='djangoPWD',
            is_superuser=True,
            is_staff=True
        )
        logged_in = self.client.login(username="superUser",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        url = reverse('RegisterManagement')
        response = self.client.post(url, json.dumps({
            "username": "uproject",
            "password": "djangoPWD",
            "password2": "djangoPWD",
            "email": "userProject@mail.com",
            "first_name": "User",
            "last_name": "Project"
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        u = User.objects.get(username="uproject")
        u.is_superuser = True
        u.is_staff = True
        u.save()

        self.client.logout()

    def test_user(self):
        # Login
        logged_in = self.client.login(username="uproject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        # Get TOKEN ID
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {'username': 'uproject',
                                          'password': 'djangoPWD'},
                                    format='json')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('access' in response.data)
        tokenID = response.data['access']

        # Get TOKEN ACCESS

        url = reverse('knox_login')
        badJWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NT" \
                 "Y3ODkwIiwibmFtZSI6IlRvVW5pdFRlc3QiLCJlbWFpbCI6ImVtYWlsQH" \
                 "Rlc3QuY29tIiwiaWF0IjoxNTE2MjM5MDIyfQ.vMiae_4TkXpkS-1l2Eh" \
                 "54-KL46hr3F_AMsZzN63Y8H0"
        response = self.client.post(url,
                                    HTTP_AUTHORIZATION='Token ' + badJWT)
        self.assertEqual(response.status_code, 401)

        response = self.client.post(url,
                                    HTTP_AUTHORIZATION='Token ' + "badJWT")
        self.assertEqual(response.status_code, 401)

        url = reverse('knox_login')
        response = self.client.post(url,
                                    HTTP_AUTHORIZATION='Token ' + tokenID)
        self.assertEqual(response.status_code, 200)
        token = response.data['token']
        self.client = Client(HTTP_AUTHORIZATION='Token ' + token)

        url = reverse("UserManagement",
                      kwargs={'username': 'uproject'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("RegisterUserAPIView")
        response = self.client.post(url, {
            "username": "toto",
            "password": "amazingPWD",
            "password2": "amazingPWD",
            "email": "toto@mail.com",
            "first_name": "toto",
            "last_name": "toto"
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.post(url, {
            "username": "titi",
            "password": "amazingPWD",
            "password2": "amazingPWDNotSame",
            "email": "titi@mail.com",
            "first_name": "titi",
            "last_name": "titi"
        })
        self.assertEqual(response.status_code, 400)

import hvac
import requests
from django.contrib.auth.models import User
from django.http import HttpResponse
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiResponse, \
    OpenApiParameter
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import ParseError

from apps.isa.models import Investigation
from apps.users.apis import IsSuperUser
from apps.opidor.tasks import import_opidor_plan_async, \
    export_opidor_plan_async


@extend_schema(
    operation_id='opidor_test_connection',
    description="Check OPIDoR connection",
    tags=["OPIDoR"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'email': {
                    'description': 'email',
                    'type': 'str',
                    'required': 'true',
                },
                'token': {
                    'description': 'Token',
                    'type': 'str',
                    'required': 'true',
                }
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def opidor_test_connection(request):
    """SUMMARY LINE

    Check OPIDoR connection
    """
    if request.method == 'POST':
        if 'email' not in request.data:
            raise ParseError("Empty email")
        if 'token' not in request.data:
            raise ParseError("Empty token")

        r = requests.post(
            'https://dmp.opidor.fr/api/v1/authenticate',
            json={
                "grant_type": "authorization_code",
                "email": request.data["email"],
                "code": request.data["token"]
            })

        if r.ok:
            return HttpResponse(status=200)
        else:
            return HttpResponse("Bad Webin or password", status=403)


@extend_schema(
    operation_id='import_opidor_plan',
    description="Import Opidor plan",
    tags=["OPIDoR"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'email': {
                    'description': 'email',
                    'type': 'str',
                    'required': 'true',
                },
                'token': {
                    'description': 'token',
                    'type': 'str',
                    'format': 'password',
                    'required': 'true',
                },
                'planID': {
                    'description': 'planID',
                    'type': 'string',
                    'required': 'true',
                }
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated, IsSuperUser])
def import_opidor_plan(request):
    """SUMMARY LINE

    Import Opidor plan
    """

    # Authen
    if 'email' not in request.data:
        raise ParseError("Empty email")
    if 'token' not in request.data:
        raise ParseError("Empty token")
    if 'planID' not in request.data:
        raise ParseError("Empty planID")

    response = requests.post(
        'https://dmp.opidor.fr/api/v1/authenticate',
        json={
            "grant_type": "authorization_code",
            "email": request.data["email"],
            "code": request.data["token"]
        })

    if response.ok:
        access_token = response.json()['access_token']
        task = import_opidor_plan_async.delay(
            request.data["planID"],
            access_token,
            request.user.username
        )

        content = {'Message': 'Your request has been submitted',
                   'Task_id': task.id
                   }
        return Response(content, status=status.HTTP_200_OK)

    else:
        return HttpResponse(response.text, status=404)


@extend_schema(
    operation_id='import_opidor_plan_vault',
    description="Import Opidor plan",
    tags=["OPIDoR"],
    parameters=[
        OpenApiParameter(
            name='planID',
            location=OpenApiParameter.PATH,
            description='OPIDoR plan ID',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsSuperUser])
def import_opidor_plan_vault(request, planID):
    """SUMMARY LINE

    Import Opidor plan
    """

    # Authen
    client = hvac.Client(url='http://vault:8200')
    u = User.objects.get(username=request.user.username)

    clientInfo = client.auth.userpass.login(
        username=u.username,
        password=u.password.split("$")[-1],
    )
    vault_id = clientInfo['auth']['entity_id']

    vaultToken = clientInfo['auth']['client_token']
    client = hvac.Client(url='http://vault:8200',
                         token=vaultToken)
    try:
        read_response = client.secrets.kv.read_secret_version(
            path=vault_id + "/" + 'OPIDoR')
    except Exception:
        return HttpResponse(status=404)

    email = ""
    token = ""

    for i in read_response['data']['data']:
        if i['ItemNameValue'] == "email":
            email = i['ItemValueValue']
        if i['ItemNameValue'] == "token":
            token = i['ItemValueValue']

    response = requests.post(
        'https://dmp.opidor.fr/api/v1/authenticate',
        json={
            "grant_type": "authorization_code",
            "email": email,
            "code": token
        })

    if response.ok:
        access_token = response.json()['access_token']
        task = import_opidor_plan_async.delay(
            planID,
            access_token,
            request.user.username
        )

        content = {'Message': 'Your request has been submitted',
                   'Task_id': task.id
                   }
        return Response(content, status=status.HTTP_200_OK)

    else:
        return HttpResponse(response.text, status=404)


@extend_schema(
    operation_id='export_opidor_plan_vault',
    description="Export OPIDoR plan",
    tags=["OPIDoR"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Investigation ID',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsSuperUser])
def export_opidor_plan_vault(request, id):
    """SUMMARY LINE

    Export to Opidor
    """

    # Authen
    client = hvac.Client(url='http://vault:8200')
    u = User.objects.get(username=request.user.username)

    clientInfo = client.auth.userpass.login(
        username=u.username,
        password=u.password.split("$")[-1],
    )
    vault_id = clientInfo['auth']['entity_id']

    vaultToken = clientInfo['auth']['client_token']
    client = hvac.Client(url='http://vault:8200',
                         token=vaultToken)
    try:
        read_response = client.secrets.kv.read_secret_version(
            path=vault_id + "/" + 'OPIDoR')
    except Exception:
        return HttpResponse(status=404)

    email = ""
    token = ""

    for i in read_response['data']['data']:
        if i['ItemNameValue'] == "email":
            email = i['ItemValueValue']
        if i['ItemNameValue'] == "token":
            token = i['ItemValueValue']

    response = requests.post(
        'https://dmp.opidor.fr/api/v1/authenticate',
        json={
            "grant_type": "authorization_code",
            "email": email,
            "code": token
        })

    if response.ok:
        access_token = response.json()['access_token']
        get_object_or_404(Investigation, pk=id)
        get_object_or_404(User, username=request.user.username)
        task = export_opidor_plan_async.delay(
            id,
            access_token,
            request.user.username
        )

        content = {'Message': 'Your request has been submitted',
                   'Task_id': task.id
                   }
        return Response(content, status=status.HTTP_200_OK)

    else:
        return HttpResponse(response.text, status=404)


@extend_schema(
    operation_id='get_meta_opidor_plan_vault',
    description="Import Opidor plan",
    tags=["OPIDoR"],
    parameters=[
        OpenApiParameter(
            name='planID',
            location=OpenApiParameter.PATH,
            description='OPIDoR plan ID',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsSuperUser])
def get_meta_opidor_plan_vault(request, planID):
    """SUMMARY LINE

    Get meta from Opidor plan
    """

    # Authen
    client = hvac.Client(url='http://vault:8200')
    u = User.objects.get(username=request.user.username)

    clientInfo = client.auth.userpass.login(
        username=u.username,
        password=u.password.split("$")[-1],
    )
    vault_id = clientInfo['auth']['entity_id']

    vaultToken = clientInfo['auth']['client_token']
    client = hvac.Client(url='http://vault:8200',
                         token=vaultToken)
    try:
        read_response = client.secrets.kv.read_secret_version(
            path=vault_id + "/" + 'OPIDoR')
    except Exception:
        return HttpResponse(status=404)

    email = ""
    token = ""

    for i in read_response['data']['data']:
        if i['ItemNameValue'] == "email":
            email = i['ItemValueValue']
        if i['ItemNameValue'] == "token":
            token = i['ItemValueValue']

    response = requests.post(
        'https://dmp.opidor.fr/api/v1/authenticate',
        json={
            "grant_type": "authorization_code",
            "email": email,
            "code": token
        })

    if response.ok:
        access_token = response.json()['access_token']
        hed = {'Authorization': 'Bearer ' + access_token}
        url = "https://dmp.opidor.fr/api/v1/madmp/plans/" + planID
        response = requests.get(url, headers=hed)

        if response.ok:
            json_response = response.json()
            return Response(json_response['meta'], status=status.HTTP_200_OK)
        else:
            return HttpResponse(response.text, status=404)

    else:
        return HttpResponse(response.text, status=404)

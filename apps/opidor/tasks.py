import re

from celery import shared_task
from django.contrib.auth.models import User

from apps.data_brokering.models import ExternalID
from apps.isa._functions import createToDoItemsInv
from apps.isa.models import Investigation, Funder, Funding, PersonContact, \
    Partner, ToDoList, Contact, Role
from apps.isa.serializers import InvestigationResumeSerializer

import requests

from apps.metadata.models import Database


class TaskFailure(Exception):
    pass


###############################################################################
# Import OPIDoR plan
###############################################################################


@shared_task(name='Import OPIDoR plan', track_started=True, bind=True)
def import_opidor_plan_async(self, planID, access_token, username):
    """Function to import metadata form OPIDoR panID"""

    hed = {'Authorization': 'Bearer ' + access_token}
    url = "https://dmp.opidor.fr/api/v1/madmp/plans/" + planID
    response = requests.get(url, headers=hed)

    if response.ok:
        json_response = response.json()
        CLEANR = re.compile('<.*?>')
        inv = Investigation.objects.create(
            title=(json_response['project']['title']),
            description=(re.sub(CLEANR, '',
                                json_response['project']['description'])),
            alias=(json_response['project']['acronym']),
            creationSource='OPIDoR'
        )

        user_temp = User.objects.get(username=username)
        c = Contact.objects.create(
            user=user_temp,
            corresponding_contact=True,
            role=Role.objects.get_or_create(name='All')[0])
        inv.collaborators.add(c)

        tdl = ToDoList.objects.create(
            content_object=inv
        )
        createToDoItemsInv(tdl)

        try:
            if 'funding' in json_response['project'].keys():
                for fun in json_response['project']['funding']:
                    if 'name' in fun['funder']:
                        funder = Funder.objects.create(
                            name=fun['funder']['name'],
                            funderId=fun['funder']['funderId'],
                            idType=fun['funder']['idType'],
                        )
                    elif 'custom_value' in fun['funder']:
                        funder = Funder.objects.create(
                            name=fun['funder']['custom_value'],
                        )
                    else:
                        funder = Funder.objects.create(
                            name=fun['funder']['name'],
                            funderId=fun['funder']['funderId'],
                            idType=fun['funder']['idType'],
                        )

                    funding = Funding.objects.create(
                        funder=funder,
                        fundingId=fun['grantId'],
                        fundingStatus=fun['fundingStatus'],
                    )

                    inv.funding.add(funding)
        except Exception:
            pass

        try:
            if 'partner' in json_response['project'].keys():
                for p in json_response['project']['partner']:
                    partner = Partner.objects.create(
                        name=p['name'],
                        partnerId=p['orgId'],
                        idType=p['idType'],
                    )
                    inv.partners.add(partner)
        except Exception:
            pass

        try:
            if 'contributor' in json_response.keys():
                for c in json_response['contributor']:
                    pc = PersonContact.objects.create(
                        email=c['mbox'],
                        first_name=c['firstName'],
                        last_name=c['lastName'],
                        affiliation=c['affiliationName'],
                        personContactId=c['personId'],
                        idType=c['idType'],
                        roles=';'.join(c['role']),
                    )
                    inv.contacts.add(pc)
        except Exception:
            pass

        serializer = InvestigationResumeSerializer(inv)

        ExternalID.objects.create(
            content_object=inv,
            db=Database.objects.get(name="OPIDoR"),
            external_id={'planId': planID}
        )

        return serializer.data
    else:
        raise TaskFailure(
            'An internal error occurred. Please contact support: ' +
            response.text)


###############################################################################
# Export to OPIDoR
###############################################################################


@shared_task(name='Export to OPIDoR', track_started=True, bind=True)
def export_opidor_plan_async(self, investigation_id, access_token, username):
    """Function to import metadata form OPIDoR panID"""
    investigation = Investigation.objects.get(pk=investigation_id)
    user = User.objects.get(username=username)
    hed = {'Authorization': 'Bearer ' + access_token}
    url = "https://dmp.opidor.fr/api/v1/madmp/plans/import?import_format=rda"
    response = requests.post(
        url, headers=hed,
        json={
            "dmp": {
                 "title": investigation.title,
                 "description": investigation.description,
                 "created": investigation.submission_date.strftime(
                     "%Y-%d-%mT%H:%M:%S"),
                 "project": [
                     {
                         "title": investigation.title,
                         "description": investigation.description,
                         "start": investigation.submission_date.strftime(
                             "%Y-%d-%m"),
                         "funding": [
                         ]
                     }
                 ],
                 "contact": {
                     "mbox": user.email,
                     "name": user.get_full_name()
                 },
                 "language": "eng",
                 "ethical_issues_exist": "no",
                 "dataset": [
                 ]
                }
        })

    if response.ok:
        ExternalID.objects.create(
            content_object=investigation,
            db=Database.objects.get(name="OPIDoR"),
            external_id={'planId': response.json()['id']}
        )
        return response.json()['id']
    else:
        raise TaskFailure(
            'An internal error occurred. Please contact support with this '
            'message: ' + response.text)

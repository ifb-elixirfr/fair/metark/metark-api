from django.urls import path

from apps.opidor.apis import import_opidor_plan, opidor_test_connection, \
    import_opidor_plan_vault, export_opidor_plan_vault, \
    get_meta_opidor_plan_vault

urlpatterns = [

    path('api/opidor/import/plan/',
         import_opidor_plan,
         name="import_opidor_plan"),

    path('api/opidor/import/plan/<str:planID>/',
         import_opidor_plan_vault,
         name="import_opidor_plan_vault"),

    path('api/opidor/get/meta/for/plan/<str:planID>/',
         get_meta_opidor_plan_vault,
         name="get_meta_opidor_plan_vault"),

    path('api/opidor/export/plan/<slug:id>/',
         export_opidor_plan_vault,
         name="export_opidor_plan_vault"),

    path('api/opidor/check/connection/',
         opidor_test_connection,
         name="opidor_test_connection"),
]

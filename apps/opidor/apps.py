from django.apps import AppConfig


class OpidorConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.opidor"

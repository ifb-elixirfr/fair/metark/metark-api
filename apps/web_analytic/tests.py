import json

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from apps.web_analytic.apis import Compute_maxValue


class TestWebAnalytic(TestCase):
    def setUp(self):
        self.client = Client()

        # Create a super user to add a new user
        User.objects.create_user(
            username='superUser',
            first_name='Super',
            last_name='User',
            email='superUser@mail.com',
            password='djangoPWD',
            is_superuser=True,
            is_staff=True
        )
        logged_in = self.client.login(username="superUser",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        # Create a new user
        url = reverse('RegisterManagement')
        response = self.client.post(url, json.dumps({
            "username": "uproject",
            "password": "djangoPWD",
            "password2": "djangoPWD",
            "email": "userProject@mail.com",
            "first_name": "User",
            "last_name": "Project"
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        u = User.objects.get(username="uproject")
        u.is_superuser = True
        u.is_staff = True
        u.save()

        self.client.logout()

        # Login
        logged_in = self.client.login(username="uproject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        url = reverse('token_obtain_pair')
        response = self.client.post(url, {'username': 'uproject',
                                          'password': 'djangoPWD'},
                                    format='json')
        self.assertEqual(response.status_code, 200)

        self.assertTrue('access' in response.data)
        tokenID = response.data['access']

        url = reverse('knox_login')
        response = self.client.post(url,
                                    HTTP_AUTHORIZATION='Token ' + tokenID)
        token = response.data['token']
        self.client = Client(HTTP_AUTHORIZATION='Token ' + token)

    def test_web_analytic(self):

        # Test APIs

        url = reverse("api_get_traffic")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_browser")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_user_overview")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_traffic")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # api_get_top_path
        url = reverse("api_get_top_path", kwargs={'top': 10, 'period': "All"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_path", kwargs={'top': 10,
                                                  'period': "Today"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_path", kwargs={'top': 10,
                                                  'period': "Week"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_path", kwargs={'top': 10,
                                                  'period': "Month"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_path", kwargs={'top': 10,
                                                  'period': "Year"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_path")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url+"toto/All/")
        self.assertEqual(response.status_code, 404)

        response = self.client.get(url+"-1/All/")
        self.assertEqual(response.status_code, 404)

        url = reverse("api_get_top_path", kwargs={'top': 10, 'period': "bad"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

        # api_get_top_users
        url = reverse("api_get_top_users", kwargs={'top': 10, 'period': "All"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_users", kwargs={'top': 10,
                                                   'period': "Today"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_users", kwargs={'top': 10,
                                                   'period': "Week"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_users", kwargs={'top': 10,
                                                   'period': "Month"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_users", kwargs={'top': 10,
                                                   'period': "Year"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("api_get_top_users")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url+"toto/All/")
        self.assertEqual(response.status_code, 404)

        response = self.client.get(url+"-1/All/")
        self.assertEqual(response.status_code, 404)

        url = reverse("api_get_top_users", kwargs={'top': 10, 'period': "bad"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

        url = reverse("api_get_top_users")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(Compute_maxValue(100000000), 100001000)

from django.apps import AppConfig


class WebAnalyticConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.web_analytic"

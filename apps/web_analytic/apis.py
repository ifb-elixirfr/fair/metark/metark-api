from collections import Counter
from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.db.models import Count
from django.http import JsonResponse

from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiResponse, \
    OpenApiParameter

from rest_framework.exceptions import ValidationError

from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.users.apis import IsSuperUser
from request.models import Request
from django.db.models import F, Func, Value, CharField


def Compute_maxValue(max_value):
    if (len(str(max_value))) >= 5:
        temp = len(str(max_value)) - 3
        max = (int(str(max_value)[0:temp]) + 1) * pow(10, temp + (3 - temp))
    else:
        max = (int(str(max_value)[0]) + 1) * pow(10, int(len(str(max_value)) -
                                                         1))
    return max


@extend_schema(
    operation_id="api_get_top_browser",
    description="Get top browser",
    tags=["Traffic"],
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT, description="Data in json format."
        ),
        404: OpenApiResponse(description="No results found."),
    },
)
@api_view(["GET"])
@permission_classes([IsAuthenticated, IsSuperUser])
def api_get_top_browser(request):
    """
    Get top browser

    retrieve: json
    """

    c = Counter([r.browser for r in Request.objects.all()])
    c_final = [{"value": value, "name": key} for key, value in c.items()]
    data = dict()
    data["description"] = "Get area content"
    data["title"] = "Top browsers"
    data["data"] = c_final
    data["request_date"] = datetime.now().strftime("%Y-%m-%d  "
                                                   "%H:%M:%S") + " (UTC)"

    return JsonResponse(data)


@extend_schema(
    operation_id="api_get_top_path",
    description="Get top path",
    parameters=[
        OpenApiParameter(
            name="top",
            location=OpenApiParameter.PATH,
            description="Top n values to print",
            required=True,
            type=int,
        ),
        OpenApiParameter(
            name="period",
            location=OpenApiParameter.PATH,
            description="Period",
            required=True,
            type=str,
            enum=["All", "Today", "Week", "Month", "Year"],
        ),
    ],
    tags=["Web analytic"],
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT, description="Data in json format."
        ),
        404: OpenApiResponse(description="No results found."),
    },
)
@api_view(["GET"])
@permission_classes([IsAuthenticated, IsSuperUser])
def api_get_top_path(request, top=10, period="All"):
    """
    Get top top path

    retrieve: json
    """

    if period not in ["All", "Today", "Week", "Month", "Year"]:
        raise ValidationError("Bad period (All, Today, Week, Month or Year)")

    if period == "Today":
        days = 0
    elif period == "Week":
        days = 7
    elif period == "Month":
        days = 30
    elif period == "Year":
        days = 365
    else:
        days = None

    if days:
        res = list(
            Request.objects
                   .filter(time__gte=datetime.now() - timedelta(days=days))
                   .values("path")
                   .annotate(visit=Count("path"))
                   .order_by("-visit")
                   .values("path", "visit")[:top]
        )
    else:
        res = list(
            Request.objects.values("path")
            .annotate(visit=Count("path"))
            .order_by("-visit")
            .values("path", "visit")[:top]
        )

    col = ["path", "visit"]

    data = dict()
    data["description"] = "Get area content"
    data["title"] = "Top browsers"
    data["data"] = res
    data["col"] = col
    data["request_date"] = datetime.now().strftime("%Y-%m-%d  "
                                                   "%H:%M:%S") + " (UTC)"

    return JsonResponse(data)


@extend_schema(
    operation_id="api_get_top_users",
    description="Get top users",
    parameters=[
        OpenApiParameter(
            name="top",
            location=OpenApiParameter.PATH,
            description="Top n values to print",
            required=True,
            type=int,
        ),
        OpenApiParameter(
            name="period",
            location=OpenApiParameter.PATH,
            description="Period",
            required=True,
            type=str,
            enum=["All", "Today", "Week", "Month", "Year"],
        ),
    ],
    tags=["Web analytic"],
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT, description="Data in json format."
        ),
        404: OpenApiResponse(description="No results found."),
    },
)
@api_view(["GET"])
@permission_classes([IsAuthenticated, IsSuperUser])
def api_get_top_users(request, top=10, period="All"):
    """
    Get top top path

    retrieve: json
    """

    if period not in ["All", "Today", "Week", "Month", "Year"]:
        raise ValidationError("Bad period (All, Today, Week, Month or Year)")

    if period == "Today":
        days = 0
    elif period == "Week":
        days = 7
    elif period == "Month":
        days = 30
    elif period == "Year":
        days = 365
    else:
        days = None

    if days:
        res = list(
            Request.objects
                   .filter(time__gte=datetime.now() - timedelta(days=days))
                   .filter(user__isnull=False)
                   .annotate(Username=F("user__username"))
                   .values("Username")
                   .annotate(visit=Count("user"))
                   .order_by("-visit")
                   .values("Username", "visit")[:top]
        )
    else:
        res = list(
            Request.objects.filter(user__isnull=False)
            .annotate(Username=F("user__username"))
            .values("Username")
            .annotate(visit=Count("Username"))
            .order_by("-visit")
            .values("Username", "visit")[:top]
        )

    col = ["Username", "visit"]

    data = dict()
    data["description"] = "Top users"
    data["title"] = "Top users"
    data["data"] = res
    data["col"] = col
    data["request_date"] = datetime.now().strftime("%Y-%m-%d  "
                                                   "%H:%M:%S") + " (UTC)"

    return JsonResponse(data)


@extend_schema(
    operation_id="api_get_user_overview",
    description="Get user overview",
    tags=["Web analytic"],
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT, description="Data in json format."
        ),
        404: OpenApiResponse(description="No results found."),
    },
)
@api_view(["GET"])
@permission_classes([IsAuthenticated, IsSuperUser])
def api_get_user_overview(request):
    """
    Get top top path

    retrieve: json
    """

    data = dict()
    data["description"] = "User overview"
    data["title"] = "User overview"
    data["anonymeUsers"] = (
        Request.objects.filter(user__isnull=True).values("ip")
               .distinct().count()
    )
    data["activeUsers"] = (
        Request.objects.filter(user__isnull=False).values("user")
               .distinct().count()
    )
    data["Users"] = User.objects.all().distinct().count()
    data["request_date"] = datetime.now().strftime("%Y-%m-%d  "
                                                   "%H:%M:%S") + " (UTC)"

    return JsonResponse(data)


@extend_schema(
    operation_id="api_get_traffic",
    description="Get traffic",
    tags=["Web analytic"],
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT, description="Data in json format."
        ),
        404: OpenApiResponse(description="No results found."),
    },
)
@api_view(["GET"])
@permission_classes([IsAuthenticated, IsSuperUser])
def api_get_traffic(request):
    """
    Get top top path

    retrieve: json
    """

    count_var = Request.objects.all().count()

    if count_var != 0:
        res_dic = list()
        start = int(Request.objects.earliest("time__date").time.strftime("%Y"))
        end = datetime.today().year
        years = range(start, end + 1)
        max = 0
        for i in years:
            res = None
            res = list(
                Request.objects.annotate(
                    formatted_date=Func(
                        F("time__date"),
                        Value("yyyy/mm/dd"),
                        function="to_char",
                        output_field=CharField(),
                    )
                )
                .values("formatted_date")
                .annotate(visit=Count("formatted_date"))
                .order_by("-visit")
                .values("formatted_date", "visit")
            )

            data_cal = list()
            for r in res:
                data_cal.append([r["formatted_date"], r["visit"]])
            res_dic.append(data_cal)

            if res and max < res[0]["visit"]:
                max = Compute_maxValue(res[0]["visit"])

        last_import = Request.objects.latest("time").time
        divHeight = 140 + 120 * len(list(years))

        data = dict()
        data["res_dic"] = res_dic
        data["max"] = max
        data["divHeight"] = divHeight
        data["years"] = list(years)
        data["last_import"] = last_import
        data["title"] = "Traffic"
        data["request_date"] = datetime.now().strftime("%Y-%m-%d "
                                                       " %H:%M:%S") + " (UTC)"
        return JsonResponse(data)
    else:   # pragma: no cover
        return Response({"Message": "No results found."}, status=404)

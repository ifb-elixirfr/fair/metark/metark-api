from django.urls import path

from apps.web_analytic.apis import (
    api_get_top_browser,
    api_get_top_path,
    api_get_top_users,
    api_get_user_overview,
    api_get_traffic,
)

urlpatterns = [

    # APIs
    path(
        "api/get/traffic/",
        api_get_traffic,
        name="api_get_traffic",
    ),
    path(
        "api/get/top/browser/",
        api_get_top_browser,
        name="api_get_top_browser",
    ),
    path(
        "api/get/user/overview/",
        api_get_user_overview,
        name="api_get_user_overview",
    ),
    path(
        "api/get/top/path/",
        api_get_top_path,
        name="api_get_top_path",
    ),
    path(
        "api/get/top/path/<int:top>/<str:period>/",
        api_get_top_path,
        name="api_get_top_path",
    ),
    path(
        "api/get/top/users/",
        api_get_top_users,
        name="api_get_top_users",
    ),
    path(
        "api/get/top/users/<int:top>/<str:period>/",
        api_get_top_users,
        name="api_get_top_users",
    ),
]

import uuid

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone


class Tag(models.Model):
    label = models.CharField(help_text="Tag label", max_length=50)
    color = models.CharField(help_text="Tag color", max_length=10,
                             default="#e66465")

    def __str__(self):
        return self.label


class Affiliation(models.Model):
    name = models.TextField(
        help_text="Affiliation name")
    address = models.TextField(
        help_text="The Affiliation address")


class Comment(models.Model):
    value = models.TextField(
        help_text="Value of comment")
    name = models.TextField(
        help_text="Name of comment")


class Publication(models.Model):
    PUBLICATION_STATUS = (
        ("submitted", "submitted"),
        ("in preparation", "in preparation"),
        ("published", "published"),
    )

    title = models.TextField(
        help_text="The title of publication associated with the "
                  "investigation."
    )

    author_list = models.TextField(
        help_text="The list of authors associated with that publication."
    )

    pubmed_id = models.TextField(
        help_text="The PubMed IDs of the described publication(s) "
                  "associated with this investigation.",
        null=True,
        blank=True,
    )

    doi = models.TextField(
        help_text="A Digital Object Identifier (DOI) for that publication "
                  "(where available).",
        null=True,
        blank=True,
    )

    status = models.TextField(
        help_text="An Ontology Annotation describing the status of that "
                  "publication (i.e. submitted, in preparation, published).",
        choices=PUBLICATION_STATUS,
    )


class Role(models.Model):
    ROLES = (
        ('All', 'Read, Edit, Manage'),
        ('Edit', 'Read, Edit'),
        ('Read', 'Read'),
    )

    name = models.CharField(max_length=4, choices=ROLES,
                            default='Read',
                            help_text="Management roles : all "
                                      "(read, edit and delete), Edit "
                                      "(read and edit) and Read "
                                      "(read only). By default: Read", )

    def __str__(self):
        return self.name


class Funder(models.Model):
    name = models.TextField(
        help_text="Funder name")

    funderId = models.TextField(
        help_text="Funder id",
        blank=True,
        null=True,
    )

    idType = models.TextField(
        help_text="Funder id",
        blank=True,
        null=True,
    )


class Funding(models.Model):
    funder = models.ForeignKey(Funder, on_delete=models.CASCADE)

    fundingId = models.TextField(
        help_text="Funding id",
        blank=True,
        null=True,
    )

    idType = models.TextField(
        help_text="Funding id",
        blank=True,
        null=True,
    )

    fundingStatus = models.TextField(
        help_text="Funding status",
        blank=True,
        null=True,
    )


class Partner(models.Model):
    name = models.TextField(
        help_text="Partner name")

    partnerId = models.TextField(
        help_text="Partner id",
        blank=True,
        null=True,
    )

    idType = models.TextField(
        help_text="Partner id",
        blank=True,
        null=True,
    )


class Contact(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    corresponding_contact = models.BooleanField(
        help_text="Corresponding contact", default=False)
    affiliation = models.ManyToManyField(Affiliation)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)


class PersonContact(models.Model):
    first_name = models.CharField(
        blank=True,
        help_text="First name (only if the person isn't a omicsBroker user)",
        max_length=200,
        null=True,
    )

    last_name = models.CharField(
        blank=True,
        help_text="Last name (only if the person isn't a omicsBroker user)",
        max_length=200,
        null=True,
    )

    mid_initial = models.CharField(
        blank=True,
        help_text="The middle initials of a person associated with the study.",
        max_length=50,
        null=True,
    )

    email = models.EmailField(
        blank=True,
        help_text="Email (only if the person isn't a "
                  "omicsBroker user)",
        max_length=200,
        null=True,
    )

    phone = models.CharField(
        blank=True,
        help_text="The telephone number of a person "
                  ".",
        max_length=50,
        null=True,
    )

    fax = models.CharField(
        blank=True,
        help_text="The fax number of a person.",
        max_length=50,
        null=True,
    )

    address = models.TextField(
        blank=True,
        help_text="The address of a person.",
        null=True,
    )

    affiliation = models.TextField(
        blank=True,
        help_text="The address of a person.",
        null=True,
    )

    roles = models.TextField(
        blank=True,
        help_text="Term to classify the role(s) performed by this person in "
                  "the context of the study, which means that the roles "
                  "reported here need not correspond to roles held withing "
                  "their affiliated organization. Multiple annotations or "
                  "values attached to one person can be provided by using a "
                  "semicolon (“;”) Unicode (U0003+B) as a separator (e.g.: "
                  "submitter;funder;sponsor) .The term can be free text or "
                  "from, for example, a controlled vocabulary or an ontology. "
                  "If the latter source is used the Term Accession Number and "
                  "Term Source REF fields below are required.",
        null=True,
    )

    roles_term_association_number = models.TextField(
        blank=True,
        help_text="The accession number from the Term Source associated with "
                  "the selected term.",
        null=True,
    )

    roles_term_source_ref = models.TextField(
        blank=True,
        help_text="Identifies the controlled vocabulary or ontology that this "
                  "term comes from. The Source REF has to match one of the "
                  "Term Source Names declared in the Ontology Source "
                  "Reference section.",
        null=True,
    )

    corresponding_contact = models.BooleanField(
        help_text="Corresponding contact", blank=True, null=True,
        default=False)

    personContactId = models.TextField(
        help_text="Person Contact id",
        blank=True,
        null=True,
    )

    idType = models.TextField(
        help_text="Person Contact id",
        blank=True,
        null=True,
    )


class Investigation(models.Model):
    identifier = models.UUIDField(primary_key=True,
                                  default=uuid.uuid4,
                                  editable=False)
    title = models.TextField(
        help_text="A concise name given to the investigation.")

    alias = models.CharField(max_length=100, help_text="Investigation alias", )

    description = models.TextField(
        help_text="A textual description of the investigation.")

    submission_date = models.DateTimeField(
        help_text="The date on which the investigation was reported to "
                  "the repository.", auto_now_add=True)

    public_release_date = models.DateField(
        help_text="The date on which the investigation was released publicly.",
        null=True,
        blank=True,
    )

    last_modification_date = models.DateTimeField(
        help_text="The date of the last modification.", auto_now_add=True)

    creationSource = models.TextField(
        help_text="Creation Source",
        null=True,
        blank=True,
    )

    publications = models.ManyToManyField(Publication)
    collaborators = models.ManyToManyField(Contact)
    contacts = models.ManyToManyField(PersonContact)
    tag = models.ManyToManyField(Tag)
    comments = models.ManyToManyField(Comment)
    funding = models.ManyToManyField(Funding)
    partners = models.ManyToManyField(Partner)

    def __str__(self):
        return self.title + ' (' + str(self.pk) + ')'


class Notes(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     related_name="content_type_isa")
    object_id = models.TextField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created_date = models.DateTimeField(auto_now_add=True,
                                        help_text="Creation date")
    update_date = models.DateTimeField(auto_now_add=True,
                                       help_text="Update date")
    notes = models.JSONField(help_text="Note",
                             blank=True, null=True, )

    class Meta:
        indexes = [
            models.Index(fields=["content_type", "object_id"]),
        ]


class DesignType(models.Model):
    type = models.TextField(
        help_text="A term allowing the classification of the study based on "
                  "the overall experimental design, e.g cross-over design or "
                  "parallel group design. The term can be free text or from, "
                  "for example, a controlled vocabulary or an ontology. "
                  "If the latter source is used the Term Accession Number "
                  "and Term Source REF fields below are required.",
        null=True,
        blank=True, )

    term_accession = models.TextField(
        help_text="The accession number from the Term Source "
                  "associated with the selected term.",
        null=True,
        blank=True, )

    term_source = models.TextField(
        help_text="Identifies the controlled vocabulary or ontology that "
                  "this term comes from. The Study Design Term Source REF "
                  "has to match one the Term Source Name declared in the "
                  "Ontology Source Reference section.",
        null=True,
        blank=True, )


class Type(models.Model):
    annotation_value = models.TextField(
        help_text="Value name type",
        null=True,
        blank=True, )

    term_accession = models.TextField(
        help_text="The accession number from the Term Source "
                  "associated with the selected term.",
        null=True,
        blank=True, )

    term_source = models.TextField(
        help_text="Identifies the controlled vocabulary or ontology that "
                  "this term comes from. The Study Design Term Source REF "
                  "has to match one the Term Source Name declared in the "
                  "Ontology Source Reference section.",
        null=True,
        blank=True, )


class Factor(models.Model):
    name = models.TextField(
        help_text="The name of one factor used in the Study and/or Assay "
                  "files. A factor corresponds to an independent variable "
                  "manipulated by the experimentalist with the intention to "
                  "affect biological systems in a way that can be measured "
                  "by an assay. The value of a factor is given in the Study "
                  "or Assay file, accordingly.", )
    type = models.ForeignKey(Type, on_delete=models.CASCADE,
                             help_text="An classification of this factor "
                                       "into categories.")


class Study(models.Model):
    identifier = models.UUIDField(primary_key=True,
                                  default=uuid.uuid4,
                                  editable=False)

    title = models.TextField(
        help_text="A concise name given to the study.")

    alias = models.CharField(max_length=100, help_text="Study alias",
                             default="")

    description = models.TextField(
        help_text="A textual description of the study.")

    submission_date = models.DateTimeField(
        help_text="The date on which the study was reported to "
                  "the repository.", auto_now_add=True)

    public_release_date = models.DateField(
        help_text="The date on which the investigation was released publicly.",
        null=True,
        blank=True,
    )

    last_modification_date = models.DateTimeField(
        help_text="The date of the last modification.", auto_now_add=True)

    publications = models.ManyToManyField(Publication)
    contacts = models.ManyToManyField(PersonContact)
    comments = models.ManyToManyField(Comment)
    design_types = models.ManyToManyField(DesignType)
    factors = models.ManyToManyField(Factor)
    partners = models.ManyToManyField(Partner)

    investigation = models.ForeignKey(Investigation, on_delete=models.CASCADE)


class MeasurementType(models.Model):
    name = models.TextField(
        help_text="An Ontology Annotation to qualify the endpoint, or "
                  "what is being measured "
                  "(e.g. gene expression profiling or protein "
                  "identification).")

    def __str__(self):
        return self.name


class TechnologyType(models.Model):
    name = models.TextField(
        help_text="An Ontology Annotation to identify the technology used to "
                  "perform the measurement, e.g. DNA microarray, "
                  "mass spectrometry.")


class Platform(models.Model):
    name = models.TextField(
        help_text="Platform name", unique=True)


class Instrument(models.Model):
    name = models.TextField(
        help_text="Instrument name", unique=True)


class LibrarySelection(models.Model):
    name = models.TextField(
        help_text="Library selection name", unique=True)
    description = models.TextField(
        help_text="Library selection description")


class LibrarySource(models.Model):
    name = models.TextField(
        help_text="Library source name", unique=True)
    description = models.TextField(
        help_text="Library source description")


class LibraryStrategy(models.Model):
    name = models.TextField(
        help_text="Library strategy name", unique=True)
    description = models.TextField(
        help_text="Library strategy description")


class LibraryLayout(models.Model):
    name = models.TextField(
        help_text="Library layout name", unique=True)


class CenterName(models.Model):
    name = models.TextField(
        help_text="Center Name", unique=True)


class Assay(models.Model):
    study = models.ForeignKey(Study,
                              on_delete=models.CASCADE)

    title = models.TextField(
        help_text="A concise name given to the investigation.")

    alias = models.CharField(max_length=100, help_text="Investigation alias", )

    measurement_type = models.ForeignKey(MeasurementType,
                                         on_delete=models.CASCADE)

    additional_annotation = models.JSONField(help_text="Additional annotation "
                                                       "tag: value format",
                                             blank=True,
                                             null=True,
                                             )
    center_name = models.ForeignKey(CenterName,
                                    on_delete=models.CASCADE, blank=True,
                                    null=True, )


class MaterialType(models.Model):
    name = models.TextField(
        help_text="Library source name")
    description = models.TextField(
        help_text="Library source description")


class Material(models.Model):
    characteristics = models.TextField(
        help_text="A list of material characteristics that may be "
                  "qualitative or quantitative in description. "
                  "Qualitative values MAY be Ontology Annotations, "
                  "while quantitative values MAY be qualified with a "
                  "Unit definition.")

    material_type = models.ForeignKey(MaterialType,
                                      on_delete=models.CASCADE)


class Protocol(models.Model):
    name = models.TextField(
        help_text="The name of the protocols used within the ISA-Tab document."
                  " The names are used as identifiers within the ISA-Tab "
                  "document and will be referenced in the Study and Assay "
                  "files in the Protocol REF columns. Names can be either"
                  " local identifiers, unique within the ISA Archive which "
                  "contains them, or fully qualified external accession "
                  "numbers.")

    type = models.TextField(
        help_text="Term to classify the protocol. The term can be free text "
                  "or from, for example, a controlled vocabulary or "
                  "an ontology. If the latter source is used the "
                  "Term Accession Number and Term Source REF fields "
                  "below are required.")

    type_term_accession_number = models.TextField(
        help_text="The accession number from the Term Source associated "
                  "with the selected term.",
        null=True,
        blank=True, )

    type_term_source_ref = models.TextField(
        help_text="Identifies the controlled vocabulary or ontology that "
                  "this term comes from. The Source REF has to match one of "
                  "the Term Source Name declared in the Ontology Source "
                  "Reference section.",
        null=True,
        blank=True, )
    description = models.TextField(
        help_text="A free-text description of the protocol.")
    uri = models.TextField(
        help_text="Pointer to protocol resources external to the ISA-Tab that"
                  " can be accessed by their Uniform Resource Identifier "
                  "(URI).",
        null=True, blank=True, )
    version = models.TextField(
        help_text="An identifier for the version to ensure protocol tracking.",
        null=True, blank=True, )
    parameters_name = models.TextField(
        help_text="A semicolon-delimited (“;”) list of parameter names, "
                  "used as an identifier within the ISA-Tab document. "
                  "These names are used in the Study and Assay files "
                  "(in the “Parameter Value []” column heading) to list "
                  "the values used for each protocol parameter. Refer to"
                  " section Multiple values fields in the Investigation "
                  "File on how to encode multiple values in one field and "
                  "match term sources",
        null=True, blank=True, )

    parameters_term_accession_number = models.TextField(
        help_text="The accession number from the Term Source associated with "
                  "the selected term.",
        null=True, blank=True, )

    parameters_term_source_ref = models.TextField(
        help_text="Identifies the controlled vocabulary or ontology that this "
                  "term comes from. The Source REF has to match one of the "
                  "Term Source Name declared in the Ontology Source "
                  "Reference section.",
        null=True, blank=True, )

    components_name = models.TextField(
        help_text="A semicolon-delimited (“;”) list of a protocol’s "
                  "components; e.g. instrument names, software names, "
                  "and reagents names. Refer to section Multiple values "
                  "fields in the Investigation File on how to encode multiple "
                  "components in one field and match term sources.",
        null=True, blank=True, )

    components_type = models.TextField(
        help_text="Term to classify the protocol components listed for "
                  "example, instrument, software, detector or reagent. "
                  "The term can be free text or from, for example, a "
                  "controlled vocabulary or an ontology. If the latter "
                  "source is used the Term Accession Number and Term Source "
                  "REF fields below are required.",
        null=True, blank=True, )

    components_type_term_accession_number = models.TextField(
        help_text="	The accession number from the Source associated to the "
                  "selected terms.",
        null=True, blank=True, )

    components_type_term_source_ref = models.TextField(
        help_text="Identifies the controlled vocabulary or ontology that this "
                  "term comes from. The Source REF has to match a Term Source "
                  "Name previously declared in the ontology section",
        null=True, blank=True, )

    procedure_step = models.PositiveIntegerField(help_text="Procedure step")

    study = models.ForeignKey(Study, on_delete=models.CASCADE)


class TypeFile(models.Model):
    name = models.TextField(help_text="Type file name")

    def __str__(self):
        return self.name


class ChecksumMethod(models.Model):
    name = models.TextField(help_text="Type file name")

    def __str__(self):
        return self.name


class DataFile(models.Model):
    assay = models.ForeignKey(Assay,
                              on_delete=models.CASCADE)
    name = models.CharField(help_text="File name", max_length=200)
    alias = models.CharField(help_text="alias", max_length=200)
    checksum = models.CharField(help_text="Checksum", max_length=200,
                                null=True)
    checksum_method = models.ForeignKey(ChecksumMethod,
                                        on_delete=models.CASCADE, blank=True,
                                        null=True)
    size = models.CharField(help_text="siza", max_length=200,
                            null=True)
    type = models.ForeignKey(TypeFile,
                             on_delete=models.CASCADE)
    location = models.TextField(help_text="File location",
                                null=True, blank=True, )
    comments = models.ManyToManyField(Comment)

    center_name = models.ForeignKey(CenterName,
                                    on_delete=models.CASCADE, blank=True,
                                    null=True, )


def one_week_hence():
    return timezone.now() + timezone.timedelta(days=7)


class ToDoList(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                     related_name="content_type_todo_list_id")
    object_id = models.TextField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.content_type.model


class ToDoItem(models.Model):
    title = models.CharField(max_length=100,
                             help_text="Item title")
    description = models.TextField(null=True, blank=True,
                                   help_text="Item description")
    created_date = models.DateTimeField(auto_now_add=True,
                                        help_text="Creation date")
    due_date = models.DateTimeField(default=one_week_hence,
                                    help_text="Due date")
    checked = models.BooleanField(help_text="Item do ?",
                                  default=False)

    todo_list = models.ForeignKey(ToDoList, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.title}: due {self.due_date}"

    class Meta:
        ordering = ["checked"]


class Project(models.Model):
    title = models.TextField(
        help_text="A concise name given to the project.")

    alias = models.CharField(max_length=100, help_text="Project alias", )

    description = models.TextField(
        help_text="A textual description of the project.")

    creation_date = models.DateTimeField(
        help_text="The date on which the project was reported to "
                  "the repository.", auto_now_add=True)

    public_release_date = models.DateField(
        help_text="The date on which the project was released publicly.",
        null=True,
        blank=True,
    )

    last_modification_date = models.DateTimeField(
        help_text="The date of the last modification.", auto_now_add=True)

    investigation = models.ManyToManyField(
        Investigation, help_text="List of investigation associated.")

from django.urls import path

from apps.isa.apis import InvestigationDetail, TagManagement, \
    PublicationManagement, PersonContactManagement, CommentManagement, \
    ContactManagement, RoleManagement, SearchForUsers, StudyManagement, \
    ISAManagement, AssayManagement, ProtocolManagement, DataFileManagement, \
    PartnerManagement, FundingManagement, ToDoManagement, NotesManagement, \
    export_to_zenodo

urlpatterns = [

    # =========================================================================
    # Investigation
    # =========================================================================

    path('api/get/form/investigation/',
         InvestigationDetail.as_view({'get': 'get_form'}),
         name="InvestigationDetail_form"),

    path('api/investigation/list/',
         InvestigationDetail.as_view({'get': 'list'}),
         name="InvestigationDetail_list"),

    path('api/investigation/list/admin/',
         InvestigationDetail.as_view({'get': 'list_admin'}),
         name="InvestigationDetail_list"),

    path('api/investigation/<slug:pk>/list/of/study/',
         InvestigationDetail.as_view({'get': 'get_study_list'}),
         name="InvestigationDetail_listStudy"),

    path('api/investigation/create/',
         InvestigationDetail.as_view({'post': 'create'}),
         name="InvestigationDetail_create"),

    path('api/investigation/<slug:pk>/',
         InvestigationDetail.as_view({'get': 'retrieve',
                                      'delete': 'destroy',
                                      'put': 'update'}),
         name="InvestigationDetail"),

    path('api/investigation/<slug:pk>/resume/',
         InvestigationDetail.as_view({'get': 'retrieve_resume'}),
         name="InvestigationDetail_resume"),

    # =========================================================================
    # Tag
    # =========================================================================

    path('api/tag/list/',
         TagManagement.as_view({'get': 'list'}),
         name="TagManagement"),
    path('api/tag/<int:tag_id>/investigation/<slug:pk>/',
         TagManagement.as_view({'delete': 'destroy_tag_investigation'}),
         name="TagManagement"),
    path('api/tag/investigation/<slug:pk>/',
         TagManagement.as_view({'post': 'create_tag_investigation',
                                'get': 'list_tag_investigation'}),
         name="TagManagement"),

    # =========================================================================
    # Publication
    # =========================================================================
    path('api/get/form/publication/',
         PublicationManagement.as_view({'get': 'get_form'}),
         name="PublicationManagement"),

    path('api/publication/<str:type>/<slug:pk>/',
         PublicationManagement.as_view({'get': 'list'}),
         name="PublicationManagement"),
    path('api/publication/<int:pk>/',
         PublicationManagement.as_view({'put': 'update',
                                        'delete': 'destroy'}),
         name="PublicationManagement"),
    path('api/publication/create/for/<str:type>/<slug:pk>/',
         PublicationManagement.as_view({'post': 'create'}),
         name="PublicationManagement"),

    # =========================================================================
    # Partner
    # =========================================================================
    path('api/get/form/partner/',
         PartnerManagement.as_view({'get': 'get_form'}),
         name="PartnerManagement"),

    path('api/partner/<str:type>/<slug:pk>/',
         PartnerManagement.as_view({'get': 'list'}),
         name="PartnerManagement"),
    path('api/partner/<int:pk>/',
         PartnerManagement.as_view({'put': 'update',
                                    'delete': 'destroy'}),
         name="PartnerManagement"),
    path('api/partner/create/for/<str:type>/<slug:pk>/',
         PartnerManagement.as_view({'post': 'create'}),
         name="PartnerManagement"),

    # =========================================================================
    # Funding
    # =========================================================================
    path('api/get/form/funding/',
         FundingManagement.as_view({'get': 'get_form'}),
         name="FundingManagement"),

    path('api/funding/<str:type>/<slug:pk>/',
         FundingManagement.as_view({'get': 'list'}),
         name="FundingManagement"),
    path('api/funding/<int:pk>/',
         FundingManagement.as_view({'put': 'update',
                                    'delete': 'destroy'}),
         name="FundingManagement"),
    path('api/funding/create/for/<str:type>/<slug:pk>/',
         FundingManagement.as_view({'post': 'create'}),
         name="FundingManagement"),


    # =========================================================================
    # PersonContact
    # =========================================================================

    path('api/get/form/personContact/',
         PersonContactManagement.as_view({'get': 'get_form'}),
         name="PublicationManagement"),
    path('api/personContact/<str:type>/<slug:pk>/',
         PersonContactManagement.as_view({'get': 'list'}),
         name="PersonContactManagement"),
    path('api/personContact/create/for/<str:type>/<slug:pk>/',
         PersonContactManagement.as_view({'post': 'create'}),
         name="PersonContactManagement"),
    path('api/personContact/<int:pk>/',
         PersonContactManagement.as_view({'put': 'update',
                                          'delete': 'destroy'})),

    # =========================================================================
    # Comment
    # =========================================================================

    path('api/comment/investigation/<slug:pk>/',
         CommentManagement.as_view({'get': 'list_investigation'}),
         name="CommentManagement"),

    # =========================================================================
    # Contact
    # =========================================================================

    path('api/contact/investigation/<slug:pk>/',
         ContactManagement.as_view({'get': 'list_investigation',
                                    'post': 'create_investigation'}),
         name="ContactManagement"),

    path('api/contact/<int:pk_contact>/investigation/<slug:pk>/',
         ContactManagement.as_view({'delete': 'destroy_investigation',
                                    'get': 'retrieve_investigation',
                                    'put': 'update_investigation', }),
         name="ContactManagement"),

    # =========================================================================
    # Role
    # =========================================================================
    path('api/role/list/',
         RoleManagement.as_view({'get': 'list'}),
         name="RoleManagement"),

    path('api/search/user/',
         SearchForUsers.as_view(),
         name="SearchForUsers"),

    # =========================================================================
    # Study
    # =========================================================================

    path('api/study/list/investigation/<slug:pk>/',
         StudyManagement.as_view({'get': 'list_investigation'}),
         name="StudyManagement_invList"),
    path('api/study/create/',
         StudyManagement.as_view({'post': 'create'}),
         name="StudyManagement_create"),
    path('api/study/<slug:pk>/',
         StudyManagement.as_view({
             'get': 'retrieve',
             'put': 'update',
             'delete': 'destroy', }),
         name="StudyManagement"),

    path('api/protocol/from/study/<slug:pk>/',
         ProtocolManagement.as_view({
             'get': 'list_from_study', }),
         name="StudyManagement_protocol"),


    # =========================================================================
    # Assay
    # =========================================================================

    path('api/study/resume/from/metadata/<int:pk_metadata>/',
         ISAManagement.as_view({'get': 'retrieve_resume_from_metadata'}),
         name="ISAManagement"),

    path('api/get/form/assay/',
         AssayManagement.as_view({'get': 'get_form'}),
         name="AssayManagement_form"),
    path('api/assay/create/for/study/<slug:pk>/',
         AssayManagement.as_view({'post': 'create'}),
         name="AssayManagement_create"),

    path('api/assay/multi/create/for/study/<slug:pk>/',
         AssayManagement.as_view({'post': 'create_multi_assays'}),
         name="AssayManagement_create"),

    path('api/assay/list/for/study/<slug:pk>/',
         AssayManagement.as_view({'get': 'list_study'}),
         name="AssayManagement_listStudy"),
    path('api/assay/<int:pk>/',
         AssayManagement.as_view({'delete': 'destroy', }),
         name="AssayManagement"),

    # =========================================================================
    # DataFile
    # =========================================================================
    path('api/datafile/from/assay/<int:pk>/',
         DataFileManagement.as_view({'get': 'list_from_assay', }),
         name="DataFileManagement"),
    path('api/get/form/datafile/',
         DataFileManagement.as_view({'get': 'get_form'})),
    path('api/datafile/create/for/assay/<int:pk>/',
         DataFileManagement.as_view({'post': 'create'})),
    path('api/datafile/<int:pk>/',
         DataFileManagement.as_view({
             'put': 'update',
             'delete': 'destroy', })),

    # =========================================================================
    # To do
    # =========================================================================
    path('api/todo/from/<str:model_name>/<str:id>/',
         ToDoManagement.as_view({'get': 'list_from_object', }),
         name="ToDoManagement"),
    path('api/todolist/from/<str:model_name>/<str:id>/',
         ToDoManagement.as_view({'get': 'retrieve_to_do_list', }),
         name="ToDoManagement"),
    path('api/todo/create/item/',
         ToDoManagement.as_view({'post': 'create'})),
    path('api/todo/<int:pk>/',
         ToDoManagement.as_view({
             'put': 'update',
             'delete': 'destroy', })),

    # =========================================================================
    # Notes
    # =========================================================================
    path('api/notes/from/<str:model_name>/<str:id>/',
         NotesManagement.as_view({'get': 'retrieve_from_object', }),
         name="NotesManagement"),
    path('api/notes/create/notes/for/<str:model_name>/<str:id>/',
         NotesManagement.as_view({'post': 'create'})),
    path('api/notes/<int:pk>/',
         NotesManagement.as_view({
             'put': 'update',
             'delete': 'destroy', })),

    # =========================================================================
    # Export
    # =========================================================================

    path('api/zenodo/sandbox/<slug:id>/',
         export_to_zenodo,
         name="export_to_zenodo")

]

from rest_framework.serializers import ModelSerializer, \
    SerializerMethodField, CharField
from django.contrib.auth.models import User
from apps.isa.models import Investigation, Contact, PersonContact, \
    Publication, Comment, Tag, Role, Study, Factor, DesignType, Protocol, \
    Assay, DataFile, MeasurementType, TypeFile, LibraryStrategy, Platform, \
    Instrument, LibrarySelection, LibrarySource, Partner, Funding, Funder, \
    ToDoItem, ToDoList, Notes


class ISAResumeSerializer(ModelSerializer):
    assay = SerializerMethodField()
    investigation = SerializerMethodField()

    class Meta:
        model = Study
        fields = "__all__"

    def get_assay(self, arg_obj):
        selected_options = Assay.objects.filter(study=arg_obj)
        print(selected_options)
        return AssaySerializerDisplay(selected_options, many=True).data

    def get_investigation(self, arg_obj):
        selected_options = arg_obj.investigation
        return InvestigationResumeSerializer(selected_options).data


class InvestigationResumeSerializer(ModelSerializer):
    class Meta:
        model = Investigation
        fields = ['title', 'description']


class StudyResumeSerializer(ModelSerializer):
    class Meta:
        model = Study
        fields = ['pk', 'title', 'description', 'alias', 'submission_date',
                  'public_release_date', 'last_modification_date']


class StudyManageSerializer(ModelSerializer):
    class Meta:
        model = Study
        fields = ['pk', 'title', 'description', 'alias', 'investigation']


class AssayResumeSerializer(ModelSerializer):
    study = SerializerMethodField()

    class Meta:
        model = Assay
        fields = "__all__"

    def get_study(self, arg_obj):
        selected_options = arg_obj.study
        return StudyResumeSerializer(selected_options).data


class StudySerializer(ModelSerializer):
    publicationList = SerializerMethodField()
    contactList = SerializerMethodField()
    commentList = SerializerMethodField()
    factorList = SerializerMethodField()
    DesignTypeList = SerializerMethodField()
    ProtocolList = SerializerMethodField()
    AssayList = SerializerMethodField()

    class Meta:
        model = Study
        fields = "__all__"

    def get_publicationList(self, arg_obj):
        selected_options = arg_obj.publications
        return PublicationSerializer(selected_options, many=True).data

    def get_contactList(self, arg_obj):
        selected_options = arg_obj.contacts
        return PersonContactSerializer(selected_options, many=True).data

    def get_commentList(self, arg_obj):
        selected_options = arg_obj.comments
        return CommentSerializer(selected_options, many=True).data

    def get_factorList(self, arg_obj):
        selected_options = arg_obj.factors
        return FactorSerializer(selected_options, many=True).data

    def get_DesignTypeList(self, arg_obj):
        selected_options = arg_obj.design_types
        return DesignTypeSerializer(selected_options, many=True).data

    def get_ProtocolList(self, arg_obj):
        selected_options = (Protocol.objects
                            .filter(study=arg_obj)
                            .order_by('procedure_step'))
        return ProtocolSerializer(selected_options, many=True).data

    def get_AssayList(self, arg_obj):
        selected_options = (Assay.objects
                            .filter(study=arg_obj))
        return AssaySerializerDisplay(selected_options, many=True).data


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "first_name",
            "last_name",
            "email"
        ]


class RoleSerializer(ModelSerializer):
    description = CharField(source='get_name_display')

    class Meta:
        model = Role
        fields = [
            "id",
            "name",
            "description"
        ]


class ContactSerializer(ModelSerializer):
    roleDescription = SerializerMethodField()
    userInfo = SerializerMethodField()

    class Meta:
        model = Contact

        fields = (
            'pk',
            'user',
            'userInfo',
            'corresponding_contact',
            'role',
            'roleDescription'
        )

    def get_userInfo(self, arg_obj):
        if arg_obj.__class__.__name__ == "Contact":
            selected_options = arg_obj.user
            return UserSerializer(selected_options).data

    def get_roleDescription(self, arg_obj):
        if arg_obj.__class__.__name__ == "Contact":
            selected_options = arg_obj.role
            return RoleSerializer(selected_options).data


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


class PartnerSerializer(ModelSerializer):
    class Meta:
        model = Partner
        fields = "__all__"


class FunderSerializer(ModelSerializer):
    class Meta:
        model = Funder
        fields = "__all__"


class FundingSerializer(ModelSerializer):
    funder = SerializerMethodField()

    class Meta:
        model = Funding
        fields = "__all__"

    def get_funder(self, arg_obj):
        selected_options = arg_obj.funder
        return FunderSerializer(selected_options).data


class PublicationSerializer(ModelSerializer):
    class Meta:
        model = Publication
        fields = "__all__"


class PersonContactSerializer(ModelSerializer):
    class Meta:
        model = PersonContact
        fields = "__all__"


class TagSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


class FactorSerializer(ModelSerializer):
    class Meta:
        model = Factor
        fields = "__all__"


class DesignTypeSerializer(ModelSerializer):
    class Meta:
        model = DesignType
        fields = "__all__"


class ProtocolSerializer(ModelSerializer):
    class Meta:
        model = Protocol
        exclude = ('study', )


class AssaySerializerResume(ModelSerializer):
    class Meta:
        model = Assay
        fields = (
            'pk',
            'title',
            'alias'
        )


class AssaySerializerWithoutStudy(ModelSerializer):
    class Meta:
        model = Assay
        exclude = ('study', )


class AssaySerializer(ModelSerializer):
    class Meta:
        model = Assay
        fields = "__all__"


class AssaySerializerDisplay(ModelSerializer):
    DataFileList = SerializerMethodField()
    measurement_type = SerializerMethodField()

    class Meta:
        model = Assay
        exclude = ('study', )

    def get_measurement_type(self, arg_obj):
        selected_options = arg_obj.measurement_type
        return MeasurementTypeSerializer(selected_options).data

    def get_DataFileList(self, arg_obj):
        selected_options = (DataFile.objects
                            .filter(assay=arg_obj))
        return DataFileSerializer(selected_options, many=True).data


class TypeFileSerializer(ModelSerializer):
    class Meta:
        model = TypeFile
        fields = "__all__"


class DataFileSerializerForm(ModelSerializer):
    class Meta:
        model = DataFile
        exclude = ('assay', 'comments', )


class DataFileSerializer(ModelSerializer):
    class Meta:
        model = DataFile
        exclude = ('comments', )


class DataFileSerializerDisplay(ModelSerializer):
    type = SerializerMethodField()
    comments = SerializerMethodField()

    class Meta:
        model = DataFile
        exclude = ('assay', )

    def get_type(self, arg_obj):
        selected_options = arg_obj.type
        return TypeFileSerializer(selected_options).data['name']

    def get_comments(self, arg_obj):
        selected_options = arg_obj.comments
        return CommentSerializer(selected_options, many=True).data


class MeasurementTypeSerializer(ModelSerializer):
    class Meta:
        model = MeasurementType
        fields = "__all__"


class PlatformSerializer(ModelSerializer):
    class Meta:
        model = Platform
        fields = "__all__"


class InstrumentSerializer(ModelSerializer):
    class Meta:
        model = Instrument
        fields = "__all__"


class LibrarySelectionSerializer(ModelSerializer):
    class Meta:
        model = LibrarySelection
        fields = "__all__"


class LibrarySourceSerializer(ModelSerializer):
    class Meta:
        model = LibrarySource
        fields = "__all__"


class LibraryStrategySerializer(ModelSerializer):
    class Meta:
        model = LibraryStrategy
        fields = "__all__"


class InvestigationSerializer(ModelSerializer):
    class Meta:
        model = Investigation
        fields = ('pk',
                  'title',
                  'alias',
                  'description',
                  'submission_date',
                  'creationSource',
                  'last_modification_date',
                  )


class ToDoItemSerializer(ModelSerializer):
    class Meta:
        model = ToDoItem
        fields = "__all__"


class ToDoListSerializer(ModelSerializer):
    class Meta:
        model = ToDoList
        fields = "__all__"


class NotesSerializer(ModelSerializer):
    class Meta:
        model = Notes
        fields = "__all__"

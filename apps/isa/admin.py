from django.contrib import admin

from apps.isa.models import Investigation


@admin.register(Investigation)
class InvestigationAdmin(admin.ModelAdmin):
    fs = [
        f.name for f in Investigation._meta.fields if f.name != "id"
    ]
    list_display = fs

    # Disbale add functionality
    def has_add_permission(self, request):
        return False

###############################################################################
# form2api
###############################################################################
import requests
from django.db.models import F

from apps.isa.models import ToDoItem


def PrimaryKeySelector(term):
    match term:
        case "study":
            return ("title")
        case _:
            return ("name")


def form2api(serializer_obj):
    """Create a todo list

    :param serializer_obj: a serializer object

    :return formList: Form in list format
    """
    formList = []
    for key, value in serializer_obj.fields.items():
        formDict = dict()
        if value.read_only is False:
            formDict["name"] = key
            formDict["label"] = value.label
            formDict["required"] = value.required
            formDict["type"] = value.__class__.__name__
            formDict["help_text"] = value.help_text

            if value.__class__.__name__ == "ChoiceField":
                formDict["choices"] = value.choice_strings_to_values
            if value.__class__.__name__ == "PrimaryKeyRelatedField":
                formDict["queryset"] = list(
                    value.get_queryset()
                         .annotate(label=F(PrimaryKeySelector(formDict["name"])
                                           )
                                   )
                         .values("pk", "label"))

            formList.append(formDict)
    return formList


def createToDoItemsInv(tdl):
    """Create a todo list

    :param tdl: a toDoList object
    """
    ToDoItem.objects.create(todo_list=tdl, title="Create a study")
    ToDoItem.objects.create(todo_list=tdl, title="Add a assay")
    ToDoItem.objects.create(todo_list=tdl, title="Add a collaborators")
    ToDoItem.objects.create(todo_list=tdl, title="...")


def get_taxon_id(scientific_name):
    """Get taxon ID for input scientific_name.

    :param scientific_name: scientific name of sample that distinguishes
                            its taxonomy
    :return taxon_id: NCBI taxonomy identifier
    """
    # endpoint for taxonomy id
    url = 'http://www.ebi.ac.uk/ena/taxonomy/rest/scientific-name'
    session = requests.Session()
    session.trust_env = False
    # url encoding: space -> %20
    scientific_name = '%20'.join(scientific_name.strip().split())
    r = session.get(f"{url}/{scientific_name}")
    try:
        taxon_id = r.json()[0]['taxId']
        return taxon_id
    except Exception:
        return None


def get_scientific_name(taxon_id):
    """Get taxon ID for input scientific_name.

    :param taxon_id: a taxon id
    :return scientific_name: The scientific name associated with the taxon id
    """
    # endpoint for taxonomy id
    url = 'http://www.ebi.ac.uk/ena/taxonomy/rest/tax-id'
    session = requests.Session()
    session.trust_env = False
    # url encoding: space -> %20
    r = session.get(f"{url}/{taxon_id}")
    try:
        scientificName = r.json()['scientificName']
        return scientificName
    except Exception:
        return None

import json

import hvac
import requests
from django.contrib.auth.models import User
from django.db.models.functions import Now
from django.http import JsonResponse, HttpResponse
from drf_spectacular.types import OpenApiTypes

from drf_spectacular.utils import extend_schema, OpenApiParameter, \
    OpenApiResponse
from rest_framework import status, pagination, generics
from rest_framework.decorators import permission_classes, api_view

from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rest_framework.viewsets import ModelViewSet

from apps.data_brokering.models import ExternalID
from apps.isa._functions import form2api, createToDoItemsInv
from apps.isa.models import Investigation, Contact, Role, Tag, Study, \
    Publication, Assay, PersonContact, Comment, Protocol, DataFile, TypeFile, \
    Partner, Funding, ToDoList, ToDoItem, Notes, MeasurementType
from apps.isa.serializers import InvestigationSerializer, TagSerializer, \
    PublicationSerializer, PersonContactSerializer, CommentSerializer, \
    ContactSerializer, RoleSerializer, UserSerializer, StudySerializer, \
    InvestigationResumeSerializer, AssaySerializer, \
    ISAResumeSerializer, StudyManageSerializer, AssaySerializerWithoutStudy, \
    AssaySerializerDisplay, StudyResumeSerializer, ProtocolSerializer, \
    DataFileSerializer, DataFileSerializerDisplay, DataFileSerializerForm, \
    PartnerSerializer, FundingSerializer, ToDoItemSerializer, \
    ToDoListSerializer, NotesSerializer
from apps.metadata.apis import get_list_fields
from apps.metadata.models import Metadata, Database

from django.apps import apps


###############################################################################
# Pagination
###############################################################################
from apps.users.apis import IsSuperUser


class CustomPagination(pagination.PageNumberPagination):
    page_size = 6
    page_size_query_param = 'page_size'
    max_page_size = 50
    page_query_param = 'p'

    def get_paginated_response(self, data):
        response = super(CustomPagination,
                         self).get_paginated_response(data)
        response.data['total_pages'] = self.page.paginator.num_pages
        return response


###############################################################################
# Decorators
###############################################################################

@extend_schema(
    operation_id="api_manage_isa",
    description="Manage isa",
    tags=["Assay - Serializer"],
)
class ISAManagement(ModelViewSet):
    serializer_class = ISAResumeSerializer
    queryset = Study.objects.all()

    def retrieve_resume_from_metadata(self, request, pk_metadata=None):
        meta = get_object_or_404(Metadata, pk=pk_metadata)
        item = get_object_or_404(self.queryset, pk=meta.study.pk)
        serializer = ISAResumeSerializer(item)
        return Response(serializer.data)


###############################################################################
# ASSAY
###############################################################################


@extend_schema(
    operation_id="api_manage_assay",
    description="Manage assay",
    tags=["Assay"],
)
@permission_classes([IsAuthenticated])
class AssayManagement(ModelViewSet):
    serializer_class = AssaySerializer
    queryset = Assay.objects.all()

    def get_form(self, request):
        return JsonResponse(form2api(AssaySerializerWithoutStudy()),
                            safe=False, status=200)

    def list_study(self, request, pk):
        q = get_object_or_404(Study, pk=pk)
        serializer = AssaySerializerDisplay(Assay.objects.filter(study=q),
                                            many=True)
        return Response(serializer.data)

    def create_multi_assays(self, request, pk):
        q = get_object_or_404(Study, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']

        print(data)
        error = list()
        for d in data:
            d['measurement_type'] = MeasurementType.objects.get_or_create(
                name=d['measurement_type']
            )[0].pk
            d['study'] = str(q.identifier)
            serializer = AssaySerializer(data=d)
            if serializer.is_valid():
                serializer.save()
            else:
                print(serializer.errors)
                error.append(serializer.errors)

        if len(error) != 0:
            return JsonResponse(error, status=400)
        else:
            return Response({'message': "ok"},
                            status=status.HTTP_201_CREATED)

    def create(self, request, pk):
        q = get_object_or_404(Study, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']

        data['study'] = str(q.identifier)
        serializer = AssaySerializer(data=data)
        if serializer.is_valid():
            obj = serializer.save()
            try:
                m = Metadata.objects.get(study=obj.study)

                list_fields = get_list_fields(m.checklist)

                inter = {}
                for lf in list_fields:
                    inter[lf.name] = None
                if m.table is None:
                    m.table = dict()

                m.table[obj.id] = inter
                m.save()

            except Metadata.DoesNotExist:
                pass

            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(Assay, pk=pk)

        try:
            m = Metadata.objects.get(study=q.study)
            if m.table is not None:
                try:
                    m.table.pop(str(q.id))
                    if len(m.table) == 0:
                        m.table = None
                    m.save()
                except KeyError as ex:
                    print("No such key: '%s'" % ex.message)
        except Metadata.DoesNotExist:
            pass
        q.delete()
        return HttpResponse(status=204)


###############################################################################
# STUDY
###############################################################################


@extend_schema(
    operation_id="api_manage_study",
    description="Manage study ",
    tags=["Study"],
)
@permission_classes([IsAuthenticated])
class StudyManagement(ModelViewSet):
    serializer_class = StudySerializer
    queryset = Study.objects.all()

    def list_investigation(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        q2 = Study.objects.filter(investigation=q)
        serializer = StudySerializer(q2, many=True)
        return Response(serializer.data)

    def update(self, request, pk):
        q = get_object_or_404(Study, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = StudyManageSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def create(self, request):
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = StudyManageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(Study, pk=pk)
        q.delete()
        return HttpResponse(status=204)

    def retrieve(self, request, pk):
        item = get_object_or_404(Study, pk=pk)
        serializer = StudyResumeSerializer(item)
        return Response(serializer.data)


###############################################################################
# INVESTIGATION
###############################################################################

@extend_schema(
    operation_id="api_category_detail",
    description="Manage question detail",
    tags=["Investigation"],
)
@permission_classes([IsAuthenticated])
class InvestigationDetail(ModelViewSet):
    serializer_class = InvestigationSerializer
    queryset = Investigation.objects.all()
    pagination_class = CustomPagination

    filter_params = ["tag"]

    def get_form(self, request):
        return JsonResponse(form2api(InvestigationSerializer()),
                            safe=False, status=200)

    def get_queryset(self):
        filter_kwargs = dict()
        for param in self.filter_params:
            value = self.request.query_params.get(param)
            if value is not None:
                param = 'tag__label' if param == "tag" else param
                filter_kwargs[param] = value
        queryset = self.queryset.filter(**filter_kwargs)
        return queryset

    def list(self, request):
        u = get_object_or_404(User, username=request.user.username)
        page = self.paginate_queryset(
            self.get_queryset()
                .filter(collaborators__user=u)
                .order_by("submission_date"))
        serializer = InvestigationSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    @permission_classes([IsAuthenticated, IsSuperUser])
    def list_admin(self, request):
        page = self.paginate_queryset(self.get_queryset().order_by(
            "submission_date"))
        serializer = InvestigationSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        u = get_object_or_404(User, username=request.user.username)
        if u.is_superuser or item.collaborators.filter(user=u).exists():
            serializer = InvestigationSerializer(item)
        else:
            return HttpResponse('Unauthorized', status=401)
        return Response(serializer.data)

    def get_study_list(self, request, pk):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = StudyResumeSerializer(item.study_set.all(), many=True)
        return Response(serializer.data)

    def retrieve_resume(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = InvestigationResumeSerializer(item)
        return Response(serializer.data)

    def destroy(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        q.delete()
        return HttpResponse(status=204)

    def update(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = InvestigationSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def create(self, request):
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = InvestigationSerializer(data=data)
        if serializer.is_valid():
            obj = serializer.save()

            tdl = ToDoList.objects.create(
                content_object=obj
            )
            createToDoItemsInv(tdl)

            user_temp = User.objects.get(username=request.user.username)
            c = Contact.objects.create(
                user=user_temp,
                corresponding_contact=True,
                role=Role.objects.get_or_create(name='All')[0])
            obj.collaborators.add(c)
            obj.save()

            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)


###############################################################################
# PARTNER
###############################################################################


@extend_schema(
    operation_id="api_manage_partner",
    description="Manage tag",
    tags=["Partner"],
)
@permission_classes([IsAuthenticated])
class PartnerManagement(ModelViewSet):
    serializer_class = PartnerSerializer
    queryset = Partner.objects.all()

    def get_form(self, request):
        return JsonResponse(form2api(PartnerSerializer()),
                            safe=False, status=200)

    # Investigation
    def list(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)

        serializer = PartnerSerializer(q.partners.all(), many=True)
        return Response(serializer.data)

    def update(self, request, pk):
        q = get_object_or_404(Partner, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PartnerSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def create(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)

        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PartnerSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            obj = serializer.save()
            q.partners.add(obj)
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(Partner, pk=pk)
        q.delete()
        return HttpResponse(status=204)


###############################################################################
# FUNDING
###############################################################################


@extend_schema(
    operation_id="api_manage_funding",
    description="Manage tag",
    tags=["FUNDING"],
)
@permission_classes([IsAuthenticated])
class FundingManagement(ModelViewSet):
    serializer_class = FundingSerializer
    queryset = Funding.objects.all()

    def get_form(self, request):
        return JsonResponse(form2api(FundingSerializer()),
                            safe=False, status=200)

    # Investigation
    def list(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)

        serializer = FundingSerializer(q.funding.all(), many=True)
        return Response(serializer.data)

    def update(self, request, pk):
        q = get_object_or_404(Funding, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = FundingSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def create(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)

        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = FundingSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            obj = serializer.save()
            q.funding.add(obj)
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(Funding, pk=pk)
        q.delete()
        return HttpResponse(status=204)


###############################################################################
# PUBLICATION
###############################################################################


@extend_schema(
    operation_id="api_manage_publication",
    description="Manage tag",
    tags=["Publication"],
)
@permission_classes([IsAuthenticated])
class PublicationManagement(ModelViewSet):
    serializer_class = PublicationSerializer
    queryset = Publication.objects.all()
    pagination_class = CustomPagination

    def get_form(self, request):
        return JsonResponse(form2api(PublicationSerializer()),
                            safe=False, status=200)

    # Investigation
    def list(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)

        serializer = PublicationSerializer(q.publications.all(), many=True)
        return Response(serializer.data)

    def update(self, request, pk):
        q = get_object_or_404(Publication, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PublicationSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def create(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)

        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PublicationSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            obj = serializer.save()
            q.publications.add(obj)
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(Publication, pk=pk)
        q.delete()
        return HttpResponse(status=204)


###############################################################################
# PERSONContact
###############################################################################


@extend_schema(
    operation_id="api_manage_publication",
    description="Manage tag",
    tags=["Person Contact"],
)
@permission_classes([IsAuthenticated])
class PersonContactManagement(ModelViewSet):
    serializer_class = PersonContactSerializer
    queryset = PersonContact.objects.all()

    def get_form(self, request):
        return JsonResponse(form2api(PersonContactSerializer()),
                            safe=False, status=200)

    def list(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)
        serializer = PersonContactSerializer(q.contacts.all(), many=True)
        return Response(serializer.data)

    def create(self, request, type, pk):
        if type == "investigation":
            q = get_object_or_404(Investigation, pk=pk)
        elif type == "study":
            q = get_object_or_404(Study, pk=pk)
        else:
            return JsonResponse("Type : investigation or study", status=400)

        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PersonContactSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            obj = serializer.save()
            q.contacts.add(obj)
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    def update(self, request, pk):
        q = get_object_or_404(PersonContact, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PersonContactSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(PersonContact, pk=pk)
        q.delete()
        return HttpResponse(status=204)


###############################################################################
# Contact
###############################################################################

@extend_schema(
    operation_id="api_manage_contact",
    description="Manage contact",
    tags=["Person Contact"],
)
@permission_classes([IsAuthenticated])
class ContactManagement(ModelViewSet):
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()
    pagination_class = CustomPagination

    # Investigation
    def list_investigation(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        page = self.paginate_queryset(q.collaborators.all().order_by("user"))
        serializer = ContactSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    def create_investigation(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        data = {
            'user': request.data.get('user'),
            'corresponding_contact': request.data.get('corresponding_contact'),
            'role': request.data.get('role'),
        }

        serializer = ContactSerializer(data=data)

        if serializer.is_valid():
            obj = serializer.save()
            q.collaborators.add(obj)

            q2 = get_object_or_404(Investigation, pk=pk)
            serializer2 = ContactSerializer(q2.collaborators.all(), many=True)
            return Response(serializer2.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy_investigation(self, request, pk, pk_contact):
        q = get_object_or_404(Investigation, pk=pk)
        try:
            q2 = q.collaborators.get(pk=pk_contact)
        except Tag.DoesNotExist:
            return HttpResponse(status=404)

        q2.delete()
        return HttpResponse(status=204)

    def retrieve_investigation(self, request, pk, pk_contact):
        q = get_object_or_404(Investigation, pk=pk)
        try:
            q2 = q.collaborators.get(pk=pk_contact)
        except Contact.DoesNotExist:
            return HttpResponse(status=404)

        serializer = ContactSerializer(q2)
        return Response(serializer.data)

    def update_investigation(self, request, pk, pk_contact):
        q = get_object_or_404(Investigation, pk=pk)
        try:
            q2 = q.collaborators.get(pk=pk_contact)
        except Contact.DoesNotExist:
            return HttpResponse(status=404)

        data = {
            'user': request.data.get('user'),
            'corresponding_contact': request.data.get('corresponding_contact'),
            'role': request.data.get('role'),
        }

        serializer = ContactSerializer(q2, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)

        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)


###############################################################################
# COMMENT
###############################################################################

@extend_schema(
    operation_id="api_manage_comment",
    description="Manage comment",
    tags=["Comment"],
)
@permission_classes([IsAuthenticated])
class CommentManagement(ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    # Investigation
    def list_investigation(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        serializer = CommentSerializer(q.comments.all(), many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id="api_manage_role",
    description="Manage role",
    tags=["Investigation"],
)
@permission_classes([IsAuthenticated])
class RoleManagement(ModelViewSet):
    serializer_class = RoleSerializer
    queryset = Role.objects.all()

    def list(self, request):
        serializer = RoleSerializer(Role.objects.all(), many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id="api_manage_tag",
    description="Manage tag",
    tags=["Tag"],
)
@permission_classes([IsAuthenticated])
class TagManagement(ModelViewSet):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()
    pagination_class = CustomPagination

    def list(self, request):
        page = self.paginate_queryset(self.queryset.order_by("label"))
        serializer = TagSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    # Investigation
    def list_tag_investigation(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        serializer = TagSerializer(q.tag.all(), many=True)
        return Response(serializer.data)

    def create_tag_investigation(self, request, pk):
        q = get_object_or_404(Investigation, pk=pk)
        data = {
            'label': request.data.get('label'),
            'color': request.data.get('color')
        }
        serializer = TagSerializer(data=data)

        if serializer.is_valid():
            obj = serializer.save()
            q.tag.add(obj)
            serializer = TagSerializer(q.tag.all(), many=True)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy_tag_investigation(self, request, pk, tag_id):
        q = get_object_or_404(Investigation, pk=pk)
        try:
            q2 = q.tag.get(id=tag_id)
        except Tag.DoesNotExist:
            return HttpResponse(status=404)

        q2.delete()
        serializer = TagSerializer(q.tag.all(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


@extend_schema(
    operation_id="api_protocol_detail",
    description="Manage protocol",
    tags=["Study"],
)
@permission_classes([IsAuthenticated])
class ProtocolManagement(ModelViewSet):
    serializer_class = ProtocolSerializer
    queryset = Protocol.objects.all()

    def list_from_study(self, request, pk):
        q = get_object_or_404(Study, pk=pk)
        serializer = ProtocolSerializer(
            Protocol.objects.filter(study=q).order_by('procedure_step'),
            many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id="api_DataFile_management",
    description="Manage protocol",
    tags=["Study"],
)
@permission_classes([IsAuthenticated])
class DataFileManagement(ModelViewSet):
    serializer_class = DataFileSerializer
    queryset = DataFile.objects.all()

    def list_from_assay(self, request, pk):
        q = get_object_or_404(Assay, pk=pk)
        serializer = DataFileSerializerDisplay(
            DataFile.objects.filter(assay=q),
            many=True)
        return Response(serializer.data)

    def get_form(self, request):
        return JsonResponse(form2api(DataFileSerializerForm()),
                            safe=False, status=200)

    def create(self, request, pk):
        q = get_object_or_404(Assay, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        data['assay'] = str(q.id)

        serializer = DataFileSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def update(self, request, pk):
        q = get_object_or_404(DataFile, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        data['assay'] = str(q.assay.id)
        if (isinstance(data['type'], str)):
            data['type'] = TypeFile.objects.get(name=data['type']).id
        serializer = DataFileSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(DataFile, pk=pk)
        q.delete()
        return HttpResponse(status=204)

###############################################################################
# SEARCHUSER
###############################################################################


class SearchForUsers(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    pagination_class = None
    search_fields = ['username']


###############################################################################
# Todo
###############################################################################

@extend_schema(
    operation_id="api_todo_management",
    description="Manage todo list",
    tags=["To do"],
)
@permission_classes([IsAuthenticated])
class ToDoManagement(ModelViewSet):
    def retrieve_to_do_list(self, request, model_name, id):
        tdl = get_object_or_404(ToDoList,
                                content_type__model=model_name,
                                object_id=id)
        serializer = ToDoListSerializer(tdl)
        return Response(serializer.data)

    def list_from_object(self, request, model_name, id):
        tdl = get_object_or_404(ToDoList,
                                content_type__model=model_name,
                                object_id=id)

        todo_items = ToDoItem.objects.filter(
            todo_list=tdl
        )

        serializer = ToDoItemSerializer(todo_items, many=True)
        return Response(serializer.data)

    def destroy(self, request, pk):
        q = get_object_or_404(ToDoItem, pk=pk)
        q.delete()
        return HttpResponse(status=204)

    def create(self, request):
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']

        serializer = ToDoItemSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def update(self, request, pk):
        q = get_object_or_404(ToDoItem, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = ToDoItemSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)


###############################################################################
# Todo
###############################################################################

@extend_schema(
    operation_id="api_notes_management",
    description="Manage todo list",
    tags=["Notes"],
)
@permission_classes([IsAuthenticated])
class NotesManagement(ModelViewSet):
    def retrieve_from_object(self, request, model_name, id):
        q = get_object_or_404(Notes,
                              content_type__model=model_name,
                              object_id=id
                              )
        serializer = NotesSerializer(q)
        return Response(serializer.data)

    def create(self, request, model_name, id):
        found = False
        for app_conf in apps.get_app_configs():
            try:
                object_model = app_conf.get_model(model_name)
                found = True
                break  # stop as soon as it is found
            except LookupError:
                pass
        if not found:
            return JsonResponse({'message': 'bad model name'}, status=400)

        q = get_object_or_404(object_model, pk=id)

        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']

        notes = Notes.objects.create(
            content_object=q
        )

        serializer = NotesSerializer(notes)
        return JsonResponse(serializer.data)

    def update(self, request, pk):
        q = get_object_or_404(Notes, pk=pk)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        data['update_date'] = Now()

        serializer = NotesSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(Notes, pk=pk)
        q.delete()
        return HttpResponse(status=204)


@extend_schema(
    operation_id='export_to_zenodo',
    description="Export metadata to zenodo",
    tags=["Zenodo"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Investigation ID',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_to_zenodo(request, id):
    """SUMMARY LINE

    Export to Zenodo
    """
    # Check

    inv = get_object_or_404(Investigation, pk=id)

    # Authen
    client = hvac.Client(url='http://vault:8200')
    u = User.objects.get(username=request.user.username)

    clientInfo = client.auth.userpass.login(
        username=u.username,
        password=u.password.split("$")[-1],
    )
    vault_id = clientInfo['auth']['entity_id']

    vaultToken = clientInfo['auth']['client_token']
    client = hvac.Client(url='http://vault:8200',
                         token=vaultToken)
    try:
        read_response = client.secrets.kv.read_secret_version(
            path=vault_id + "/" + 'Zenodo')
    except Exception:
        return HttpResponse(status=404)

    ACCESS_TOKEN = ""

    for i in read_response['data']['data']:
        if i['ItemNameValue'] == "token":
            ACCESS_TOKEN = i['ItemValueValue']

    if ACCESS_TOKEN == "":
        return HttpResponse(status=404)
    else:
        r = requests.get('https://sandbox.zenodo.org/api/deposit/depositions',
                         params={'access_token': ACCESS_TOKEN})
        if r.ok:
            user = User.objects.get(username=request.user.username)
            headers = {"Content-Type": "application/json"}
            params = {'access_token': ACCESS_TOKEN}
            data = {
                'metadata': {
                    'title': inv.title,
                    'upload_type': 'dataset',
                    'description': inv.description,
                    'language': 'eng',
                    'creators': [{'name': user.get_full_name(),
                                  'affiliation': 'Zenodo'}]
                }
            }
            r = requests.post(
                'https://sandbox.zenodo.org/api/deposit/depositions',
                params=params,
                json={},
                data=json.dumps(data),
                headers=headers)

            ExternalID.objects.create(
                content_object=inv,
                db=Database.objects.get(name="Zenodo"),
                external_id=r.json()
            )

            return JsonResponse(r.json(), status=200)
        else:
            return JsonResponse(r.json(), status=404)

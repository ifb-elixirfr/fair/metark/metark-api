import json

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from apps.isa.models import Investigation, Study


class TestWebAnalytic(TestCase):
    def setUp(self):
        self.client = Client()

        # Create a super user to add a new user
        User.objects.create_user(
            username='superUser',
            first_name='Super',
            last_name='User',
            email='superUser@mail.com',
            password='djangoPWD',
            is_superuser=True,
            is_staff=True
        )
        logged_in = self.client.login(username="superUser",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        # Create a new user
        url = reverse('RegisterManagement')
        response = self.client.post(url, json.dumps({
            "username": "uproject",
            "password": "djangoPWD",
            "password2": "djangoPWD",
            "email": "userProject@mail.com",
            "first_name": "User",
            "last_name": "Project"
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        u = User.objects.get(username="uproject")
        u.is_superuser = True
        u.is_staff = True
        u.save()

        self.client.logout()

        # Get a token access
        logged_in = self.client.login(username="uproject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        url = reverse('token_obtain_pair')
        response = self.client.post(url, {'username': 'uproject',
                                          'password': 'djangoPWD'},
                                    format='json')
        self.assertEqual(response.status_code, 200)

        self.assertTrue('access' in response.data)
        tokenID = response.data['access']

        url = reverse('knox_login')
        response = self.client.post(url,
                                    HTTP_AUTHORIZATION='Token ' + tokenID)
        token = response.data['token']
        self.token = response.data['token']
        self.client = Client(HTTP_AUTHORIZATION='Token ' + token)

    def test_isa(self):
        ############################
        # Investigation
        ############################
        url = reverse("InvestigationDetail_form")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("InvestigationDetail_create")
        response = self.client.post(url, json.dumps({
            "title": "string",
            "alias": "string",
            "description": "string"
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url, json.dumps({
            "title": "string",
        }), content_type="application/json")
        self.assertEqual(response.status_code, 400)

        investigation = Investigation.objects.first()

        url = reverse("InvestigationDetail", kwargs={'pk': investigation.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.put(url, json.dumps({
            "title": "Update",
            "alias": "Update",
            "description": "Update"
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        response = self.client.put(url, json.dumps({
            "title": "Update",
        }), content_type="application/json")
        self.assertEqual(response.status_code, 400)

        url = reverse("InvestigationDetail_listStudy",
                      kwargs={'pk': investigation.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("InvestigationDetail_resume",
                      kwargs={'pk': investigation.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("InvestigationDetail_list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url, {"tag": "tag"},
                                   HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, 200)

        ############################
        # Study
        ############################

        url = reverse("StudyManagement_invList",
                      kwargs={'pk': investigation.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("StudyManagement_create")
        response = self.client.post(url, json.dumps({
            "title": "string",
            "investigation": str(investigation.pk)
        }), content_type="application/json")
        self.assertEqual(response.status_code, 400)

        response = self.client.post(url, json.dumps({
            "title": "string",
            "alias": "string",
            "description": "string",
            "investigation": str(investigation.pk)
        }), content_type="application/json")
        self.assertEqual(response.status_code, 201)

        study = Study.objects.first()
        # Get
        url = reverse("StudyManagement",
                      kwargs={'pk': study.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Update
        response = self.client.put(url, json.dumps({
            "title": "Update",
            "investigation": str(investigation.pk)
        }), content_type="application/json")
        self.assertEqual(response.status_code, 400)

        response = self.client.put(url, json.dumps({
            "title": "Update",
            "alias": "Update",
            "description": "Update",
            "investigation": str(investigation.pk)
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        ############################
        # Assay
        ############################

        url = reverse("AssayManagement_form")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        url = reverse("AssayManagement_listStudy",
                      kwargs={'pk': study.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        ############################
        # Delete area
        ############################
        # Study
        url = reverse("StudyManagement",
                      kwargs={'pk': study.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)

        # Investigation
        url = reverse("InvestigationDetail", kwargs={'pk': investigation.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)

# Generated by Django 4.1.5 on 2023-04-13 06:52

import apps.isa.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("contenttypes", "0002_remove_content_type_name"),
        ("isa", "0004_investigation_creationsource_study_partners"),
    ]

    operations = [
        migrations.CreateModel(
            name="ToDoList",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("object_id", models.TextField()),
                (
                    "content_type",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="content_type_todo_list_id",
                        to="contenttypes.contenttype",
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="ToDoItem",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("title", models.CharField(help_text="Item title", max_length=100)),
                (
                    "description",
                    models.TextField(
                        blank=True, help_text="Item description", null=True
                    ),
                ),
                (
                    "created_date",
                    models.DateTimeField(auto_now_add=True, help_text="Creation date"),
                ),
                (
                    "due_date",
                    models.DateTimeField(
                        default=apps.isa.models.one_week_hence, help_text="Due date"
                    ),
                ),
                ("checked", models.BooleanField(default=False, help_text="Item do ?")),
                (
                    "todo_list",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="isa.todolist"
                    ),
                ),
            ],
            options={
                "ordering": ["checked"],
            },
        ),
    ]

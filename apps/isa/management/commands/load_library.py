from django.core.management.base import BaseCommand

from apps.isa.models import Platform, Instrument, \
    LibrarySelection, LibrarySource, LibraryStrategy, LibraryLayout, \
    ChecksumMethod

PLATFORM = (
    ("LS454", "LS454"),
    ("ILLUMINA", "ILLUMINA"),
    ("PACBIO_SMRT", "PACBIO_SMRT"),
    ("ION_TORRENT", "ION_TORRENT"),
    ("CAPILLARY", "CAPILLARY"),
    ("OXFORD_NANOPORE", "OXFORD_NANOPORE"),
    ("BGISEQ", "BGISEQ"),
    ("DNBSEQ", "DNBSEQ"),
    ("VELA", "VELA"),
)

INSTRUMENT = (
    ("454 GS", "454 GS"),
    ("454 GS 20", "454 GS 20"),
    ("454 GS FLX", "454 GS FLX"),
    ("454 GS FLX+", "454 GS FLX+"),
    ("454 GS FLX Titanium", "454 GS FLX Titanium"),
    ("454 GS Junior", "454 GS Junior"),
    ("HiSeq X Five", "HiSeq X Five"),
    ("HiSeq X Ten", "HiSeq X Ten"),
    ("Illumina Genome Analyzer", "Illumina Genome Analyzer"),
    ("Illumina Genome Analyzer II", "Illumina Genome Analyzer II"),
    ("Illumina Genome Analyzer IIx", "Illumina Genome Analyzer IIx"),
    ("Illumina HiScanSQ", "Illumina HiScanSQ"),
    ("Illumina HiSeq 1000", "Illumina HiSeq 1000"),
    ("Illumina HiSeq 1500", "Illumina HiSeq 1500"),
    ("Illumina HiSeq 2000", "Illumina HiSeq 2000"),
    ("Illumina HiSeq 2500", "Illumina HiSeq 2500"),
    ("Illumina HiSeq 3000", "Illumina HiSeq 3000"),
    ("Illumina HiSeq 4000", "Illumina HiSeq 4000"),
    ("Illumina iSeq 100", "Illumina iSeq 100"),
    ("Illumina MiSeq", "Illumina MiSeq"),
    ("Illumina MiniSeq", "Illumina MiniSeq"),
    ("Illumina NovaSeq 6000", "Illumina NovaSeq 6000"),
    ("NextSeq 500", "NextSeq 500"),
    ("NextSeq 550", "NextSeq 550"),
    ("PacBio RS", "PacBio RS"),
    ("PacBio RS II", "PacBio RS II"),
    ("Sequel", "Sequel"),
    ("Ion Torrent PGM", "Ion Torrent PGM"),
    ("Ion Torrent Proton", "Ion Torrent Proton"),
    ("Ion Torrent S5", "Ion Torrent S5"),
    ("Ion Torrent S5 XL", "Ion Torrent S5 XL"),
    ("AB 3730xL Genetic Analyzer", "AB 3730xL Genetic Analyzer"),
    ("AB 3730 Genetic Analyzer", "AB 3730 Genetic Analyzer"),
    ("AB 3500xL Genetic Analyzer", "AB 3500xL Genetic Analyzer"),
    ("AB 3500 Genetic Analyzer", "AB 3500 Genetic Analyzer"),
    ("AB 3130xL Genetic Analyzer", "AB 3130xL Genetic Analyzer"),
    ("AB 3130 Genetic Analyzer", "AB 3130 Genetic Analyzer"),
    ("AB 310 Genetic Analyzer", "AB 310 Genetic Analyzer"),
    ("MinION", "MinION"),
    ("GridION", "GridION"),
    ("PromethION", "PromethION"),
    ("BGISEQ-500", "BGISEQ-500"),
    ("DNBSEQ-T7", "DNBSEQ-T7"),
    ("DNBSEQ-G400", "DNBSEQ-G400"),
    ("DNBSEQ-G50", "DNBSEQ-G50"),
    ("DNBSEQ-G400 FAST", "DNBSEQ-G400 FAST"),
    ("Sentosa SQ ViroKey FLEX", "Sentosa SQ ViroKey FLEX"),
    ("Applied Biosystems SeqStudio Genetic Analyzer",
     "Applied Biosystems SeqStudio Genetic Analyzer"),
    ("unspecified", "unspecified")
)

LIBRARY_SELECTION = (
    ("RANDOM", "RANDOM"),
    ("PCR", "PCR"),
    ("RANDOM PCR", "RANDOM PCR"),
    ("RT-PCR", "RT-PCR"),
    ("HMPR", "HMPR"),
    ("MF", "MF"),
    ("repeat fractionation", "repeat fractionation"),
    ("size fractionation", "size fractionation"),
    ("MSLL", "MSLL"),
    ("cDNA", "cDNA"),
    ("cDNA_randomPriming", "cDNA_randomPriming"),
    ("cDNA_oligo_dT", "cDNA_oligo_dT"),
    ("PolyA", "PolyA"),
    ("Oligo-dT", "Oligo-dT"),
    ("Inverse rRNA", "Inverse rRNA"),
    ("Inverse rRNA selection", "Inverse rRNA selection"),
    ("ChIP", "ChIP"),
    ("ChIP-Seq", "ChIP-Seq"),
    ("MNase", "MNase"),
    ("DNase", "DNase"),
    ("Hybrid Selection", "Hybrid Selection"),
    ("Reduced Representation", "Reduced Representation"),
    ("Restriction Digest", "Restriction Digest"),
    ("5-methylcytidine antibody", "5-methylcytidine antibody"),
    ("MBD2 protein methyl-CpG binding domain",
     "MBD2 protein methyl-CpG binding domain"),
    ("CAGE", "CAGE"),
    ("RACE", "RACE"),
    ("MDA", "MDA"),
    ("padlock probes capture method", "padlock probes capture method"),
    ("other", "other"),
    ("unspecified", "unspecified"),)

LIBRARY_SOURCE = (
    ("GENOMIC", "GENOMIC"),
    ("GENOMIC SINGLE CELL", "GENOMIC SINGLE CELL"),
    ("TRANSCRIPTOMIC", "TRANSCRIPTOMIC"),
    ("TRANSCRIPTOMIC SINGLE CELL", "TRANSCRIPTOMIC SINGLE CELL"),
    ("METAGENOMIC", "METAGENOMIC"),
    ("METATRANSCRIPTOMIC", "METATRANSCRIPTOMIC"),
    ("SYNTHETIC", "SYNTHETIC"),
    ("VIRAL RNA", "VIRAL RNA"),
    ("OTHER", "OTHER"),)

LIBRARY_STRATEGY = (
    ("WGS", "WGS"),
    ("WGA", "WGA"),
    ("WXS", "WXS"),
    ("RNA-Seq", "RNA-Seq"),
    ("ssRNA-seq", "ssRNA-seq"),
    ("miRNA-Seq", "miRNA-Seq"),
    ("ncRNA-Seq", "ncRNA-Seq"),
    ("FL-cDNA", "FL-cDNA"),
    ("EST", "EST"),
    ("Hi-C", "Hi-C"),
    ("ATAC-seq", "ATAC-seq"),
    ("WCS", "WCS"),
    ("RAD-Seq", "RAD-Seq"),
    ("CLONE", "CLONE"),
    ("POOLCLONE", "POOLCLONE"),
    ("AMPLICON", "AMPLICON"),
    ("CLONEEND", "CLONEEND"),
    ("FINISHING", "FINISHING"),
    ("ChIP-Seq", "ChIP-Seq"),
    ("MNase-Seq", "MNase-Seq"),
    ("DNase-Hypersensitivity", "DNase-Hypersensitivity"),
    ("Bisulfite-Seq", "Bisulfite-Seq"),
    ("CTS", "CTS"),
    ("MRE-Seq", "MRE-Seq"),
    ("MeDIP-Seq", "MeDIP-Seq"),
    ("MBD-Seq", "MBD-Seq"),
    ("Tn-Seq", "Tn-Seq"),
    ("VALIDATION", "VALIDATION"),
    ("FAIRE-seq", "FAIRE-seq"),
    ("SELEX", "SELEX"),
    ("RIP-Seq", "RIP-Seq"),
    ("ChIA-PET", "ChIA-PET"),
    ("Synthetic-Long-Read", "Synthetic-Long-Read"),
    ("Targeted-Capture", "Targeted-Capture"),
    ("Tethered Chromatin Conformation Capture",
     "Tethered Chromatin Conformation Capture"),
    ("OTHER", "OTHER"),
)

ENDED_TYPE = (
    ('single', 'single'),
    ('paired', 'paired')
)

ANSWER_TYPE = (
    (True, 'yes'),
    (False, 'no')
)


class Command(BaseCommand):
    help = 'Load platforms and instruments'

    def handle(self, *args, **kwargs):
        for i in PLATFORM:
            Platform.objects.create(
                name=i[0]
            )

        for i in INSTRUMENT:
            Instrument.objects.create(
                name=i[0]
            )

        for i in LIBRARY_SELECTION:
            LibrarySelection.objects.create(
                name=i[0]
            )

        for i in LIBRARY_SOURCE:
            LibrarySource.objects.create(
                name=i[0]
            )

        for i in LIBRARY_STRATEGY:
            LibraryStrategy.objects.create(
                name=i[0]
            )

        for i in ENDED_TYPE:
            LibraryLayout.objects.create(
                name=i[0]
            )

        ChecksumMethod.objects.create(name="MD5")

        self.stdout.write("Done")

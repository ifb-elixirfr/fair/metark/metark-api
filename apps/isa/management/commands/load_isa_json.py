import json
import os
from datetime import datetime

from django.core.management.base import BaseCommand

from apps.isa._functions import createToDoItemsInv
from apps.isa.models import Investigation, Study, Publication, PersonContact, \
    Comment, Protocol, Assay, MeasurementType, DataFile, TypeFile, \
    DesignType, Factor, Type, ToDoList


class Command(BaseCommand):
    help = 'Reload regions data'

    def handle(self, *args, **kwargs):
        # Opening JSON file
        f = open('apps/isa/static/data/exampleISA.json')

        # Create dict
        data = json.load(f)

        # Investigation
        i = Investigation.objects.create(
            title=data['title'],
            alias=data['identifier'],
            description=data['description'],
            submission_date=datetime.strptime(data['submissionDate'],
                                              '%Y-%m-%d'),
            public_release_date=datetime.strptime(data['publicReleaseDate'],
                                                  '%Y-%m-%d'),
        )

        tdl = ToDoList.objects.create(
            content_object=i
        )
        createToDoItemsInv(tdl)

        for p in data['publications']:
            pub = Publication()
            pub.pubmed_id = p['pubMedID']
            pub.doi = p['doi']
            pub.status = p['status']['annotationValue']
            pub.title = p['title']
            pub.author_list = p['authorList']
            pub.save()
            i.publications.add(pub)

        for p in data['people']:
            people = PersonContact()
            people.first_name = p['firstName']
            people.last_name = p['lastName']
            people.mid_initial = p['midInitials']
            people.email = p['email']
            people.phone = p['phone']
            people.fax = p['fax']
            people.fax = p['fax']
            people.address = p['address']
            people.affiliation = p['affiliation']
            people.roles = '; '.join([r['annotationValue'] for r in
                                      p['roles']])
            people.save()
            i.contacts.add(people)

        for c in data['comments']:
            comment = Comment.objects.create(
                value=c['value'],
                name=c['name'],
            )
            i.comments.add(comment)

        for study in data['studies']:
            s = Study()
            s.alias = study['identifier']
            s.title = study['title']
            s.description = study['description']
            s.submission_date = datetime.strptime(study['submissionDate'],
                                                  '%Y-%m-%d')
            s.public_release_date = datetime.strptime(
                study['publicReleaseDate'], '%Y-%m-%d')
            s.investigation = i
            s.save()
            for fac in study['factors']:
                factor = Factor.objects.get_or_create(
                    name=fac['factorName'],
                    type=Type.objects.get_or_create(
                        term_accession=fac['factorType']['termAccession'],
                        term_source=fac['factorType']['termSource'],
                        annotation_value=fac['factorType']['annotationValue'],
                    )[0]
                )[0]
                s.factors.add(factor)
            for c in study['comments']:
                comment = Comment.objects.create(
                    value=c['value'],
                    name=c['name'],
                )
                s.comments.add(comment)
            for p in study['publications']:
                pub = Publication()
                pub.pubmed_id = p['pubMedID']
                pub.doi = p['doi']
                pub.status = p['status']['annotationValue']
                pub.title = p['title']
                pub.author_list = p['authorList']
                pub.save()
                s.publications.add(pub)
            for dt in study['studyDesignDescriptors']:
                design = DesignType.objects.get_or_create(
                    type=dt['annotationValue'],
                    term_accession=dt['termAccession'],
                    term_source=dt['termSource']
                )
                s.design_types.add(design[0])
            for p in study['people']:
                people = PersonContact()
                people.first_name = p['firstName']
                people.last_name = p['lastName']
                people.mid_initial = p['midInitials']
                people.email = p['email']
                people.phone = p['phone']
                people.fax = p['fax']
                people.fax = p['fax']
                people.address = p['address']
                people.affiliation = p['affiliation']
                people.roles = '; '.join([r['annotationValue']
                                          for r in p['roles']])
                people.save()
                s.contacts.add(people)
            count = 1
            for p in study['protocols']:
                protocol = Protocol()
                protocol.name = p['name']
                protocol.description = p['description']
                protocol.uri = p['uri']
                protocol.version = p['version']
                protocol.parameters_name = '; '.join([i['parameterName'][
                                                          'annotationValue'
                                                      ] for i in
                                                      p['parameters']
                                                      ])
                protocol.components_name = '; '.join(p['components'])
                protocol.type = p['protocolType']['annotationValue']
                protocol.procedure_step = count
                count += 1
                protocol.study = s
                protocol.save()
            for a in study['assays']:
                assay = Assay()
                assay.study = s
                assay.measurement_type = MeasurementType.objects.get_or_create(
                    name=a['measurementType']['annotationValue'])[0]
                assay.save()
                for file in a['dataFiles']:
                    f = DataFile()
                    f.assay = assay
                    f.name = file['name']
                    f.alias = os.path.basename(file['name']).split(".")[0]
                    f.type = TypeFile.objects.get_or_create(
                        name=file['type'])[0]
                    f.save()

                    for c in file['comments']:
                        f.comments.add(Comment.objects.create(
                                            value=c['value'],
                                            name=c['name'],
                                        ))

        #  'processSequence', 'unitCategories',
        #  'characteristicCategories', 'assays', 'filename',
        # '@id', 'materials',
        self.stdout.write("Done")

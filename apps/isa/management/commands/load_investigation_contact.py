import csv

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from apps.isa.models import Investigation, Contact, Role


class Command(BaseCommand):
    help = 'Reload regions data'

    def handle(self, *args, **kwargs):
        with open('apps/isa/static/data/work_on.csv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                i_temp = Investigation.objects.get(title=row[0])
                user_temp = User.objects.get(username=row[1])
                corresponding_contact = True if row[2] == "True" else False

                c = Contact.objects.create(
                    user=user_temp,
                    corresponding_contact=corresponding_contact,
                    role=Role.objects.get_or_create(name=row[3])[0])
                i_temp.collaborators.add(c)

        self.stdout.write("Done")

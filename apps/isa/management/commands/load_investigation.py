import csv

from django.core.management.base import BaseCommand

from apps.isa._functions import createToDoItemsInv
from apps.isa.models import Investigation, ToDoList


class Command(BaseCommand):
    help = 'Reload regions data'

    def handle(self, *args, **kwargs):
        with open('apps/isa/static/data/investigation.csv',
                  newline='', encoding='ISO-8859-1') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
            next(spamreader, None)
            for row in spamreader:
                inv = Investigation.objects.create(
                    title=row[0],
                    alias=row[1],
                    description=row[2])

                tdl = ToDoList.objects.create(
                    content_object=inv
                )
                createToDoItemsInv(tdl)

        self.stdout.write("Done")

from django.contrib import admin

from apps.metadata.models import Checklist, Database, Field, Metadata


@admin.register(Database)
class databaseAdmin(admin.ModelAdmin):
    save_on_top = True


@admin.register(Checklist)
class checklistAdmin(admin.ModelAdmin):
    save_on_top = True


@admin.register(Field)
class fieldAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = [f.name for f in Field._meta.fields if f.name != "id"]
    list_filter = ['checklist']
    search_fields = ["name"]


@admin.register(Metadata)
class metadataAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = [f.name for f in Metadata._meta.fields
                    if f.name not in ["id", "table"]]

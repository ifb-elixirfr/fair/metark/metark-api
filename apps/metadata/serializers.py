import json

from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from apps.metadata.models import Database, Checklist, Metadata, Field


class DatabaseSerializer(ModelSerializer):
    class Meta:
        model = Database
        fields = "__all__"


class ChecklistSerializer(ModelSerializer):
    database = SerializerMethodField()

    class Meta:
        model = Checklist
        fields = "__all__"

    def get_database(self, arg_obj):
        selected_options = arg_obj.database
        return DatabaseSerializer(selected_options).data


class MetadataSerializer(ModelSerializer):
    checklist_detail = SerializerMethodField()

    class Meta:
        model = Metadata
        fields = "__all__"

    def get_checklist_detail(self, arg_obj):
        selected_options = arg_obj.checklist
        return ChecklistSerializer(selected_options).data


class MetadataSerializerNoTable(ModelSerializer):
    checklist_detail = SerializerMethodField()
    nb_items = SerializerMethodField()

    class Meta:
        model = Metadata
        exclude = ['table', 'metadata']

    def get_checklist_detail(self, arg_obj):
        selected_options = arg_obj.checklist
        return ChecklistSerializer(selected_options).data

    def get_nb_items(self, arg_obj):
        if isinstance(arg_obj.table, str):
            selected_options = json.loads(arg_obj.table)
        else:
            selected_options = arg_obj.table

        try:
            return len(selected_options)
        except Exception:
            return 0


class MetadataSerializerCompletness(ModelSerializer):
    class Meta:
        model = Metadata
        fields = ['mandatory', 'recommended', 'optional']


class FieldSerializer(ModelSerializer):
    class Meta:
        model = Field
        fields = "__all__"

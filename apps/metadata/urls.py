from django.urls import path

from apps.metadata.apis import DatabaseManagement, ChecklistManagement, \
    MetadataManagement, FieldManagement, export_xml_submission, \
    export_xml_project, export_xml_run, export_xml_experiment, \
    export_xml_sample, export_metadata_format

urlpatterns = [
    path('api/database/list/',
         DatabaseManagement.as_view({'get': 'list'}),
         name="DatabaseManagement"),

    path('api/checklist/list/from/database/<int:pk>/',
         ChecklistManagement.as_view({'get': 'list_database'}),
         name="ChecklistManagement"),

    path('api/metadata/create/',
         MetadataManagement.as_view({'post': 'create'}),
         name="MetadataManagement"),

    path('api/metadata/create/and/complete/',
         MetadataManagement.as_view({'post': 'create_and_complete'}),
         name="MetadataManagement"),

    path('api/metadata/update/multiple/',
         MetadataManagement.as_view({'put': 'update_multiple'}),
         name="MetadataManagement"),

    path('api/metadata/<int:pk>/',
         MetadataManagement.as_view({'get': 'retrieve',
                                     'put': 'update'}),
         name="MetadataManagement"),

    path('api/metadata/<int:id>/export/file/<str:formatFile>/',
         export_metadata_format, name="export_metadata_format"),

    path('api/metadata/check/values/of/<int:pk>/',
         MetadataManagement.as_view({'get': 'check_values'}),
         name="MetadataManagement"),

    path('api/metadata/get/completness/<int:pk>/',
         MetadataManagement.as_view({'get': 'get_completness'}),
         name="MetadataManagement"),

    path('api/metadata/by/study/<slug:pk>/',
         MetadataManagement.as_view({'get': 'list_by_study'}),
         name="MetadataManagement"),

    path('api/metadata/by/study/<slug:pk>/<str:status>/',
         MetadataManagement.as_view({'get': 'list_by_study_and_status'}),
         name="MetadataManagement"),

    path('api/metadata/by/study/<slug:pk>/<str:status>/to/<str:db>/',
         MetadataManagement.as_view(
             {'get': 'retrieve_by_study_and_status_and_db'}),
         name="MetadataManagement"),

    path('api/field/from/checklist/<int:pk_checklist>/',
         FieldManagement.as_view({'get': 'list_from_checklist'}),
         name="FieldManagement"),

    path('api/get/xml/submission/file/',
         export_xml_submission,
         name="export_xml_submission"),
    path('api/get/xml/project/file/from/metadata/<int:id>/',
         export_xml_project,
         name="export_xml_project"),
    path('api/get/xml/run/file/from/metadata/<int:id>/',
         export_xml_run,
         name="export_xml_run"),
    path('api/get/xml/experiment/file/from/metadata/<int:id>/',
         export_xml_experiment,
         name="export_xml_experiment"),
    path('api/get/xml/sample/file/from/metadata/<int:id>/',
         export_xml_sample,
         name="export_xml_sample"),

]

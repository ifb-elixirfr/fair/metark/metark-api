import csv
import json
import re
import tempfile
from tempfile import TemporaryFile
from xml.dom import minidom

from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter, \
    OpenApiResponse
from rest_framework import status
from rest_framework.decorators import permission_classes, api_view
from rest_framework.exceptions import ParseError
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rest_framework.viewsets import ModelViewSet

from apps.isa.models import Study, Assay, DataFile
from apps.metadata.models import Database, Checklist, Field, Metadata
from apps.metadata.serializers import DatabaseSerializer, \
    ChecklistSerializer, MetadataSerializer, FieldSerializer, \
    MetadataSerializerCompletness, MetadataSerializerNoTable

from django.utils import timezone

from django.db.models import Case, When, Value


XML_NODE_TYPE = {
    "1": "ELEMENT_NODE",
    "2": "ATTRIBUTE_NODE",
    "3": "TEXT_NODE",
    "4": "CDATA_SECTION_NODE",
    "5": "ENTITY_REFERENCE_NODE",
    "6": "ENTITY_NODE",
    "7": "PROCESSING_INSTRUCTION_NODE",
    "8": "COMMENT_NODE",
    "9": "DOCUMENT_NODE",
    "10": "DOCUMENT_TYPE_NODE",
    "11": "DOCUMENT_FRAGMENT_NODE",
    "12": "NOTATION_NODE"
}


def check_completness(json_inter, fields):
    print(json_inter)
    mandatory = 0
    recommended = 0
    optional = 0

    if len(json_inter) != 0:
        col_manda = 0
        col_reco = 0
        col_opt = 0

        manda = 0
        reco = 0
        opt = 0

        for f in fields:
            if f.requirement == 'mandatory':
                col_manda += 1
            elif f.requirement == 'recommended':
                col_reco += 1
            elif f.requirement == 'optional':
                col_opt += 1

            for item in json_inter.values():
                if item.get(f.name) is not None:
                    if f.requirement == 'mandatory':
                        manda += 1
                    if f.requirement == 'recommended':
                        reco += 1
                    if f.requirement == 'optional':
                        opt += 1
                # todo : ENA exception add to ENA app
                elif f.name == "INSERT_SIZE" and \
                        item.get('LIBRARY_LAYOUT') == "single":
                    manda += 1

        if col_manda != 0:
            mandatory = (manda / (
                    len(json_inter) * col_manda)) * 100

        if col_reco != 0:
            recommended = (reco / (
                    len(json_inter) * col_reco)) * 100

        if col_opt != 0:
            optional = (opt / (
                    len(json_inter) * col_opt)) * 100

    return {
        'mandatory': round(mandatory),
        'recommended': round(recommended),
        'optional': round(optional)
    }


@extend_schema(
    operation_id="api_manage_database",
    description="Manage database",
    tags=["Metadata"],
)
class FieldManagement(ModelViewSet):
    serializer_class = FieldSerializer
    queryset = Field.objects.all()

    def list_from_checklist(self, request, pk_checklist=None):
        c = get_object_or_404(Checklist, pk=pk_checklist)
        f = Field.objects.filter(checklist=c)
        if c.database.name == "ENA":
            f = f | Field.objects.filter(checklist__name="MIP_ENA")

        serializer = FieldSerializer(f.order_by(
            Case(
                When(requirement="mandatory", then=Value(0)),
                When(requirement="recommended", then=Value(1)),
                When(requirement="optional", then=Value(2))
            ), "name"
        ), many=True)

        temp = list()
        data = serializer.data

        for i in data:
            temp.append(i)
            if i['unit'] is not None and i['unit'] != "":
                new = dict(i)
                new['name'] = new['name'] + " (unit)"
                temp.append(new)

        return Response(temp)


@extend_schema(
    operation_id="api_manage_database",
    description="Manage database",
    tags=["Metadata"],
)
@permission_classes([IsAuthenticated])
class DatabaseManagement(ModelViewSet):
    serializer_class = DatabaseSerializer
    queryset = Database.objects.all()

    def list(self, request, ):
        serializer = DatabaseSerializer(Database.objects.all(), many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id="api_manage_checklist",
    description="Manage database",
    tags=["Metadata"],
)
class ChecklistManagement(ModelViewSet):
    serializer_class = ChecklistSerializer
    queryset = Checklist.objects.all()

    def list_database(self, request, pk=None):
        db = get_object_or_404(Database, pk=pk)
        items = Checklist.objects.filter(database=db)
        serializer = ChecklistSerializer(items, many=True)
        return Response(serializer.data)


def get_list_fields(checklist):
    list_fields = Field.objects.filter(checklist=checklist)
    if checklist.database.name == "ENA":
        list_fields = list_fields | Field.objects.filter(
            checklist__name="MIP_ENA")

    return list_fields.order_by(
        Case(
            When(requirement="mandatory", then=Value(0)),
            When(requirement="recommended", then=Value(1)),
            When(requirement="optional", then=Value(2))
        ), "name"
    )


@extend_schema(
    operation_id="api_manage_metadata",
    description="Manage database",
    tags=["Metadata"],
)
@permission_classes([IsAuthenticated])
class MetadataManagement(ModelViewSet):
    serializer_class = MetadataSerializer
    queryset = Metadata.objects.all()

    def update_multiple(self, request):
        """
        Update a existing metadata table with multiples values

        :param request:
        :return:
        """
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']

        get_object_or_404(Study, pk=data['study'])
        m = get_object_or_404(Metadata, pk=data['metadata'])

        assayItems = Assay.objects.filter(study__pk=data['study'],
                                          alias__in=data['samples'].keys())
        list_fields = get_list_fields(m.checklist)
        for assay in assayItems:
            inter = {}
            for lf in list_fields:
                inter[lf.name] = None
                if lf.unit is not None and lf.unit != "":
                    inter[lf.name + " (unit)"] = None
            if m.table is None:
                m.table = dict()

            m.table[assay.id] = inter
            m.save()

            id_assay = assay.pk
            if id_assay not in m.table.keys():
                id_assay = str(id_assay)

            if id_assay in m.table.keys():
                for v in data['samples'][assay.alias]:
                    m.table[id_assay][v['tag']] = v['value']
                    if 'unit' in v.keys():
                        m.table[id_assay][v['tag'] + ' (unit)'] = v['unit']

            m.save()

        check_result = check_completness(m.table, list_fields)
        m.mandatory = check_result['mandatory']
        m.recommended = check_result['recommended']
        m.optional = check_result['optional']
        m.save()

        return HttpResponse(status=204)

    def create_and_complete(self, request):
        """
        Create metadata table for a study, add existing assays and complete
        values

        :return: Metadata serializer
        """
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']

        get_object_or_404(Study, pk=data['study'])
        md = {
            'checklist': Checklist.objects.get(
                original_id=data['checklist']).pk,
            'study': data['study'],
            'created_by': request.user.username,
            'last_modification_by': request.user.username,
            'status': 'pending'
        }

        serializer = MetadataSerializer(data=md)

        if serializer.is_valid():
            m = serializer.save()
            list_fields = get_list_fields(m.checklist)
            assayItems = Assay.objects.filter(study=m.study)
            for assay in assayItems:
                inter = {}
                for lf in list_fields:
                    inter[lf.name] = None
                    if lf.unit is not None and lf.unit != "":
                        inter[lf.name + " (unit)"] = None
                if m.table is None:
                    m.table = dict()

                m.table[assay.id] = inter
                m.save()

            for key, value in data['samples'].items():
                a = Assay.objects.get(
                    study__pk=data['study'],
                    alias=key)

                id_assay = a.pk
                if id_assay not in m.table.keys():
                    id_assay = str(id_assay)

                if id_assay in m.table.keys():
                    # to do : check if in checklist. Maybe a try except
                    for v in value:
                        m.table[id_assay][v['tag']] = v['value']
                        if 'unit' in v.keys():
                            m.table[id_assay][v['tag'] + ' (unit)'] = v['unit']

                m.save()

            check_result = check_completness(m.table, list_fields)
            m.mandatory = check_result['mandatory']
            m.recommended = check_result['recommended']
            m.optional = check_result['optional']
            m.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @csrf_exempt
    def create(self, request):
        data = {
            'checklist': request.data.get('checklist'),
            'study': request.data.get('study'),
            'created_by': request.user.username,
            'last_modification_by': request.user.username,
            'short_description': request.data.get('short_description'),
            'status': 'pending'
        }
        serializer = MetadataSerializer(data=data)

        if serializer.is_valid():
            m = serializer.save()

            list_fields = get_list_fields(m.checklist)
            assayItems = Assay.objects.filter(study=m.study)
            for assay in assayItems:
                inter = {}
                for lf in list_fields:
                    inter[lf.name] = None
                    if lf.unit is not None and lf.unit != "":
                        inter[lf.name + " (unit)"] = None
                if m.table is None:
                    m.table = dict()

                m.table[assay.id] = inter
                m.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def list_by_study(self, request, pk=None):
        study = get_object_or_404(Study, pk=pk)
        item = (Metadata.objects.filter(study=study)
                .order_by("checklist__database__name"))
        serializer = MetadataSerializerNoTable(item, many=True)
        return Response(serializer.data)

    def list_by_study_and_status(self, request, status, pk=None):
        study = get_object_or_404(Study, pk=pk)
        item = Metadata.objects.filter(study=study, status=status)
        serializer = MetadataSerializerNoTable(item, many=True)
        return Response(serializer.data)

    def retrieve_by_study_and_status_and_db(self, request,
                                            status, db, pk=None):
        study = get_object_or_404(Study, pk=pk)
        database = get_object_or_404(Database, name=db)
        item = get_object_or_404(Metadata, study=study, status=status,
                                 checklist__database=database)
        serializer = MetadataSerializer(item)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(Metadata, pk=pk)
        serializer = MetadataSerializer(item)
        return Response(serializer.data)

    @csrf_exempt
    def update(self, request, pk=None):
        metadata_element = get_object_or_404(Metadata, pk=pk)
        fields = Field.objects.filter(checklist=metadata_element.checklist)
        if metadata_element.checklist.database.name == "ENA":
            fields = fields | Field.objects.filter(checklist__name="MIP_ENA")

        json_inter = request.data.get('table')
        check_result = check_completness(json_inter, fields)

        data = {
            'table': json_inter,
            'metadata': request.data.get('metadata'),
            'last_modification_date': timezone.now(),
            'last_modification_by': request.user.username,
            'mandatory': check_result['mandatory'],
            'recommended': check_result['recommended'],
            'optional': check_result['optional'],
            'study': metadata_element.study.pk,
            'created_by': metadata_element.created_by,
            'checklist': metadata_element.checklist.pk
        }

        serializer = MetadataSerializer(metadata_element, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)

        print(serializer.errors)
        return JsonResponse(serializer.errors, status=400)

    def get_completness(self, request, pk=None):
        metadata_element = get_object_or_404(Metadata, pk=pk)
        fields = Field.objects.filter(checklist=metadata_element.checklist)

        if metadata_element.checklist.database.name == "ENA":
            fields = fields | Field.objects.filter(checklist__name="MIP_ENA")

        check_result = check_completness(metadata_element.table, fields)
        serializer = MetadataSerializerCompletness(check_result)
        return JsonResponse(serializer.data)

    def check_values(self, request, pk=None):
        metadata_element = get_object_or_404(Metadata, pk=pk)
        fields = Field.objects.filter(checklist=metadata_element.checklist)

        if metadata_element.checklist.database.name == "ENA":
            fields = fields | Field.objects.filter(checklist__name="MIP_ENA")

        dictField = dict()
        for f in fields:
            dictField[f.name] = {
                'requirement': f.requirement,
                'restriction': f.restriction,
                'format': f.format,
                'unit': f.unit
            }

        if isinstance(metadata_element.table, str):
            table = json.loads(metadata_element.table)
        else:
            table = metadata_element.table

        regex = r"\(unit\)$"

        resultCheck = dict()
        completness = dict()
        completness_all = dict()

        for key, line in table.items():
            tempCheck = dict()
            for name, value in line.items():
                error_flag = False
                if bool(re.search(regex, name)):
                    clearName = re.sub(regex, '', name).strip()

                    # check if data is available before check unit
                    if line[clearName] != "" and line[clearName] is not None:
                        listItem = dictField[clearName]['unit'].split("@")
                        if value not in listItem:
                            tempCheck[name] = 'Not in item unit list : ' + \
                                              ', '.join(listItem)
                            error_flag = True
                    name = clearName
                else:
                    if dictField[name]['requirement'] == "mandatory" and \
                            (value == "" or value is None):
                        # ENA specification
                        # todo : with a function in ENA app
                        if name == "INSERT_SIZE" and \
                                line['LIBRARY_LAYOUT'] == "single":
                            pass
                        else:
                            tempCheck[name] = 'mandatory'
                            error_flag = True

                    if value != "" and value is not None:
                        if dictField[name]['format'] == "TEXT_CHOICE_FIELD":
                            listItem = dictField[name][
                                'restriction'].split("@")
                            if value not in listItem:
                                tempCheck[name] = 'Not in item ' \
                                                  'restriction list'
                                error_flag = True
                        else:
                            if dictField[name]['restriction'] != "" and \
                                    dictField[name]['restriction'] is not None:
                                if not re.search(
                                        dictField[name]['restriction'],
                                        value):
                                    tempCheck[name] = 'Enforced restriction'
                                    error_flag = True
                    else:
                        if name == "INSERT_SIZE" and \
                                line['LIBRARY_LAYOUT'] == "single":
                            pass
                        else:
                            # Flagged because empty
                            error_flag = True

                if dictField[name]['requirement'] not in \
                        completness_all.keys():
                    completness_all[dictField[name]['requirement']] = 1
                    completness[dictField[name]['requirement']] = 0
                else:
                    completness_all[dictField[name]['requirement']] += 1

                if not error_flag:
                    completness[dictField[name]['requirement']] += 1

            if len(tempCheck) != 0:
                resultCheck[key] = tempCheck

        for k in completness_all.keys():
            completness[k] = completness[k] * 100 / completness_all[k]

        if len(resultCheck) != 0:
            return JsonResponse({'status': 'error',
                                 'results': resultCheck,
                                 'Completeness after check': completness})
        else:
            return JsonResponse({'status': 'ok',
                                 'Completeness after check': completness})


@extend_schema(
    operation_id='export_metadata_format',
    description="Export metadata",
    tags=["Metadata"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Metadata ID',
            type=int
        ),
        OpenApiParameter(
            name='formatFile',
            location=OpenApiParameter.PATH,
            enum=["csv", "tsv", ],
            description='Export format',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@csrf_exempt
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_metadata_format(request, id, formatFile):
    """SUMMARY LINE

    export_metadata_format
    """
    if request.method == 'GET':
        metadata_element = get_object_or_404(Metadata, pk=id)
        fields = get_list_fields(metadata_element.checklist)
        if formatFile not in ['csv', 'tsv']:
            raise ParseError("Error file format")

        if formatFile in ['csv', 'tsv']:
            f_temp = tempfile.NamedTemporaryFile(suffix='.'+formatFile,
                                                 prefix="export_"+formatFile,
                                                 delete=False,
                                                 mode='w+')
            if formatFile == "csv":
                delimiter = ";"
            else:
                delimiter = "\t"

            filewriter = csv.writer(f_temp, delimiter=delimiter)
            header = list(fields.values_list("name", flat=True))
            filewriter.writerow(["sampelPK"] + header)

            if isinstance(metadata_element.table, str):
                table = json.loads(metadata_element.table)
            else:
                table = metadata_element.table

            for key, line in table.items():
                tempLine = [key]
                for h in header:
                    tempLine.append(line[h])
                filewriter.writerow(tempLine)
            f_temp.seek(0)

            response = HttpResponse(f_temp.read(),
                                    content_type='text/'+formatFile
                                    )
            response[
                'Content-Disposition'] = \
                'attachment; filename=export_' + formatFile + '.' + formatFile
            return response


@extend_schema(
    operation_id='export_xml_submission',
    description="Generate ENA XML : submission",
    tags=["XML"],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@csrf_exempt
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_xml_submission(request):
    """SUMMARY LINE

    Generate ENA XML : submission
    """
    if request.method == 'GET':
        with TemporaryFile() as tmp:
            root = minidom.Document()

            SUBMISSION = root.createElement('SUBMISSION')
            root.appendChild(SUBMISSION)

            ACTIONS = root.createElement('ACTIONS')
            SUBMISSION.appendChild(ACTIONS)

            ACTION = root.createElement('ACTION')
            ACTIONS.appendChild(ACTION)

            ADD = root.createElement('ADD')
            ACTION.appendChild(ADD)

            xml_str = root.toprettyxml(indent="\t", encoding="UTF-8")
            tmp.write(xml_str)
            tmp.seek(0)
            response = HttpResponse(tmp.read(),
                                    content_type='application/xml'
                                    )
            response[
                'Content-Disposition'] = \
                'attachment; filename=submission.xml'
            return response


@extend_schema(
    operation_id='export_xml_project',
    description="Generate ENA XML : project",
    tags=["XML"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Metadata ID',
            type=int
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@csrf_exempt
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_xml_project(request, id):
    """SUMMARY LINE

    Generate ENA XML : project
    """

    q = get_object_or_404(Metadata, pk=id)
    study = q.study

    if request.method == 'GET':
        root = minidom.Document()

        # PROJECT_SET
        PROJECT_SET = root.createElement('PROJECT_SET')
        root.appendChild(PROJECT_SET)

        # PROJECT_SET > PROJECT
        PROJECT = root.createElement('PROJECT')
        PROJECT.setAttribute('alias', study.alias)
        PROJECT_SET.appendChild(PROJECT)

        # PROJECT_SET > PROJECT > TITLE
        TITLE = root.createElement('TITLE')
        TITLE.appendChild(root.createTextNode(study.title))
        PROJECT.appendChild(TITLE)

        # PROJECT_SET > PROJECT > DESCRIPTION
        DESCRIPTION = root.createElement('DESCRIPTION')
        DESCRIPTION.appendChild(root.createTextNode(study.description))
        PROJECT.appendChild(DESCRIPTION)

        # PROJECT_SET > PROJECT > SUBMISSION_PROJECT
        SUBMISSION_PROJECT = root.createElement('SUBMISSION_PROJECT')
        PROJECT.appendChild(SUBMISSION_PROJECT)

        # PROJECT_SET > PROJECT > SUBMISSION_PROJECT > SEQUENCING_PROJECT
        SEQUENCING_PROJECT = root.createElement('SEQUENCING_PROJECT')
        SUBMISSION_PROJECT.appendChild(SEQUENCING_PROJECT)

        xml_str = root.toprettyxml(indent="\t", encoding="UTF-8")

        with TemporaryFile() as tmp:
            tmp.write(xml_str)
            tmp.seek(0)
            response = HttpResponse(tmp.read(),
                                    content_type='application/xml'
                                    )
            response[
                'Content-Disposition'] = \
                'attachment; filename=project.xml'
            return response


@extend_schema(
    operation_id='export_xml_run',
    description="Generate ENA XML : run",
    tags=["XML"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Metadata ID',
            type=int
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@csrf_exempt
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_xml_run(request, id):
    """SUMMARY LINE

    Generate ENA XML : project
    """

    q = get_object_or_404(Metadata, pk=id)

    if request.method == 'GET':
        root = minidom.Document()

        # PROJECT_SET
        RUN_SET = root.createElement('RUN_SET')
        root.appendChild(RUN_SET)

        assayItems = Assay.objects.filter(study=q.study)

        for f in DataFile.objects.filter(assay__in=assayItems):
            # RUN_SET > RUN
            RUN = root.createElement('RUN')
            RUN.setAttribute('alias', f.alias)
            if f.center_name:
                RUN.setAttribute('center_name', f.center_name.name)
            RUN_SET.appendChild(RUN)

            # RUN_SET > RUN > EXPERIMENT_REF
            EXPERIMENT_REF = root.createElement('EXPERIMENT_REF')
            EXPERIMENT_REF.setAttribute('refname', f.assay.alias)
            RUN.appendChild(EXPERIMENT_REF)

            # RUN_SET > RUN > DATA_BLOCK
            DATA_BLOCK = root.createElement('DATA_BLOCK')
            RUN.appendChild(DATA_BLOCK)

            # RUN_SET > RUN > DATA_BLOCK > FILES
            FILES = root.createElement('FILES')
            DATA_BLOCK.appendChild(FILES)

            # RUN_SET > RUN > DATA_BLOCK > FILES > FILES
            FILE = root.createElement('FILE')
            FILE.setAttribute('filename', f.name)
            FILE.setAttribute('filetype', f.type.name)
            FILE.setAttribute('checksum_method', f.checksum_method.name)
            FILE.setAttribute('checksum', f.checksum)
            FILES.appendChild(FILE)

        xml_str = root.toprettyxml(indent="\t", encoding="UTF-8")

        with TemporaryFile() as tmp:
            tmp.write(xml_str)
            tmp.seek(0)
            response = HttpResponse(tmp.read(),
                                    content_type='application/xml'
                                    )
            response[
                'Content-Disposition'] = \
                'attachment; filename=run.xml'
            return response


@extend_schema(
    operation_id='export_xml_experiment',
    description="Generate ENA XML : experiment",
    tags=["XML"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Metadata ID',
            type=int
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@csrf_exempt
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_xml_experiment(request, id):
    """SUMMARY LINE

    Generate ENA XML : project
    """

    q = get_object_or_404(Metadata, pk=id)

    if request.method == 'GET':
        root = minidom.Document()

        # EXPERIMENT_SET
        EXPERIMENT_SET = root.createElement('EXPERIMENT_SET')
        root.appendChild(EXPERIMENT_SET)
        assayItems = Assay.objects.filter(study=q.study)

        for f in DataFile.objects.filter(assay__in=assayItems):
            # EXPERIMENT_SET > EXPERIMENT
            EXPERIMENT = root.createElement('EXPERIMENT')
            EXPERIMENT.setAttribute('alias', f.assay.alias)
            if f.assay.center_name:
                EXPERIMENT.setAttribute('center_name',
                                        f.assay.center_name.name)
            EXPERIMENT_SET.appendChild(EXPERIMENT)

            # EXPERIMENT_SET > EXPERIMENT > TITLE
            TITLE = root.createElement('TITLE')
            TITLE.appendChild(root.createTextNode(f.assay.title))
            EXPERIMENT.appendChild(TITLE)

            # EXPERIMENT_SET > EXPERIMENT > STUDY_REF
            STUDY_REF = root.createElement('STUDY_REF')
            STUDY_REF.setAttribute('accession', 'accession')
            EXPERIMENT.appendChild(STUDY_REF)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN
            DESIGN = root.createElement('DESIGN')
            EXPERIMENT.appendChild(DESIGN)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > DESIGN_DESCRIPTION
            DESIGN_DESCRIPTION = root.createElement('DESIGN_DESCRIPTION')
            DESIGN.appendChild(DESIGN_DESCRIPTION)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > SAMPLE_DESCRIPTOR
            SAMPLE_DESCRIPTOR = root.createElement('SAMPLE_DESCRIPTOR')
            SAMPLE_DESCRIPTOR.setAttribute('accession', 'accession')
            DESIGN.appendChild(SAMPLE_DESCRIPTOR)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR
            LIBRARY_DESCRIPTOR = root.createElement('LIBRARY_DESCRIPTOR')
            DESIGN.appendChild(LIBRARY_DESCRIPTOR)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
            # LIBRARY_NAME
            LIBRARY_NAME = root.createElement('LIBRARY_NAME')
            LIBRARY_DESCRIPTOR.appendChild(LIBRARY_NAME)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
            # LIBRARY_STRATEGY
            LIBRARY_STRATEGY = root.createElement('LIBRARY_STRATEGY')
            LIBRARY_STRATEGY.appendChild(
                root.createTextNode(f.assay.library_strategy.name))
            LIBRARY_DESCRIPTOR.appendChild(LIBRARY_STRATEGY)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
            # LIBRARY_SOURCE
            LIBRARY_SOURCE = root.createElement('LIBRARY_SOURCE')
            LIBRARY_SOURCE.appendChild(
                root.createTextNode(f.assay.library_source.name))
            LIBRARY_DESCRIPTOR.appendChild(LIBRARY_SOURCE)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
            # LIBRARY_SELECTION
            LIBRARY_SELECTION = root.createElement('LIBRARY_SELECTION')
            LIBRARY_SELECTION.appendChild(
                root.createTextNode(f.assay.library_selection.name))
            LIBRARY_DESCRIPTOR.appendChild(LIBRARY_SELECTION)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
            # LIBRARY_LAYOUT
            LIBRARY_LAYOUT = root.createElement('LIBRARY_LAYOUT')
            LIBRARY_DESCRIPTOR.appendChild(LIBRARY_LAYOUT)

            # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
            # LIBRARY_LAYOUT > PAIRED
            if f.assay.library_layout.name == "paired":
                LLI = root.createElement('PAIRED')
                if f.assay.nominal_length is not None:
                    LLI.setAttribute('NOMINAL_LENGTH', f.assay.nominal_length)
                if f.assay.nominal_sdev is not None:
                    LLI.setAttribute('NOMINAL_SDEV', f.assay.nominal_sdev)
            elif f.assay.library_layout.name == "single":
                LLI = root.createElement('SINGLE')

            LIBRARY_LAYOUT.appendChild(LLI)

            if f.assay.library_construction_protocol:
                LIBRARY_CONSTRUCTION_PROTOCOL = \
                    root.createElement('LIBRARY_CONSTRUCTION_PROTOCOL')
                LIBRARY_CONSTRUCTION_PROTOCOL.appendChild(
                    root.createTextNode(f.assay.library_construction_protocol))
                LIBRARY_DESCRIPTOR.appendChild(LIBRARY_CONSTRUCTION_PROTOCOL)

            # EXPERIMENT_SET > EXPERIMENT > PLATFORM
            PLATFORM = root.createElement('PLATFORM')
            EXPERIMENT.appendChild(PLATFORM)

            # EXPERIMENT_SET > EXPERIMENT > PLATFORM > INSTRUMENT
            # bad name it's to illustrate
            INSTRUMENT = root.createElement(f.assay.platform.name)
            PLATFORM.appendChild(INSTRUMENT)

            # EXPERIMENT_SET > EXPERIMENT > PLATFORM > INSTRUMENT >
            # INSTRUMENT_MODEL
            INSTRUMENT_MODEL = root.createElement('INSTRUMENT_MODEL')
            INSTRUMENT_MODEL.appendChild(
                root.createTextNode(f.assay.instrument.name))
            INSTRUMENT.appendChild(INSTRUMENT_MODEL)

            if f.assay.additional_annotation:
                # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES
                EXPERIMENT_ATTRIBUTES = root.createElement(
                    'EXPERIMENT_ATTRIBUTES')
                EXPERIMENT.appendChild(EXPERIMENT_ATTRIBUTES)

                # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES >
                # EXPERIMENT_ATTRIBUTE
                EXPERIMENT_ATTRIBUTE = root.createElement(
                    'EXPERIMENT_ATTRIBUTE')
                EXPERIMENT_ATTRIBUTES.appendChild(EXPERIMENT_ATTRIBUTE)

                # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES >
                # EXPERIMENT_ATTRIBUTE > TAG
                TAG = root.createElement('TAG')
                TAG.appendChild(root.createTextNode("TAG"))
                EXPERIMENT_ATTRIBUTE.appendChild(TAG)

                # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES >
                # EXPERIMENT_ATTRIBUTE > VALUE
                VALUE = root.createElement('VALUE')
                VALUE.appendChild(root.createTextNode("VALUE"))
                EXPERIMENT_ATTRIBUTE.appendChild(VALUE)

        xml_str = root.toprettyxml(indent="\t", encoding="UTF-8")

        with TemporaryFile() as tmp:
            tmp.write(xml_str)
            tmp.seek(0)
            response = HttpResponse(tmp.read(),
                                    content_type='application/xml'
                                    )
            response[
                'Content-Disposition'] = \
                'attachment; filename=experiment.xml'
            return response


@extend_schema(
    operation_id='export_xml_sample',
    description="Generate ENA XML : sample",
    tags=["XML"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Metadata ID',
            type=int
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@csrf_exempt
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def export_xml_sample(request, id):
    """SUMMARY LINE

    Generate ENA XML : project
    """

    q = get_object_or_404(Metadata, pk=id)

    if request.method == 'GET':
        root = minidom.Document()

        # SAMPLE_SET
        SAMPLE_SET = root.createElement('SAMPLE_SET')
        root.appendChild(SAMPLE_SET)

        if isinstance(q.table, str):
            table = json.loads(q.table)
        else:
            table = q.table

        print(type(table))
        for row in table:
            # SAMPLE_SET > SAMPLE
            SAMPLE = root.createElement('SAMPLE')
            SAMPLE.setAttribute('alias', row['alias'])
            SAMPLE.setAttribute('center_name', '')
            SAMPLE_SET.appendChild(SAMPLE)

            # SAMPLE_SET > SAMPLE > TITLE
            TITLE = root.createElement('TITLE')
            TITLE.appendChild(root.createTextNode(row['title']))
            SAMPLE.appendChild(TITLE)

            # SAMPLE_SET > SAMPLE > SAMPLE_NAME
            SAMPLE_NAME = root.createElement('SAMPLE_NAME')
            SAMPLE.appendChild(SAMPLE_NAME)

            # SAMPLE_SET > SAMPLE > SAMPLE_NAME > TAXON_ID
            TAXON_ID = root.createElement('TAXON_ID')
            TAXON_ID.appendChild(root.createTextNode(row['taxon id']))
            SAMPLE_NAME.appendChild(TAXON_ID)

            # SAMPLE_SET > SAMPLE > SAMPLE_NAME > SCIENTIFIC_NAME
            SCIENTIFIC_NAME = root.createElement('SCIENTIFIC_NAME')
            SCIENTIFIC_NAME.appendChild(root.createTextNode(
                row['scientific name']))
            SAMPLE_NAME.appendChild(SCIENTIFIC_NAME)

            # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES
            SAMPLE_ATTRIBUTES = root.createElement('SAMPLE_ATTRIBUTES')
            SAMPLE.appendChild(SAMPLE_ATTRIBUTES)

            if not isinstance(row, str):
                for key, value in row.items():
                    if value is not None and value != "" and \
                            key not in ['alias', 'title', 'description',
                                        'scientific name', 'taxon id',
                                        'project name']:

                        field = Field.objects.get(checklist=q.checklist,
                                                  name=key)

                        # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                        # SAMPLE_ATTRIBUTE
                        SAMPLE_ATTRIBUTE = root.createElement(
                            'SAMPLE_ATTRIBUTE')
                        SAMPLE_ATTRIBUTES.appendChild(SAMPLE_ATTRIBUTE)

                        # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                        # SAMPLE_ATTRIBUTE > TAG
                        TAG = root.createElement('TAG')
                        TAG.appendChild(root.createTextNode(key))
                        SAMPLE_ATTRIBUTE.appendChild(TAG)

                        # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                        # SAMPLE_ATTRIBUTE > VALUE
                        print(key + value)
                        VALUE = root.createElement('VALUE')
                        VALUE.appendChild(
                            root.createTextNode(value))
                        SAMPLE_ATTRIBUTE.appendChild(VALUE)

                        if not (field.unit is None or field.unit == ""):
                            UNITS = root.createElement('UNITS')
                            UNITS.appendChild(
                                root.createTextNode(field.unit))
                            SAMPLE_ATTRIBUTE.appendChild(UNITS)

                # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                # SAMPLE_ATTRIBUTE
                SAMPLE_ATTRIBUTE = root.createElement(
                    'SAMPLE_ATTRIBUTE')
                SAMPLE_ATTRIBUTES.appendChild(SAMPLE_ATTRIBUTE)

                # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                # SAMPLE_ATTRIBUTE > TAG
                TAG = root.createElement('TAG')
                TAG.appendChild(root.createTextNode("ENA-CHECKLIST"))
                SAMPLE_ATTRIBUTE.appendChild(TAG)

                # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                # SAMPLE_ATTRIBUTE > VALUE
                VALUE = root.createElement('VALUE')
                VALUE.appendChild(
                    root.createTextNode(q.checklist.original_id))
                SAMPLE_ATTRIBUTE.appendChild(VALUE)

        xml_str = root.toprettyxml(indent="\t", encoding="UTF-8")

        with TemporaryFile() as tmp:
            tmp.write(xml_str)
            tmp.seek(0)
            response = HttpResponse(tmp.read(),
                                    content_type='application/xml'
                                    )
            response[
                'Content-Disposition'] = \
                'attachment; filename=sample.xml'
            return response

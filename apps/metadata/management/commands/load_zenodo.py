from django.core.management.base import BaseCommand

from apps.metadata.models import Database


class Command(BaseCommand):
    help = 'Create zenodo db'

    def handle(self, *args, **kwargs):
        Database.objects.create(
            name="Zenodo",
            url="https://zenodo.org/",
            description="Built and developed by researchers, to ensure that "
                        "everyone can join in Open Science.",
            urlImage="https://about.zenodo.org/static/img/logos/"
                     "zenodo-gradient-200.png"
        )

        self.stdout.write("Done")

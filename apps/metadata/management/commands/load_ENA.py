import csv
import xml.etree.ElementTree as ET

import requests
from django.core.management.base import BaseCommand

from apps.metadata.models import Database, Checklist, Field


class Command(BaseCommand):
    help = 'Create ENA datatablse'

    def handle(self, *args, **kwargs):
        d = Database.objects.create(
            name="ENA",
            url="https://www.ebi.ac.uk/ena/browser/home",
            description="The European Nucleotide Archive (ENA) provides a "
                        "comprehensive record of the world’s nucleotide "
                        "sequencing information, covering raw sequencing "
                        "data, sequence assembly information and functional "
                        "annotation.",
            urlImage="https://www.ebi.ac.uk/ena/sah/ENA-logo.png"
        )

        for i in range(11, 60):
            response = requests.get(
                'https://www.ebi.ac.uk/ena/browser/api/xml/ERC0000' + str(i))
            if response.ok:
                print('ERC0000' + str(i))
                tree = ET.ElementTree(ET.fromstring(response.content))
                root = tree.getroot()

                cl = Checklist.objects.create(
                    original_id=root.findall(
                        "./CHECKLIST/IDENTIFIERS/PRIMARY_ID")[0].text,
                    name=root.findall("./CHECKLIST/DESCRIPTOR/LABEL")[0].text,
                    description=root.findall(
                        "./CHECKLIST/DESCRIPTOR/DESCRIPTION")[0].text,
                    url='https://www.ebi.ac.uk/ena/browser/view/' +
                        root.findall(
                            "./CHECKLIST/IDENTIFIERS/PRIMARY_ID")[0].text,
                    # rawfile=file,
                    database=d
                )

                for fg in root.findall("./CHECKLIST/DESCRIPTOR/FIELD_GROUP"):
                    for inter_field in fg.findall('FIELD'):
                        f = Field()
                        f.name = inter_field.find('NAME').text

                        if inter_field.find('DESCRIPTION'):
                            inter_split = (inter_field.find('DESCRIPTION')
                                           .text.split(" Example: "))

                            f.description = inter_split[0]

                            if len(inter_split) == 2:
                                f.example = inter_split[1]

                        f.format = inter_field.find('FIELD_TYPE')[0].tag
                        f.requirement = inter_field.find('MANDATORY').text

                        if inter_field.findall(
                                './FIELD_TYPE/TEXT_FIELD/REGEX_VALUE'):
                            f.restriction = inter_field.find(
                                './FIELD_TYPE/TEXT_FIELD/REGEX_VALUE').text
                        elif inter_field.findall(
                                './FIELD_TYPE/TEXT_CHOICE_FIELD/TEXT_VALUE'):
                            items = []
                            for i in inter_field.findall(
                                    './FIELD_TYPE/TEXT_CHOICE_FIELD/TEXT_VALUE'
                            ):
                                items.append(i.find('VALUE').text)
                            f.restriction = "@".join(items)

                        if inter_field.findall(
                                './UNITS/UNIT'):
                            items = []
                            for i in inter_field.findall(
                                    './UNITS/UNIT'):
                                items.append(i.text)
                            f.unit = "@".join(items)

                        f.group = fg.find('NAME').text

                        f.checklist = cl

                        f.save()

        cl = Checklist.objects.create(
            original_id="MIP_ENA",
            name="MIP_ENA",
            description="Minimal information mandatory in ENA",
            url='',
            rawfile='',
            database=d
        )

        with open('static-apps/checklists/MIP_ENA.tsv') as tsvfile:
            spamreader = csv.reader(tsvfile, delimiter='\t')
            for row in spamreader:
                f = Field()
                f.name = row[1]
                f.requirement = row[2]
                f.description = row[3]
                f.format = row[4]
                f.restriction = row[5]
                f.checklist = cl
                f.save()

        self.stdout.write("Done")

from django.core.management.base import BaseCommand

from apps.metadata.models import Database


class Command(BaseCommand):
    help = 'Create OPIDoR database'

    def handle(self, *args, **kwargs):
        Database.objects.create(
            name="OPIDoR",
            url="https://dmp.opidor.fr/plans",
            description="Le portail OPIDoR met à votre disposition un "
                        "ensemble d’outils et de services qui vous "
                        "facilitent la gestion et la valorisation des "
                        "données de recherche. Vous pourrez ainsi répondre "
                        "au mieux aux critères d’intégrité et de "
                        "reproductibilité et tendre vers les principes "
                        "FAIR qui visent à rendre les données Faciles à "
                        "trouver, Accessibles, Interopérables, Réutilisables.",
            urlImage="https://dmp.opidor.fr/assets/logo-e56e95490f17dd15cf78"
                     "e3edfd44d7399fa8bc5b952aa17c5228b9f372a5be57.png"
        )

        self.stdout.write("Done")

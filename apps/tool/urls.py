from django.urls import path

from apps.tool.apis import export_companion_to_cluster, ToolManagement, \
    get_orcid_info

urlpatterns = [
    path('api/send/companion/file/<slug:id>/', export_companion_to_cluster,
         name="export_companion_to_cluster"),
    path('api/get/info/from/orcid/<str:orcid>/', get_orcid_info,
         name="get_orcid_info"),

    path('api/tool/list/',
         ToolManagement.as_view({'get': 'list'}),
         name="ToolManagement"),



]

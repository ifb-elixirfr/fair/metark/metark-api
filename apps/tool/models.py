from django.db import models


class Tool(models.Model):
    name = models.CharField(help_text="Name of tool", max_length=50)

    url_test = models.URLField(help_text="URL of tool (dev)",
                               blank=True,
                               null=True)

    url = models.URLField(help_text="URL of tool (prod)",
                          blank=True,
                          null=True)

    urlImage = models.URLField(help_text="URL logo",
                               blank=True,
                               null=True)

    description = models.TextField(help_text="Description of database",
                                   blank=True,
                                   null=True)

    def __str__(self):
        return self.name

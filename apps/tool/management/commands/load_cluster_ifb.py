from django.core.management.base import BaseCommand

from apps.tool.models import Tool


class Command(BaseCommand):
    help = 'Create IFB cluster tool'

    def handle(self, *args, **kwargs):
        Tool.objects.create(
            name="Cluster IFB",
            url="https://www.france-bioinformatique.fr/cluster-ifb-core/",
            description="Le cluster de l’IFB-core est en production depuis "
                        "novembre 2018. Il est composé de 4300 cœurs "
                        "(hyperthreadés) et de 2Po de stockage. Son but est "
                        "de donner accès à des ressources bioinformatiques "
                        "générales et spécialisées à des utilisateurs de "
                        "différents domaines (biologistes et "
                        "bioinformaticiens) et niveaux d’expertise "
                        "(du novice à l’expert).",
            urlImage="https://www.france-bioinformatique.fr/wp-content/"
                     "uploads/logo-ifb-couleur.svg"
        )

        self.stdout.write("Done")

from rest_framework.serializers import ModelSerializer

from apps.tool.models import Tool


class ToolSerializer(ModelSerializer):
    class Meta:
        model = Tool
        fields = "__all__"

import json
from tempfile import TemporaryFile

import hvac
import requests
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from drf_spectacular.types import OpenApiTypes

from drf_spectacular.utils import extend_schema, OpenApiParameter, \
    OpenApiResponse
from rest_framework.decorators import permission_classes, api_view
from rest_framework.exceptions import ParseError

from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.isa.models import Investigation
from apps.isa.serializers import InvestigationSerializer,\
    StudyResumeSerializer
import paramiko

from apps.tool.models import Tool
from apps.tool.serializers import ToolSerializer
from xml.dom.minidom import parseString


###############################################################################
# Management
###############################################################################

@extend_schema(
    operation_id="api_manage_database",
    description="Manage database",
    tags=["Tool"],
)
@permission_classes([IsAuthenticated])
class ToolManagement(ModelViewSet):
    serializer_class = ToolSerializer
    queryset = Tool.objects.all()

    def list(self, request, ):
        serializer = ToolSerializer(Tool.objects.all(), many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id='get_orcid_info',
    description="Get information with an ORCid",
    tags=["Tool"],
    parameters=[
        OpenApiParameter(
            name='orcid',
            location=OpenApiParameter.PATH,
            description='Orcid',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_orcid_info(request, orcid):
    response = requests.get("https://pub.orcid.org/v3.0/" + orcid + "/")
    if response.ok:
        res_xml = parseString(response.content)
        gn = res_xml.getElementsByTagName("personal-details:given-names")[
            0].firstChild.data
        fn = res_xml.getElementsByTagName("personal-details:family-name")[
            0].firstChild.data
        data = dict()
        data["orcid"] = orcid
        data["given_name"] = gn
        data["family_name"] = fn
        return JsonResponse(data, safe=False)
    else:
        return HttpResponse("ORCid not found", status=404)


###############################################################################
# Send
###############################################################################

@extend_schema(
    operation_id='export_companion_to_cluster',
    description="Export companion file to IFB cluster",
    tags=["Tool"],
    parameters=[
        OpenApiParameter(
            name='id',
            location=OpenApiParameter.PATH,
            description='Investigation ID',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def export_companion_to_cluster(request, id):
    """SUMMARY LINE

    Export to Opidor
    """
    # Check

    if 'listItems' not in request.data:
        raise ParseError("Empty listItems")
    if 'project' not in request.data:
        raise ParseError("Empty project")

    inv = get_object_or_404(Investigation, pk=id)

    # Authen
    client = hvac.Client(url='http://vault:8200')
    u = User.objects.get(username=request.user.username)

    clientInfo = client.auth.userpass.login(
        username=u.username,
        password=u.password.split("$")[-1],
    )
    vault_id = clientInfo['auth']['entity_id']

    vaultToken = clientInfo['auth']['client_token']
    client = hvac.Client(url='http://vault:8200',
                         token=vaultToken)
    try:
        read_response = client.secrets.kv.read_secret_version(
            path=vault_id + "/" + 'Cluster IFB')
    except Exception:
        return HttpResponse(status=404)

    username = ""
    pwd = ""

    for i in read_response['data']['data']:
        if i['ItemNameValue'] == "Username":
            username = i['ItemValueValue']
        if i['ItemNameValue'] == "Password":
            pwd = i['ItemValueValue']

    if username == "" or pwd == "":
        return HttpResponse(status=404)
    else:

        if request.data['project'] == "":
            path = "/shared/home/" + username + "/"
        else:
            path = "/shared/projects/" + request.data['project'] + "/"

        # Prepare data

        data = dict()

        if "investigation" in request.data['listItems']:
            data['investigation'] = InvestigationSerializer(inv).data

        if "study" in request.data['listItems']:
            data['studies'] = StudyResumeSerializer(inv.study_set.all(),
                                                    many=True).data

        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(hostname='core.cluster.france-bioinformatique.fr',
                           username=username, password=pwd)
        ftp_client = ssh_client.open_sftp()
        with TemporaryFile() as tmp:
            tmp.write(json.dumps(data,
                                 ensure_ascii=False).encode('utf8'))
            tmp.seek(0)
            ftp_client.putfo(tmp, path + str(inv.pk) + ".json")
            ftp_client.close()
        ssh_client.close()
        return HttpResponse(status=200)

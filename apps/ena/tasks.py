import json
import os
from celery import shared_task
from django.contrib.auth.models import User

from apps.data_brokering.models import ExternalID
from apps.ena._functions import sizeof_fmt
from apps.isa._functions import createToDoItemsInv
from apps.isa.models import Investigation, Study, MeasurementType, Assay, \
    DataFile, TypeFile, ChecksumMethod, CenterName, ToDoList, Role, \
    Contact
from apps.isa.serializers import InvestigationResumeSerializer
from apps.metadata.apis import check_completness, XML_NODE_TYPE
from apps.metadata.models import Checklist, Field, Database
from apps.metadata.serializers import MetadataSerializer

import requests
from xml.dom.minidom import parseString


class TaskFailure(Exception):
    pass


###############################################################################
# Import ENA project
###############################################################################


@shared_task(name='Import ENA project', track_started=True, bind=True)
def import_ena_project_async(self, project_id, username):
    """Function to import all metadata form ENA ID"""

    response = requests.get(
        'https://www.ebi.ac.uk/ena/browser/api/xml/' + project_id +
        '?includeLinks=false')

    project_xml = parseString(response.content)

    try:
        inv = Investigation.objects.create(
            title=(project_xml.getElementsByTagName("TITLE")[0]
                   .firstChild.data.capitalize()),
            description=(project_xml.getElementsByTagName("DESCRIPTION")[0]
                         .firstChild.data),
            alias=(project_xml.getElementsByTagName("PROJECT")[0]
                   .getAttribute('alias')),
            creationSource='ENA'
        )

        user_temp = User.objects.get(username=username)
        c = Contact.objects.create(
            user=user_temp,
            corresponding_contact=True,
            role=Role.objects.get_or_create(name='All')[0])
        inv.collaborators.add(c)

        tdl = ToDoList.objects.create(
            content_object=inv
        )
        createToDoItemsInv(tdl)

        serializer = InvestigationResumeSerializer(inv)

        response = requests.get(
            "https://www.ebi.ac.uk/ena/portal/api/filereport?accession=" +
            project_id + "&result=read_run&fields=study_accession,"
                         "sample_accession,experiment_accession,"
                         "run_accession,tax_id,scientific_name,"
                         "fastq_ftp,submitted_ftp,sra_ftp,fastq_bytes,"
                         "fastq_md5"
                         "&format=json&limit=0")
        data = response.json()
        sampleList = dict()
        for d in data:
            response = requests.get(
                'https://www.ebi.ac.uk/ena/browser/api/xml/' +
                d['study_accession'] +
                '?includeLinks=false')
            study_xml = parseString(response.content)

            study, created = Study.objects.get_or_create(
                investigation=inv,
                alias=d['study_accession'],
                title=(study_xml.getElementsByTagName("PROJECT")[0]
                       .getElementsByTagName("TITLE")[0]
                       .firstChild.data.capitalize()),
                description=(study_xml.getElementsByTagName("PROJECT")[0]
                             .getElementsByTagName("DESCRIPTION")[0]
                             .firstChild.data)
            )

            try:
                ext_id = ExternalID.objects.get(
                    content_type__model="study",
                    object_id=str(study.pk),
                    db__name="ENA")
            except ExternalID.DoesNotExist:
                ext_id = ExternalID.objects.create(
                    content_object=study,
                    db=Database.objects.get(name="ENA")
                )

            if not ext_id.external_id:
                ext_id.external_id = dict()
                ext_id.external_id["PROJECT"] = d['study_accession']
                ext_id.save()

            response = requests.get(
                'https://www.ebi.ac.uk/ena/browser/api/xml/' +
                d['experiment_accession'] +
                '?includeLinks=false')
            experiment_xml = parseString(response.content)

            # Get platform
            platform = (
                experiment_xml
                .getElementsByTagName("EXPERIMENT")[0]
                .getElementsByTagName("PLATFORM")[0]
            )

            for p in platform.childNodes:
                if XML_NODE_TYPE[str(p.nodeType)] == "ELEMENT_NODE":
                    PLATFORM = p.nodeName
                    INSTRUMENT_MODEL = p.getElementsByTagName(
                        "INSTRUMENT_MODEL")[0].firstChild.data
                    break

            layout = (
                experiment_xml
                .getElementsByTagName("EXPERIMENT")[0]
                .getElementsByTagName("LIBRARY_LAYOUT")[0]
            )

            for p in layout.childNodes:
                if XML_NODE_TYPE[str(p.nodeType)] == "ELEMENT_NODE":
                    LAYOUT = p.nodeName
                    if LAYOUT == "PAIRED":
                        LAYOUT = "paired"
                        try:
                            NOMINAL_LENGTH = int(
                                p.getAttribute('NOMINAL_LENGTH'))
                        except Exception:
                            NOMINAL_LENGTH = None

                        try:
                            NOMINAL_SDEV = int(
                                p.getAttribute('NOMINAL_SDEV'))
                        except Exception:
                            NOMINAL_SDEV = None
                    else:
                        LAYOUT = "single"
                        NOMINAL_LENGTH = None
                        NOMINAL_SDEV = None
                    break
            LIBRARY_SELECTION = (experiment_xml
                                 .getElementsByTagName("EXPERIMENT")[0]
                                 .getElementsByTagName("LIBRARY_SELECTION")[0]
                                 .firstChild.data)
            LIBRARY_SOURCE = (experiment_xml
                              .getElementsByTagName("EXPERIMENT")[0]
                              .getElementsByTagName("LIBRARY_SOURCE")[0]
                              .firstChild.data)
            LIBRARY_STRATEGY = (experiment_xml
                                .getElementsByTagName("EXPERIMENT")[0]
                                .getElementsByTagName("LIBRARY_STRATEGY")[0]
                                .firstChild.data)

            assay = Assay.objects.create(
                study=study,
                title=(experiment_xml
                       .getElementsByTagName("EXPERIMENT")[0]
                       .getElementsByTagName("TITLE")[0]
                       .firstChild.data),
                alias=(experiment_xml
                       .getElementsByTagName("EXPERIMENT")[0]
                       .getAttribute('alias')),
                center_name=CenterName.objects.get_or_create(
                    name=experiment_xml.getElementsByTagName(
                        "EXPERIMENT")[0].getAttribute('center_name'))[0],
                measurement_type=MeasurementType.objects.get_or_create(
                    name=(experiment_xml
                          .getElementsByTagName("EXPERIMENT")[0]
                          .getElementsByTagName("TITLE")[0]
                          .firstChild.data))[0],
            )

            if 'EXPERIMENT' not in ext_id.external_id:
                ext_id.external_id['EXPERIMENT'] = []

            ext_id.external_id['EXPERIMENT'].append(
                {
                    'alias': (experiment_xml
                              .getElementsByTagName("EXPERIMENT")[0]
                              .getAttribute('alias')),
                    'assayID': assay.id,
                    'accession': (experiment_xml
                                  .getElementsByTagName("EXPERIMENT")[0]
                                  .getAttribute('accession'))}
            )
            ext_id.save()
            response = requests.get(
                'https://www.ebi.ac.uk/ena/browser/api/xml/' +
                d['run_accession'] +
                '?includeLinks=false')
            run_xml = parseString(response.content)

            for run in run_xml.getElementsByTagName("RUN"):
                run_acc = run.getAttribute('accession')
                run_center_name = run.getAttribute('center_name')
                run_alias = run.getAttribute('alias')

                if 'RUN' not in ext_id.external_id:
                    ext_id.external_id['RUN'] = []

                if len(run.getElementsByTagName("FILE")) == 0:
                    for data_item in data:
                        if run_acc in data_item["run_accession"]:
                            for indexFile in range(
                                    len(data_item["fastq_ftp"].split(";"))
                            ):
                                df = DataFile.objects.create(
                                    assay=assay,
                                    alias=run_alias,
                                    checksum=(
                                        data_item["fastq_md5"]
                                        .split(";")[indexFile]),
                                    name=(
                                        data_item["fastq_ftp"]
                                        .split(";")[indexFile]),
                                    checksum_method=(ChecksumMethod
                                                     .objects
                                                     .get(name="MD5")),
                                    location=(
                                        data_item["fastq_ftp"]
                                        .split(";")[indexFile]),
                                    size=sizeof_fmt(
                                        int(data_item["fastq_bytes"]
                                            .split(";")[indexFile])),
                                    type=TypeFile.objects.get_or_create(
                                        name="fastq")[0],
                                    center_name=(
                                        CenterName.objects.get_or_create(
                                            name=run_center_name)[0])
                                )

                                ext_id.external_id['RUN'].append(
                                    {
                                        'alias': run_alias,
                                        'assayID': assay.id,
                                        'datafileId': df.id,
                                        'accession': run_acc}
                                )
                                ext_id.save()
                else:
                    for f in run.getElementsByTagName("FILE"):
                        location = []
                        size = []
                        for data_item in data:
                            if run_acc in data_item["fastq_ftp"]:
                                if data_item["submitted_ftp"] == "":
                                    sourceName = "fastq_ftp"
                                else:
                                    sourceName = "submitted_ftp"
                                index = [
                                    i for i, word in
                                    enumerate(data_item[sourceName]
                                              .split(";")) if word.endswith(
                                        os.path.basename(
                                            f.getAttribute('filename')))
                                ][0]
                                location = data_item["fastq_ftp"].split(";")[
                                    index]
                                size = sizeof_fmt(int(
                                    data_item["fastq_bytes"].split(";")[index])
                                )
                                break

                        df = DataFile.objects.create(
                            assay=assay,
                            alias=run_alias,
                            checksum=f.getAttribute('checksum'),
                            name=f.getAttribute('filename'),
                            checksum_method=ChecksumMethod.objects.get(
                                name=f.getAttribute('checksum_method')),
                            location=location,
                            size=size,
                            type=TypeFile.objects.get_or_create(
                                name=f.getAttribute('filetype'))[0],
                            center_name=CenterName.objects.get_or_create(
                                name=run_center_name)[0]
                        )

                        ext_id.external_id['RUN'].append(
                            {
                                'alias': run_alias,
                                'assayID': assay.id,
                                'datafileId': df.id,
                                'accession': run_acc}
                        )
                        ext_id.save()

            response = requests.get(
                'https://www.ebi.ac.uk/ena/browser/api/xml/' +
                d['sample_accession'] +
                '?includeLinks=false')
            sample_xml = parseString(response.content)
            clFind = False
            for sa in sample_xml.getElementsByTagName("SAMPLE_ATTRIBUTE"):
                if (sa.getElementsByTagName("TAG")[0]
                        .firstChild.data == 'ENA-CHECKLIST'):
                    checklist = (sa.getElementsByTagName("TAG")[0]
                                 .parentNode.getElementsByTagName(
                        "VALUE")[0].firstChild.data)
                    cl = Checklist.objects.get(original_id=checklist)
                    clFind = True
                    break

            if clFind is not True:
                cl = Checklist.objects.get(original_id="ERC000011")

            list_fields = (Field.objects.filter(checklist=cl) |
                           Field.objects.filter(checklist__name="MIP_ENA"))

            inter = dict()

            for lf in list_fields:
                inter[lf.name] = None
                if lf.unit is not None and lf.unit != "":
                    inter[lf.name + " (unit)"] = None

            inter['alias'] = (
                sample_xml.getElementsByTagName("SAMPLE")[0]
                          .getAttribute('alias'))
            inter['title'] = (
                sample_xml.getElementsByTagName("TITLE")[0]
                          .firstChild.data)

            if len(sample_xml.getElementsByTagName("DESCRIPTION")) != 0:
                inter['description'] = (
                    sample_xml.getElementsByTagName("DESCRIPTION")[0]
                              .firstChild.data)

            inter['scientific name'] = (
                sample_xml.getElementsByTagName("SCIENTIFIC_NAME")[0]
                          .firstChild.data)
            inter['taxon id'] = (
                sample_xml.getElementsByTagName("TAXON_ID")[0]
                          .firstChild.data)
            inter['PLATFORM'] = PLATFORM
            inter['INSTRUMENT_MODEL'] = INSTRUMENT_MODEL
            inter['LIBRARY_SELECTION'] = LIBRARY_SELECTION
            inter['LIBRARY_SOURCE'] = LIBRARY_SOURCE
            inter['LIBRARY_STRATEGY'] = LIBRARY_STRATEGY
            inter['LIBRARY_LAYOUT'] = LAYOUT
            inter['NOMINAL_LENGTH'] = NOMINAL_LENGTH
            inter['NOMINAL_SDEV'] = NOMINAL_SDEV

            for sa in sample_xml.getElementsByTagName("SAMPLE_ATTRIBUTE"):
                if not (sa.getElementsByTagName("TAG")[0]
                          .firstChild.data == 'ENA-CHECKLIST'):
                    key = sa.getElementsByTagName("TAG")[0].firstChild.data
                    value = (sa.getElementsByTagName("VALUE")[0]
                             .firstChild.data)
                    if key in inter.keys():
                        inter[key] = value
                        if len(sa.getElementsByTagName("UNITS")) != 0:
                            inter[key + " (unit)"] = sa.getElementsByTagName(
                                "UNITS")[0].firstChild.data

            sampleList[assay.id] = inter

            if 'SAMPLE' not in ext_id.external_id:
                ext_id.external_id['SAMPLE'] = []

            ext_id.external_id['SAMPLE'].append({
                'alias': (sample_xml.getElementsByTagName("SAMPLE")[0]
                          .getAttribute('alias')),
                'assayID': assay.id,
                'accession': (sample_xml.getElementsByTagName("SAMPLE")[0]
                              .getAttribute('accession'))})
            ext_id.save()

        check_result = check_completness(sampleList, list_fields)

        data = {
            'checklist': str(cl.pk),
            'study': str(study.pk),
            'created_by': username,
            'last_modification_by': username,
            'short_description': 'Auto load',
            'mandatory': check_result['mandatory'],
            'recommended': check_result['recommended'],
            'optional': check_result['optional'],
            'table': json.dumps(sampleList),
            'status': "pending"
        }
        serializer = MetadataSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return (serializer.data)

    except Exception as e:
        print(e)
        raise TaskFailure(
            'An internal error occurred while processing your '
            'file. Please contact support with the name of the '
            'submitted file.')

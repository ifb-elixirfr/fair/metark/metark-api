import json
import os
import re

from xml.dom import minidom

import requests

from apps.data_brokering.models import Submission
from apps.isa.models import Assay, DataFile
from apps.metadata.models import Field


def sizeof_fmt(num, suffix="B"):
    """
    Convert size of file

    :param num: numeric value
    :param suffix: Unit suffix
    :return: Converted value
    """
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


def generate_submission_file(action):
    """
    Generate submission file for ENA

    :return: XML (str format)
    """
    root = minidom.Document()

    SUBMISSION = root.createElement('SUBMISSION')
    root.appendChild(SUBMISSION)

    ACTIONS = root.createElement('ACTIONS')
    SUBMISSION.appendChild(ACTIONS)

    ACTION = root.createElement('ACTION')
    ACTIONS.appendChild(ACTION)

    ADD = root.createElement(action)
    ACTION.appendChild(ADD)

    return root.toprettyxml(indent="\t", encoding="UTF-8")


def generate_project_file(study):
    """
    Generate project file for ENA

    :param Study object

    :return: XML (str format)
    """
    root = minidom.Document()

    # PROJECT_SET
    PROJECT_SET = root.createElement('PROJECT_SET')
    root.appendChild(PROJECT_SET)

    # PROJECT_SET > PROJECT
    PROJECT = root.createElement('PROJECT')
    PROJECT.setAttribute('alias', study.alias)
    PROJECT_SET.appendChild(PROJECT)

    # PROJECT_SET > PROJECT > TITLE
    TITLE = root.createElement('TITLE')
    TITLE.appendChild(root.createTextNode(study.title))
    PROJECT.appendChild(TITLE)

    # PROJECT_SET > PROJECT > DESCRIPTION
    DESCRIPTION = root.createElement('DESCRIPTION')
    DESCRIPTION.appendChild(root.createTextNode(study.description))
    PROJECT.appendChild(DESCRIPTION)

    # PROJECT_SET > PROJECT > SUBMISSION_PROJECT
    SUBMISSION_PROJECT = root.createElement('SUBMISSION_PROJECT')
    PROJECT.appendChild(SUBMISSION_PROJECT)

    # PROJECT_SET > PROJECT > SUBMISSION_PROJECT > SEQUENCING_PROJECT
    SEQUENCING_PROJECT = root.createElement('SEQUENCING_PROJECT')
    SUBMISSION_PROJECT.appendChild(SEQUENCING_PROJECT)

    return root.toprettyxml(indent="\t", encoding="UTF-8")


def generate_sample_file(metadata):
    """
    Generate sample file for ENA

    :param Metadata object

    :return: XML (str format)
    """

    root = minidom.Document()

    # SAMPLE_SET
    SAMPLE_SET = root.createElement('SAMPLE_SET')
    root.appendChild(SAMPLE_SET)

    if isinstance(metadata.table, str):
        table = json.loads(metadata.table)
    else:
        table = metadata.table

    table = table.values()

    regex = r"\(unit\)$"

    for row in table:
        # SAMPLE_SET > SAMPLE
        SAMPLE = root.createElement('SAMPLE')
        SAMPLE.setAttribute('alias', row['alias'])
        SAMPLE.setAttribute('center_name', '')
        SAMPLE_SET.appendChild(SAMPLE)

        # SAMPLE_SET > SAMPLE > TITLE
        TITLE = root.createElement('TITLE')
        TITLE.appendChild(root.createTextNode(row['title']))
        SAMPLE.appendChild(TITLE)

        # SAMPLE_SET > SAMPLE > SAMPLE_NAME
        SAMPLE_NAME = root.createElement('SAMPLE_NAME')
        SAMPLE.appendChild(SAMPLE_NAME)

        # SAMPLE_SET > SAMPLE > SAMPLE_NAME > TAXON_ID
        TAXON_ID = root.createElement('TAXON_ID')
        TAXON_ID.appendChild(root.createTextNode(row['taxon id']))
        SAMPLE_NAME.appendChild(TAXON_ID)

        # SAMPLE_SET > SAMPLE > SAMPLE_NAME > SCIENTIFIC_NAME
        SCIENTIFIC_NAME = root.createElement('SCIENTIFIC_NAME')
        SCIENTIFIC_NAME.appendChild(root.createTextNode(
            row['scientific name']))
        SAMPLE_NAME.appendChild(SCIENTIFIC_NAME)

        # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES
        SAMPLE_ATTRIBUTES = root.createElement('SAMPLE_ATTRIBUTES')
        SAMPLE.appendChild(SAMPLE_ATTRIBUTES)

        mip_values = list(Field.objects.filter(checklist__name="MIP_ENA")
                          .values_list("name", flat=True))

        if not isinstance(row, str):
            for key, value in row.items():
                if value is not None and value != "" and \
                        key not in mip_values and not bool(re.search(regex,
                                                                     key)):
                    field = Field.objects.get(checklist=metadata.checklist,
                                              name=key)

                    # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                    # SAMPLE_ATTRIBUTE
                    SAMPLE_ATTRIBUTE = root.createElement(
                        'SAMPLE_ATTRIBUTE')
                    SAMPLE_ATTRIBUTES.appendChild(SAMPLE_ATTRIBUTE)

                    # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                    # SAMPLE_ATTRIBUTE > TAG
                    TAG = root.createElement('TAG')
                    TAG.appendChild(root.createTextNode(key))
                    SAMPLE_ATTRIBUTE.appendChild(TAG)

                    # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
                    # SAMPLE_ATTRIBUTE > VALUE
                    VALUE = root.createElement('VALUE')
                    VALUE.appendChild(
                        root.createTextNode(value))
                    SAMPLE_ATTRIBUTE.appendChild(VALUE)

                    if not (field.unit is None or field.unit == ""):
                        UNITS = root.createElement('UNITS')
                        UNITS.appendChild(
                            root.createTextNode(row[key + " (unit)"]))
                        SAMPLE_ATTRIBUTE.appendChild(UNITS)

            # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
            # SAMPLE_ATTRIBUTE
            SAMPLE_ATTRIBUTE = root.createElement(
                'SAMPLE_ATTRIBUTE')
            SAMPLE_ATTRIBUTES.appendChild(SAMPLE_ATTRIBUTE)

            # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
            # SAMPLE_ATTRIBUTE > TAG
            TAG = root.createElement('TAG')
            TAG.appendChild(root.createTextNode("ENA-CHECKLIST"))
            SAMPLE_ATTRIBUTE.appendChild(TAG)

            # SAMPLE_SET > SAMPLE > SAMPLE_ATTRIBUTES >
            # SAMPLE_ATTRIBUTE > VALUE
            VALUE = root.createElement('VALUE')
            VALUE.appendChild(
                root.createTextNode(metadata.checklist.original_id))
            SAMPLE_ATTRIBUTE.appendChild(VALUE)

    return root.toprettyxml(indent="\t", encoding="UTF-8")


def get_taxon_id(scientific_name):
    """Get taxon ID for input scientific_name.
    :param scientific_name: scientific name of sample that distinguishes
                            its taxonomy
    :return taxon_id: NCBI taxonomy identifier
    """
    # Function from https://doi.org/10.5281/zenodo.6908270
    # endpoint for taxonomy id

    url = 'http://www.ebi.ac.uk/ena/taxonomy/rest/scientific-name'
    session = requests.Session()
    session.trust_env = False

    # url encoding: space -> %20
    scientific_name = '%20'.join(scientific_name.strip().split())
    r = session.get(f"{url}/{scientific_name}")
    try:
        taxon_id = r.json()[0]['taxId']
        return taxon_id
    except Exception:
        return None


def generate_experiment_file(study, metadata):
    """
    Generate experiment file for ENA

    :param Run object

    :return: XML (str format)
    """

    if isinstance(metadata, str):
        table = json.loads(metadata)
    else:
        table = metadata

    print(table)
    print(table.keys())

    root = minidom.Document()

    # EXPERIMENT_SET
    EXPERIMENT_SET = root.createElement('EXPERIMENT_SET')
    root.appendChild(EXPERIMENT_SET)

    submissions = Submission.objects.filter(
        content_type__model="assay",
        object_id__in=list(Assay.objects.filter(study__pk=study.pk)
                                .values_list("pk", flat=True)),
        db__name="ENA",
        status="in progress")

    submission_project = Submission.objects.get(
        content_type__model="study",
        object_id=str(study.pk),
        db__name="ENA")

    assayItems = list(submissions.values_list("object_id", flat=True))

    for f in DataFile.objects.filter(assay__in=assayItems):
        sub_external_id = (submissions.get(object_id=str(f.assay.id))
                           .external_id)
        sample_accession = sub_external_id['SAMPLE']

        # EXPERIMENT_SET > EXPERIMENT
        EXPERIMENT = root.createElement('EXPERIMENT')
        EXPERIMENT.setAttribute('alias', f.assay.alias)
        if f.assay.center_name:
            EXPERIMENT.setAttribute('center_name',
                                    f.assay.center_name.name)
        EXPERIMENT_SET.appendChild(EXPERIMENT)

        # EXPERIMENT_SET > EXPERIMENT > TITLE
        TITLE = root.createElement('TITLE')
        TITLE.appendChild(root.createTextNode(f.assay.title))
        EXPERIMENT.appendChild(TITLE)

        # EXPERIMENT_SET > EXPERIMENT > STUDY_REF
        STUDY_REF = root.createElement('STUDY_REF')
        STUDY_REF.setAttribute('accession',
                               submission_project.external_id['PROJECT'])
        EXPERIMENT.appendChild(STUDY_REF)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN
        DESIGN = root.createElement('DESIGN')
        EXPERIMENT.appendChild(DESIGN)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > DESIGN_DESCRIPTION
        DESIGN_DESCRIPTION = root.createElement('DESIGN_DESCRIPTION')
        DESIGN.appendChild(DESIGN_DESCRIPTION)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > SAMPLE_DESCRIPTOR
        SAMPLE_DESCRIPTOR = root.createElement('SAMPLE_DESCRIPTOR')
        SAMPLE_DESCRIPTOR.setAttribute('accession', sample_accession)
        DESIGN.appendChild(SAMPLE_DESCRIPTOR)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR
        LIBRARY_DESCRIPTOR = root.createElement('LIBRARY_DESCRIPTOR')
        DESIGN.appendChild(LIBRARY_DESCRIPTOR)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
        # LIBRARY_NAME
        LIBRARY_NAME = root.createElement('LIBRARY_NAME')
        LIBRARY_DESCRIPTOR.appendChild(LIBRARY_NAME)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
        # LIBRARY_STRATEGY
        LIBRARY_STRATEGY = root.createElement('LIBRARY_STRATEGY')
        LIBRARY_STRATEGY.appendChild(
            root.createTextNode(table[str(f.assay.pk)]['LIBRARY_STRATEGY']))
        LIBRARY_DESCRIPTOR.appendChild(LIBRARY_STRATEGY)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
        # LIBRARY_SOURCE
        LIBRARY_SOURCE = root.createElement('LIBRARY_SOURCE')
        LIBRARY_SOURCE.appendChild(
            root.createTextNode(table[str(f.assay.pk)]['LIBRARY_SOURCE']))
        LIBRARY_DESCRIPTOR.appendChild(LIBRARY_SOURCE)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
        # LIBRARY_SELECTION
        LIBRARY_SELECTION = root.createElement('LIBRARY_SELECTION')
        LIBRARY_SELECTION.appendChild(
            root.createTextNode(table[str(f.assay.pk)]['LIBRARY_SELECTION']))
        LIBRARY_DESCRIPTOR.appendChild(LIBRARY_SELECTION)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
        # LIBRARY_LAYOUT
        LIBRARY_LAYOUT = root.createElement('LIBRARY_LAYOUT')
        LIBRARY_DESCRIPTOR.appendChild(LIBRARY_LAYOUT)

        # EXPERIMENT_SET > EXPERIMENT > DESIGN > LIBRARY_DESCRIPTOR >
        # LIBRARY_LAYOUT > PAIRED

        if table[str(f.assay.pk)]['LIBRARY_LAYOUT'] == "paired":
            LLI = root.createElement('PAIRED')
            if table[str(f.assay.pk)]['NOMINAL_LENGTH'] is not None:
                LLI.setAttribute('NOMINAL_LENGTH',
                                 table[str(f.assay.pk)]['NOMINAL_LENGTH'])
            if table[str(f.assay.pk)]['NOMINAL_SDEV'] is not None:
                LLI.setAttribute('NOMINAL_SDEV',
                                 table[str(f.assay.pk)]['NOMINAL_SDEV'])
        elif table[str(f.assay.pk)]['LIBRARY_LAYOUT'] == "single":
            LLI = root.createElement('SINGLE')

        LIBRARY_LAYOUT.appendChild(LLI)

        if table[str(f.assay.pk)]['LIBRARY_CONSTRUCTION_PROTOCOL']:
            LIBRARY_CONSTRUCTION_PROTOCOL = \
                root.createElement('LIBRARY_CONSTRUCTION_PROTOCOL')
            LIBRARY_CONSTRUCTION_PROTOCOL.appendChild(
                root.createTextNode(
                    table[str(f.assay.pk)]['LIBRARY_CONSTRUCTION_PROTOCOL']))
            LIBRARY_DESCRIPTOR.appendChild(LIBRARY_CONSTRUCTION_PROTOCOL)

        # EXPERIMENT_SET > EXPERIMENT > PLATFORM
        PLATFORM = root.createElement('PLATFORM')
        EXPERIMENT.appendChild(PLATFORM)

        # EXPERIMENT_SET > EXPERIMENT > PLATFORM > INSTRUMENT
        # bad name it's to illustrate
        INSTRUMENT = root.createElement(table[str(f.assay.pk)]['PLATFORM'])
        PLATFORM.appendChild(INSTRUMENT)

        # EXPERIMENT_SET > EXPERIMENT > PLATFORM > INSTRUMENT >
        # INSTRUMENT_MODEL
        INSTRUMENT_MODEL = root.createElement('INSTRUMENT_MODEL')
        INSTRUMENT_MODEL.appendChild(
            root.createTextNode(table[str(f.assay.pk)]['INSTRUMENT_MODEL']))
        INSTRUMENT.appendChild(INSTRUMENT_MODEL)

        if f.assay.additional_annotation:
            # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES
            EXPERIMENT_ATTRIBUTES = root.createElement(
                'EXPERIMENT_ATTRIBUTES')
            EXPERIMENT.appendChild(EXPERIMENT_ATTRIBUTES)

            # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES >
            # EXPERIMENT_ATTRIBUTE
            EXPERIMENT_ATTRIBUTE = root.createElement(
                'EXPERIMENT_ATTRIBUTE')
            EXPERIMENT_ATTRIBUTES.appendChild(EXPERIMENT_ATTRIBUTE)

            # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES >
            # EXPERIMENT_ATTRIBUTE > TAG
            TAG = root.createElement('TAG')
            TAG.appendChild(root.createTextNode("TAG"))
            EXPERIMENT_ATTRIBUTE.appendChild(TAG)

            # EXPERIMENT_SET > EXPERIMENT > EXPERIMENT_ATTRIBUTES >
            # EXPERIMENT_ATTRIBUTE > VALUE
            VALUE = root.createElement('VALUE')
            VALUE.appendChild(root.createTextNode("VALUE"))
            EXPERIMENT_ATTRIBUTE.appendChild(VALUE)

    return root.toprettyxml(indent="\t", encoding="UTF-8")


def generate_run_file(study):
    """
    Generate run file for ENA

    :param Run object

    :return: XML (str format)
    """

    root = minidom.Document()

    # PROJECT_SET
    RUN_SET = root.createElement('RUN_SET')
    root.appendChild(RUN_SET)

    submissions = Submission.objects.filter(
        content_type__model="assay",
        object_id__in=list(Assay.objects.filter(study__pk=study.pk)
                           .values_list("pk", flat=True)),
        db__name="ENA",
        status="in progress")
    assayItems = list(submissions.values_list("object_id", flat=True))

    for f in DataFile.objects.filter(assay__in=assayItems):
        # RUN_SET > RUN
        RUN = root.createElement('RUN')
        RUN.setAttribute('alias', f.alias)
        if f.center_name:
            RUN.setAttribute('center_name', f.center_name.name)
        RUN_SET.appendChild(RUN)

        # RUN_SET > RUN > EXPERIMENT_REF
        EXPERIMENT_REF = root.createElement('EXPERIMENT_REF')
        EXPERIMENT_REF.setAttribute('refname', f.assay.alias)
        RUN.appendChild(EXPERIMENT_REF)

        # RUN_SET > RUN > DATA_BLOCK
        DATA_BLOCK = root.createElement('DATA_BLOCK')
        RUN.appendChild(DATA_BLOCK)

        # RUN_SET > RUN > DATA_BLOCK > FILES
        FILES = root.createElement('FILES')
        DATA_BLOCK.appendChild(FILES)

        # RUN_SET > RUN > DATA_BLOCK > FILES > FILES
        FILE = root.createElement('FILE')
        FILE.setAttribute('filename', os.path.basename(f.name))
        FILE.setAttribute('filetype', f.type.name)
        FILE.setAttribute('checksum_method', f.checksum_method.name)
        FILE.setAttribute('checksum', f.checksum)
        FILES.appendChild(FILE)

    return root.toprettyxml(indent="\t", encoding="UTF-8")

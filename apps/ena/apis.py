import json

import pandas as pd
from tempfile import TemporaryFile
from xml.dom import minidom
from django.db.models.functions import Now
from django.contrib.auth.models import User

from django.http import HttpResponse
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter, \
    OpenApiResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.exceptions import ParseError

from apps.data_brokering.models import Submission, SubmissionMessage
from apps.ena._functions import generate_project_file, \
    generate_submission_file, generate_sample_file, generate_experiment_file, \
    generate_run_file
from apps.isa.models import Assay, DataFile, Study
from apps.metadata.apis import check_completness
from apps.metadata.models import Metadata, Database, Field
from apps.users.apis import IsSuperUser
import requests
from requests.auth import HTTPBasicAuth
from xml.dom.minidom import parseString
from apps.ena.tasks import import_ena_project_async


@extend_schema(
    operation_id='import_ena_metadata',
    description="Import ENA metadata with a project ID and create "
                "an isa entry",
    tags=["ENA"],
    parameters=[
        OpenApiParameter(
            name='projectID',
            location=OpenApiParameter.PATH,
            description='ENA project ID',
            type=str
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['GET'])
@permission_classes([IsAuthenticated, IsSuperUser])
def import_ena_metadata(request, projectID):
    """SUMMARY LINE

    Import ENA metadata with a project ID and create
    an isa entry
    """

    response = requests.get(
        'https://www.ebi.ac.uk/ena/browser/api/xml/' + projectID +
        '?includeLinks=false')

    if response.ok:
        task = import_ena_project_async.delay(
            projectID,
            request.user.username
        )

        content = {'Message': 'Your request has been submitted',
                   'Task_id': task.id
                   }
        return Response(content, status=status.HTTP_200_OK)
    else:
        return HttpResponse(status=404)


@extend_schema(
    operation_id='ena_test_connection',
    description="List vault secret",
    tags=["ENA"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'Webin': {
                    'description': 'webin',
                    'type': 'str',
                    'required': 'true',
                },
                'Password': {
                    'description': 'Password',
                    'type': 'str',
                    'required': 'true',
                }
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def ena_test_connection(request):
    """SUMMARY LINE

    Set vault secret
    """
    if request.method == 'POST':
        if 'Webin' not in request.data:
            raise ParseError("Empty Webin")
        if 'Password' not in request.data:
            raise ParseError("Empty Password")

        with TemporaryFile() as tmp:
            root = minidom.Document()

            SUBMISSION = root.createElement('SUBMISSION')
            root.appendChild(SUBMISSION)

            ACTIONS = root.createElement('ACTIONS')
            SUBMISSION.appendChild(ACTIONS)

            ACTION = root.createElement('ACTION')
            ACTIONS.appendChild(ACTION)

            ADD = root.createElement('ADD')
            ACTION.appendChild(ADD)

            xml_str = root.toprettyxml(indent="\t", encoding="UTF-8")
            tmp.write(xml_str)
            tmp.seek(0)

            r = requests.post(
                'https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/',
                files={'SUBMISSION=submission_test.xml': tmp.read()},
                auth=HTTPBasicAuth(request.data["Webin"],
                                   request.data["Password"]))

            if r.ok:
                return HttpResponse(status=200)
            else:
                return HttpResponse("Bad Webin or password", status=403)


@extend_schema(
    operation_id='ena_send_submission',
    description="Send submission file",
    tags=["ENA"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'Webin': {
                    'description': 'webin',
                    'type': 'str',
                    'required': 'true',
                },
                'Password': {
                    'description': 'Password',
                    'type': 'str',
                    'required': 'true',
                },
                'pk': {
                    'description': 'PK of metadata',
                    'type': 'str',
                    'required': 'true',
                }
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def ena_send_project(request):
    """SUMMARY LINE

    Set vault secret
    """
    if request.method == 'POST':
        if 'Webin' not in request.data:
            raise ParseError("Empty Webin")
        if 'Password' not in request.data:
            raise ParseError("Empty Password")
        if 'pk' not in request.data:
            raise ParseError("Empty metadata pk (pk)")

        study = get_object_or_404(Study, pk=request.data["pk"])
        db = Database.objects.get(name="ENA")

        try:
            s = Submission.objects.get(
                content_type__model="study",
                object_id=str(study.pk),
                db=db)
            submissionFile = generate_submission_file('MODIFY')

        except Submission.DoesNotExist:
            s = Submission.objects.create(
                content_object=study,
                db=db,
                creation_date=Now(),
            )
            submissionFile = generate_submission_file('ADD')

        projectFile = generate_project_file(study)

        r = requests.post(
            'https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/',
            files=[
                ("SUBMISSION", ('submission.xml', submissionFile)),
                ("PROJECT", ('project.xml', projectFile)),
            ],
            auth=HTTPBasicAuth(request.data["Webin"],
                               request.data["Password"]))

        if r.ok:
            user_object = User.objects.get(username=request.user.username)

            s.status = 'submitted'
            s.submission_date = Now()
            s.assigned_to = user_object
            s.save()

            SubmissionMessage.objects.create(
                sub=s,
                date=Now(),
                message=r.text,
                message_by=user_object,
                message_type="submission_results"
            )

            temp = parseString(r.text)

            if (temp.getElementsByTagName("RECEIPT")[0]
                    .getAttribute('success').lower()) == 'false':
                return HttpResponse(r.text, status=403)
            dictTemp = dict()
            dictTemp['PROJECT'] = (
                    temp.getElementsByTagName("PROJECT")[0].getAttribute(
                        'accession'))
            s.external_id = dictTemp
            s.save()

            return HttpResponse(r.text, status=200)
        else:
            s.status = 'submitted'
            s.submission_date = Now()
            s.assigned_to = User.objects.get(username=request.user.username)
            s.external_id['PROJECT'] = parseString(r.text)
            s.save()

            return HttpResponse(r.text, status=403)


@extend_schema(
    operation_id='ena_send_samples',
    description="Send sample file",
    tags=["ENA"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'Webin': {
                    'description': 'webin',
                    'type': 'str',
                    'required': 'true',
                },
                'Password': {
                    'description': 'Password',
                    'type': 'str',
                    'required': 'true',
                },
                'pk': {
                    'description': 'PK of metadata',
                    'type': 'str',
                    'required': 'true',
                }
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def ena_send_samples(request):
    """SUMMARY LINE

    Set vault secret
    """
    if request.method == 'POST':
        if 'Webin' not in request.data:
            raise ParseError("Empty Webin")
        if 'Password' not in request.data:
            raise ParseError("Empty Password")
        if 'pk' not in request.data:
            raise ParseError("Empty metadata pk (pk)")

        m = get_object_or_404(Metadata, pk=request.data["pk"])

        if m.status == "to update":
            submissionFile = generate_submission_file('MODIFY')
        elif m.status == "to submit":
            submissionFile = generate_submission_file('ADD')
        else:
            raise ParseError("Error metadata status")

        sampleFile = generate_sample_file(m)

        r = requests.post(
            'https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/',
            files=[
                ("SUBMISSION", ('submission.xml', submissionFile)),
                ("SAMPLE", ('sample.xml', sampleFile)),
            ],
            auth=HTTPBasicAuth(request.data["Webin"],
                               request.data["Password"]))

        if r.ok:
            user_object = User.objects.get(username=request.user.username)
            db = Database.objects.get(name="ENA")

            try:
                s = Submission.objects.get(
                    content_type__model="study",
                    object_id=str(m.study.pk),
                    db=db)

            except Submission.DoesNotExist:
                s = Submission.objects.create(
                    content_object=m.study,
                    db=db,
                    creation_date=Now(),
                )

            SubmissionMessage.objects.create(
                sub=s,
                date=Now(),
                message=r.text,
                message_by=user_object,
                message_type="submission_results"
            )

            temp = parseString(r.text)

            if (temp.getElementsByTagName("RECEIPT")[0]
                    .getAttribute('success').lower()) == 'false':
                return HttpResponse(r.text, status=403)

            if isinstance(m.table, str):
                table = json.loads(m.table)
            else:
                table = m.table

            if isinstance(m.table, str):
                table = json.loads(m.table)
            else:
                table = m.table

            try:
                m_submitted = Metadata.objects.get(
                    study=m.study, checklist__database=db, status="submitted")
            except Metadata.DoesNotExist:
                m_submitted = Metadata.objects.create(
                    checklist=m.checklist,
                    study=m.study,
                    created_by=request.user.username,
                    last_modification_by=request.user.username,
                    status="submitted"
                )

            for s_xml in temp.getElementsByTagName("SAMPLE"):
                a = pd.DataFrame(table.values())
                assayID = list(table.keys())[
                    a.index[a['alias'] == s_xml.getAttribute(
                        'alias')].tolist()[0]]

                sub = Submission.objects.get(
                    content_type__model="assay",
                    object_id=str(assayID),
                    db=db,
                )
                sub.submission_date = Now()
                sub.last_update_date = Now()
                sub.status = 'in progress'
                dictTemp = dict()
                dictTemp['SAMPLE'] = s_xml.getAttribute('accession')
                sub.external_id = dictTemp
                sub.save()

                if m_submitted.table is None or m_submitted.table == "":
                    m_submitted.table = dict()

                if isinstance(m_submitted.table, str):
                    temp = json.loads(m_submitted.table)
                else:
                    temp = m_submitted.table

                temp[str(assayID)] = table[str(assayID)]
                m_submitted.table = temp

                fields = Field.objects.filter(
                    checklist=m_submitted.checklist)
                if m.checklist.database.name == "ENA":
                    fields = fields | Field.objects.filter(
                        checklist__name="MIP_ENA")

                check_result = check_completness(temp, fields)
                m_submitted.mandatory = check_result['mandatory']
                m_submitted.recommended = check_result['recommended']
                m_submitted.optional = check_result['optional']
                m_submitted.save()

                del table[str(assayID)]
                m.table = table
                check_result = check_completness(table, fields)
                m.mandatory = check_result['mandatory']
                m.recommended = check_result['recommended']
                m.optional = check_result['optional']
                m.save()

            return HttpResponse(r.text, status=200)
        else:
            return HttpResponse(r.text, status=403)


@extend_schema(
    operation_id='ena_send_runs_experiments',
    description="Send run and experiment file",
    tags=["ENA"],
    request={
        'multipart/form-data': {
            'type': 'object',
            'properties': {
                'Webin': {
                    'description': 'webin',
                    'type': 'str',
                    'required': 'true',
                },
                'Password': {
                    'description': 'Password',
                    'type': 'str',
                    'required': 'true',
                },
                'pk': {
                    'description': 'PK of metadata',
                    'type': 'str',
                    'required': 'true',
                }
            },
        },
    },
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='data'),
        404: OpenApiResponse(description='Not found.'),
    },
)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def ena_send_runs_experiments(request):
    """SUMMARY LINE

    Send run and experiment file
    """
    if request.method == 'POST':
        if 'Webin' not in request.data:
            raise ParseError("Empty Webin")
        if 'Password' not in request.data:
            raise ParseError("Empty Password")
        if 'pk' not in request.data:
            raise ParseError("Empty metadata pk (pk)")

        m = get_object_or_404(Metadata, pk=request.data["pk"])

        if m.status == "to update":
            submissionFile = generate_submission_file('MODIFY')
        elif m.status == "to submit":
            submissionFile = generate_submission_file('ADD')
        else:
            raise ParseError("Error metadata status")

        metadata = get_object_or_404(Metadata, study=m.study,
                                     status="submitted")
        get_object_or_404(Submission,
                          content_type__model="study",
                          object_id=str(m.study.pk),
                          db__name="ENA")

        experimentFile = generate_experiment_file(m.study, metadata.table)
        runFile = generate_run_file(m.study)

        if experimentFile is None:
            return HttpResponse(
                "SAMPLE id not available. Check previous step.", status=400)

        r = requests.post(
            'https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/',
            files=[
                ("SUBMISSION", ('submission.xml', submissionFile)),
                ("EXPERIMENT", ('experiment.xml', experimentFile)),
                ("RUN", ('run.xml', runFile)),
            ],
            auth=HTTPBasicAuth(request.data["Webin"],
                               request.data["Password"]))

        if r.ok:
            user_object = User.objects.get(username=request.user.username)
            db = Database.objects.get(name="ENA")

            temp = parseString(r.text)

            if (temp.getElementsByTagName("RECEIPT")[0]
                    .getAttribute('success').lower()) == 'false':
                return HttpResponse(r.text, status=403)

            assayItems = Assay.objects.filter(study=m.study
                                              ).values("id", "alias")
            a = pd.DataFrame(assayItems)

            for e_xml in temp.getElementsByTagName("EXPERIMENT"):
                s = Submission.objects.get(
                    content_type__model="assay",
                    object_id=str(a.loc[
                        a['alias'] == str(e_xml.getAttribute('alias'))
                    ].iloc[0]['id']),
                    db=db)

                s.last_update_date = Now()
                s.status = 'submitted'
                s.external_id['EXPERIMENT'] = e_xml.getAttribute('accession')
                s.save()

                SubmissionMessage.objects.create(
                    sub=s,
                    date=Now(),
                    message=str(e_xml),
                    message_by=user_object,
                    message_type="submission_results"
                )

            dfItems = DataFile.objects.filter(assay__study=m.study
                                              ).values("id", "alias",
                                                       "assay__id")
            a = pd.DataFrame(dfItems)

            for r_xml in temp.getElementsByTagName("RUN"):
                dfID = str(a.loc[a['alias'] == str(
                    r_xml.getAttribute('alias'))].iloc[0]['id'])

                tempDict = dict()
                tempDict['RUN'] = r_xml.getAttribute('accession')
                Submission.objects.create(
                    content_object=DataFile.objects.get(pk=dfID),
                    db=db,
                    submission_date=Now(),
                    last_update_date=Now(),
                    status='submitted',
                    external_id=tempDict
                )

                SubmissionMessage.objects.create(
                    sub=s,
                    date=Now(),
                    message=str(r_xml),
                    message_by=user_object,
                    message_type="submission_results"
                )

            return HttpResponse(r.text, status=200)
        else:
            return HttpResponse(r.text, status=403)

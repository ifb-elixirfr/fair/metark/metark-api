from django.urls import path

from apps.ena.apis import import_ena_metadata, ena_test_connection, \
    ena_send_project, ena_send_samples, ena_send_runs_experiments

urlpatterns = [

    # =========================================================================
    # Import
    # =========================================================================

    path('api/ena/import/project/<str:projectID>/',
         import_ena_metadata,
         name="import_ena_metadata"),

    path('api/ena/check/connection/',
         ena_test_connection,
         name="ena_test_connection"),

    path('api/ena/send/project/',
         ena_send_project,
         name="ena_send_project"),

    path('api/ena/send/samples/',
         ena_send_samples,
         name="ena_send_samples"),

    path('api/ena/send/runs/experiments/',
         ena_send_runs_experiments,
         name="ena_send_runs_experiments"),
]

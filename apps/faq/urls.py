from django.urls import path

from apps.faq.apis import QuestionList, QuestionDetail, CategoryDetail

urlpatterns = [

    path('api/questions/by/categories/', QuestionList.as_view(),
         name="question-list"),

    path('api/question/detail/<int:pk>/',
         QuestionDetail.as_view(
            {'get': 'retrieve',
             'delete': 'destroy',
             'put': 'update'}
         ),
         name="question-detail-vs"),
    path('api/question/create/',
         QuestionDetail.as_view(
            {'post': 'create'}
         ),
         name="question-detail-vs"),

    path('api/question/list/',
         QuestionDetail.as_view(
            {'get': 'list'}
         ),
         name="question-detail-vs"),

    # Category
    path('api/category/list/',
         CategoryDetail.as_view(
            {'get': 'list'}
         ),
         name="category-detail"),
    path('api/category/create/',
         CategoryDetail.as_view(
            {'post': 'create'}
         ),
         name="category-detail"),
]

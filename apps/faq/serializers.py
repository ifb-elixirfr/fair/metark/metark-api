from rest_framework.serializers import ModelSerializer
from rest_framework import serializers

from apps.faq.models import question, category


class CategorySerializer(ModelSerializer):
    class Meta:
        model = category
        fields = ['id', 'name', 'position']


class CategoryQuestionsSerializer(ModelSerializer):
    questions = serializers.SerializerMethodField()

    class Meta:
        model = category
        fields = ['id', 'name', 'position', 'questions']

    def get_questions(self, obj):
        selected_options = obj.question_set.all()
        return QuestionSerializer(selected_options, many=True).data


class QuestionSerializer(ModelSerializer):
    class Meta:
        model = question
        fields = ["id",
                  "question",
                  "response",
                  "position",
                  "category"]

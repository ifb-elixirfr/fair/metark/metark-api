from django.contrib import admin

from apps.faq.models import category, question


@admin.register(category)
class categoryAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('name', 'position')
    ordering = ('position',)

    class Meta:
        verbose_name_plural = "Categories"
        ordering = ['position']


@admin.register(question)
class questionAdmin(admin.ModelAdmin):
    save_on_top = True

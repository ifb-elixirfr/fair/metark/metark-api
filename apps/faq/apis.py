from django.http import JsonResponse, HttpResponse
from drf_spectacular.utils import extend_schema
from rest_framework import status, pagination
from rest_framework.generics import get_object_or_404

from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.faq.models import question, category
from apps.faq.serializers import CategoryQuestionsSerializer, \
    QuestionSerializer, CategorySerializer


###############################################################################
# Pagination
###############################################################################

class CustomPagination(pagination.PageNumberPagination):
    page_size = 3
    page_size_query_param = 'page_size'
    max_page_size = 50
    page_query_param = 'p'

###############################################################################
# API
###############################################################################


@extend_schema(
    operation_id="api_QuestionList",
    description="Get question list",
    tags=["Question"],
)
class QuestionList(APIView):
    def get(self, *args, **kwargs):
        categories = category.objects.filter(
            question__isnull=False).distinct().order_by('position')
        serializer = CategoryQuestionsSerializer(categories, many=True)
        return Response(serializer.data)


@extend_schema(
    operation_id="api_category_detail",
    description="Manage question detail",
    tags=["Question"],
)
class CategoryDetail(ModelViewSet):
    pagination_class = None
    serializer_class = CategorySerializer
    queryset = category.objects.all()

    def list(self, request):
        serializer = CategorySerializer(self.queryset.all(), many=True)
        return Response(serializer.data)

    def create(self, request):
        data = {
            'name': request.data.get('name'),
            'position': request.data.get('position')
        }
        serializer = CategorySerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@extend_schema(
    operation_id="api_question_detail",
    description="Manage question detail",
    tags=["Question"],
)
class QuestionDetail(ModelViewSet):
    serializer_class = QuestionSerializer
    queryset = question.objects.all()

    def list(self, request):
        serializer = QuestionSerializer(self.queryset.all(), many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        item = get_object_or_404(self.queryset, pk=pk)
        serializer = QuestionSerializer(item)
        return Response(serializer.data)

    def create(self, request):
        data = {
            'question': request.data.get('question'),
            'response': request.data.get('response'),
            'position': request.data.get('position'),
            'category': request.data.get('category')
        }
        serializer = QuestionSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        q = get_object_or_404(question, pk=pk)
        data = JSONParser().parse(request)
        serializer = QuestionSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    def destroy(self, request, pk):
        q = get_object_or_404(question, pk=pk)
        q.delete()
        return HttpResponse(status=204)

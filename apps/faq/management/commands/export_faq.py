import csv
import json

from django.core.management.base import BaseCommand

from apps.faq.models import category, question


class Command(BaseCommand):
    help = 'Export FAQs'

    def handle(self, *args, **kwargs):
        cats = category.objects.all()
        with open('apps/faq/static/data/category.tsv', 'wt') as out_file:
            tsv_writer = csv.writer(out_file, delimiter='\t')
            tsv_writer.writerow(['name', 'position'])
            for cat in cats:
                tsv_writer.writerow([cat.name, cat.position])

        self.stdout.write("Categories : Done")

        questions = question.objects.all()
        with open('apps/faq/static/data/question.tsv', 'wt') as out_file:
            tsv_writer = csv.writer(out_file, delimiter='\t',
                                    quotechar="\'", quoting=csv.QUOTE_NONE)
            tsv_writer.writerow(['question', 'response', 'category',
                                 'position'])
            for q in questions:
                tsv_writer.writerow([q.question, json.dumps(q.response),
                                     q.category.name, q.position])

        self.stdout.write("Questions :Done")

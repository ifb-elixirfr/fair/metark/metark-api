from django.db import models


class category(models.Model):
    name = models.CharField(max_length=100, help_text="Category name",
                            unique=True,
                            null=False,
                            blank=False)
    position = models.IntegerField(help_text="Position in FAQ page", )

    def __str__(self):
        return "%s %s" % (self.name, self.position)


class question(models.Model):
    question = models.CharField(max_length=100, help_text="Question", )
    response = models.JSONField(help_text="Response",
                                blank=False,
                                null=False,
                                )
    category = models.ForeignKey(category, on_delete=models.CASCADE)
    position = models.IntegerField(help_text="Position in the category", )

    def __str__(self):
        return self.question

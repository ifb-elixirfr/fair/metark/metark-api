from django.urls import path

from apps.task.apis import TaskResultManagement

urlpatterns = [

    path("api/task/result/<str:task_id>/",
         TaskResultManagement.as_view({
             'get': 'retrieve',
             }),
         name="TaskResultManagement"),

    path("api/task/result/<str:task_id>/",
         TaskResultManagement.as_view({
             'get': 'retrieve',
             }),
         name="TaskResultManagement"),
]

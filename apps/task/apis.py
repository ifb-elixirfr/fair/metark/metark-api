from django_celery_results.models import TaskResult
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiParameter, \
    OpenApiResponse
from rest_framework.decorators import permission_classes
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.task.serialiazers import TaskResultSerializer


@extend_schema(
    operation_id='task_status',
    description='Get status of task selected by ID. If staus is succes, '
                'then api returns file.',
    tags=["Task"],
    parameters=[
        OpenApiParameter(
            name='task_id',
            location=OpenApiParameter.PATH,
            description='Task_ID',
        ),
    ],
    responses={
        200: OpenApiResponse(response=OpenApiTypes.OBJECT,
                             description='File'),
    },
)
@permission_classes([IsAuthenticated])
class TaskResultManagement(ModelViewSet):
    serializer_class = TaskResultSerializer
    queryset = TaskResult.objects.all()

    def retrieve(self, request, task_id):
        """SUMMARY LINE
            Get status of task selected by ID. If status is success,
            then api returns file.

            Args:
                task_ID (str): Task_ID

            Returns:
                Return status
            """
        item = get_object_or_404(TaskResult, task_id=task_id)
        serializer = TaskResultSerializer(item)
        return Response(serializer.data)

    def list(self, request):
        serializer = TaskResultSerializer(TaskResult.objects.all(), many=True)
        return Response(serializer.data)

from django_celery_results.models import TaskResult
from rest_framework.serializers import ModelSerializer


class TaskResultSerializer(ModelSerializer):
    class Meta:
        model = TaskResult
        fields = '__all__'

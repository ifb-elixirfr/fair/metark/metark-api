from django.utils import timezone

from django.db import models


class WebMessage(models.Model):
    MESSAGE_COLOR = (
        ("text-white bg-primary", "blue"),
        ("text-white bg-secondary", "grey"),
        ("text-white bg-success", "green"),
        ("text-white bg-danger", "red"),
        ("text-white bg-dark", "black"),
        ("text-dark bg-warning", "yellow"),
        ("text-dark bg-info", "light blue"),
        ("text-dark bg-light", "light grey"),
    )

    MESSAGE_CATEGORY = (
        ("information", "information"),
        ("warning", "warning"),
        ("success", "success"),
        ("danger", "danger"),
        ("news", "news"),
        ("other", "other"),
    )

    messageTitle = models.TextField(help_text="Message title to users")

    messageAbstract = models.TextField(
        help_text="Message abtract to users "
                  "(used in actuality area (home page))",
        default="",
    )

    messageAlert = models.TextField(
        help_text="Leave blank if you do not wish to see it in the alert area."
        " This text should be short.",
        null=True,
        blank=True,
    )

    messageContent = models.JSONField(
        help_text="Message content. This is the full text to be displayed "
                  "to users",
        blank=True,
        null=True,
    )

    messageColor = models.CharField(
        max_length=200,
        help_text="Message color",
        choices=MESSAGE_COLOR,
        default="text-dark bg-light",
    )

    messageIcon = models.CharField(
        max_length=500,
        help_text="Message icon from fontawesome v6 (free)",
        default='<i class="fa-solid fa-message"></i>',
    )

    messageCategory = models.CharField(
        max_length=200,
        help_text="Message category",
        choices=MESSAGE_CATEGORY,
        default="other",
    )

    messageDateCreation = models.DateTimeField(
        default=timezone.now, help_text="Message creation date"
    )

    messageDateArchive = models.DateTimeField(
        help_text="Message archive date", null=True, blank=True, default=None
    )

    messagePin = models.BooleanField(help_text="Pin the message",
                                     default=False)

from django.contrib import admin

from apps.web_message.models import WebMessage


@admin.register(WebMessage)
class WebMessageAdmin(admin.ModelAdmin):
    fs = [f.name for f in WebMessage._meta.fields]
    list_display = fs

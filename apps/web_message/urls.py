from django.urls import path

from apps.web_message.apis import WebMessageManagement

urlpatterns = [
    path('api/web/message/create/',
         WebMessageManagement.as_view({'post': 'create'}),
         name="WebMessageManagement"),

    path('api/web/message/list/',
         WebMessageManagement.as_view({'get': 'list'}),
         name="WebMessageManagement"),

    path("api/web/message/<int:pk>/",
         WebMessageManagement.as_view({
             'get': 'retrieve',
             'put': 'update',
             'delete': 'destroy'
             }),
         name="WebMessageManagement"),

    path("api/get/form/web/message/",
         WebMessageManagement.as_view({
             'get': 'get_form',
             }),
         name="WebMessageManagement"),
]

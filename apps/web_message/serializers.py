from rest_framework.serializers import ModelSerializer

from apps.web_message.models import WebMessage


class WebMessageSerializer(ModelSerializer):
    class Meta:
        model = WebMessage
        fields = '__all__'

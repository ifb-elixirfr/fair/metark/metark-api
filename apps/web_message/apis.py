from datetime import datetime
from django.http import JsonResponse, HttpResponse

from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiResponse

from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.isa._functions import form2api
from apps.users.apis import IsSuperUser
from apps.web_message.models import WebMessage
from apps.web_message.serializers import WebMessageSerializer


@extend_schema(
    operation_id="api_web_message_management",
    description="Web message manager",
    tags=["Web message"],
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT, description="Data in json format."
        ),
        404: OpenApiResponse(description="No results found."),
    },
)
# @permission_classes([IsAuthenticated])
class WebMessageManagement(ModelViewSet):
    serializer_class = WebMessageSerializer
    queryset = WebMessage.objects.all()
    search_fields = ['messageTitle']

    def get_permissions(self):
        if self.action in ['destroy', 'create', 'update', 'get_form']:
            return [IsAuthenticated(), IsSuperUser()]
        return super(WebMessageManagement, self).get_permissions()

    def get_form(self, request):
        return JsonResponse(form2api(WebMessageSerializer()),
                            safe=False, status=200)

    def retrieve(self, request, pk):
        q = get_object_or_404(WebMessage, pk=pk)
        serializer = WebMessageSerializer(q)
        return Response(serializer.data)

    def destroy(self, request, pk):
        q = get_object_or_404(WebMessage, pk=pk)
        q.delete()
        return HttpResponse(status=204)

    def update(self, request, pk):
        q = get_object_or_404(WebMessage, pk=pk)
        data = JSONParser().parse(request)
        print(data)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = WebMessageSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            print(data)
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    def create(self, request):
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        data['messageDateCreation'] = datetime.now()
        serializer = WebMessageSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)

        return JsonResponse(serializer.errors, status=400)

from django.db import models


class PageContent(models.Model):
    PAGES = (
        ("about", "about"),
    )

    pageName = models.TextField(help_text="Page name",
                                choices=PAGES, unique=True)

    title = models.CharField(max_length=250, help_text="Page title")

    subtitle = models.CharField(max_length=250, help_text="Page subtitle")

    content = models.JSONField(
        help_text="Page content",
        blank=True,
        null=True,
    )

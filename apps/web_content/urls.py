from django.conf import settings
from django.urls import path, re_path
from django.views.static import serve

from apps.web_content.apis import PageContentManagement

urlpatterns = [
    re_path(r'^sphynx/documentation/(?P<path>.*)$', serve,
            {'document_root': settings.DOCS_ROOT}, name="docs"),

    path('api/area/content/create/',
         PageContentManagement.as_view({'post': 'create'}),
         name="PageContentManagement"),

    path("api/area/content/<str:areaName>/",
         PageContentManagement.as_view({
             'get': 'retrieve',
             'put': 'update',
             'delete': 'destroy'
             }),
         name="PageContentManagement"),


]

# Generated by Django 4.1.5 on 2023-03-28 16:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="PageContent",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "pageName",
                    models.TextField(
                        choices=[("about", "about")], help_text="Page name", unique=True
                    ),
                ),
                ("title", models.CharField(help_text="Page title", max_length=250)),
                (
                    "subtitle",
                    models.CharField(help_text="Page subtitle", max_length=250),
                ),
                (
                    "content",
                    models.JSONField(blank=True, help_text="Page content", null=True),
                ),
            ],
        ),
    ]

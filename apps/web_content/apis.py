from django.http import JsonResponse, HttpResponse

from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema, OpenApiResponse, \
    OpenApiParameter

from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.web_content.models import PageContent
from apps.web_content.serializers import PageContentSerializer


@extend_schema(
    operation_id="api_get_area_content",
    description="Get area content",
    parameters=[
        OpenApiParameter(
            name="areaName",
            location=OpenApiParameter.PATH,
            description="Area name",
            enum=[x[0] for x in PageContent.PAGES],
        ),
    ],
    tags=["Web content"],
    responses={
        200: OpenApiResponse(
            response=OpenApiTypes.OBJECT, description="Data in json format."
        ),
        404: OpenApiResponse(description="No results found."),
    },
)
@extend_schema(
    operation_id="api_protocol_detail",
    description="Manage protocol",
    tags=["Study"],
)
# @permission_classes([IsAuthenticated, IsSuperUser])
class PageContentManagement(ModelViewSet):
    serializer_class = PageContentSerializer
    queryset = PageContent.objects.all()

    def retrieve(self, request, areaName):
        q = get_object_or_404(PageContent, pageName=areaName)
        serializer = PageContentSerializer(q)
        return Response(serializer.data)

    def destroy(self, request, areaName):
        q = get_object_or_404(PageContent, pageName=areaName)
        q.delete()
        return HttpResponse(status=204)

    def update(self, request, areaName):
        q = get_object_or_404(PageContent, pageName=areaName)
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PageContentSerializer(q, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    def create(self, request):
        data = JSONParser().parse(request)
        if 'headers' in data:  # pragma: no cover
            del data['headers']
        serializer = PageContentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

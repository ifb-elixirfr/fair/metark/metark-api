from rest_framework.serializers import ModelSerializer
from apps.web_content.models import PageContent


class PageContentSerializer(ModelSerializer):
    class Meta:
        model = PageContent
        fields = '__all__'

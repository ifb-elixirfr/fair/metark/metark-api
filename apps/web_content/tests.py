import json

from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse


class TestWebAnalytic(TestCase):
    def setUp(self):
        self.client = Client()

        # Create a super user to add a new user
        User.objects.create_user(
            username='superUser',
            first_name='Super',
            last_name='User',
            email='superUser@mail.com',
            password='djangoPWD',
            is_superuser=True,
            is_staff=True
        )
        logged_in = self.client.login(username="superUser",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        # Create a new user
        url = reverse('RegisterManagement')
        response = self.client.post(url, json.dumps({
            "username": "uproject",
            "password": "djangoPWD",
            "password2": "djangoPWD",
            "email": "userProject@mail.com",
            "first_name": "User",
            "last_name": "Project"
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        u = User.objects.get(username="uproject")
        u.is_superuser = True
        u.is_staff = True
        u.save()

        self.client.logout()

        # Login
        logged_in = self.client.login(username="uproject",
                                      password="djangoPWD")
        self.assertTrue(logged_in)

        url = reverse('token_obtain_pair')
        response = self.client.post(url, {'username': 'uproject',
                                          'password': 'djangoPWD'},
                                    format='json')
        self.assertEqual(response.status_code, 200)

        self.assertTrue('access' in response.data)
        tokenID = response.data['access']

        url = reverse('knox_login')
        response = self.client.post(url,
                                    HTTP_AUTHORIZATION='Token ' + tokenID)
        token = response.data['token']
        self.token = response.data['token']
        self.client = Client(HTTP_AUTHORIZATION='Token ' + token)

    def test_web_content(self):
        # Create
        url = reverse("PageContentManagement")

        response = self.client.post(url, json.dumps({
            'pageName': 'about'
        }), content_type="application/json")
        self.assertEqual(response.status_code, 400)

        response = self.client.post(url, json.dumps({
            'pageName': 'about',
            'title': 'title',
            'subtitle': 'subtitle'
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        # GET
        url = reverse("PageContentManagement",
                      kwargs={'areaName': 'about'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # UPDATE
        response = self.client.put(url, json.dumps({
            'pageName': 'about',
            'title': 'title 2',
            'subtitle': 'subtitle', 'content': json.dumps(
                {'json': 'object'})
        }), content_type="application/json")
        self.assertEqual(response.status_code, 200)

        response = self.client.put(url, json.dumps({
            'pageName': 'about'
        }), content_type="application/json")
        self.assertEqual(response.status_code, 400)

        # DELETE
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)

from django.contrib import admin

from apps.web_content.models import PageContent


@admin.register(PageContent)
class PageContentAdmin(admin.ModelAdmin):
    save_on_top = True

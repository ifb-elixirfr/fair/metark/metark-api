# metark-api

## Description

Metark is a data brokering tool for facilitating the submission of Life Science data to international databases.

## Overview

**Language**

[![Made with Django](https://img.shields.io/badge/Made%20with-Django-blue)](https://www.djangoproject.com/)
[![Made with Django REST framework](https://img.shields.io/badge/Made%20with-Django%20REST%20framework-blue)](https://www.django-rest-framework.org/)

**Continuous Integration**

- Flake8 : Tool For Style Guide Enforcement
- Coverage : test the code
- Build : Auto docker image build and send in Gitlab registry  
- Release : Auto release

**Questions**

[:speech_balloon: Ask a question](https://gitlab.com/ifb-elixirfr/fair/metark-api/-/issues/new)
[:book: Lisez les questions](https://gitlab.com/ifb-elixirfr/fair/metark-api/-/issues/)
[:e-mail: Par mail](mailto:support-metark@groupes.france-bioinformatique.fr)

**Contribution**

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

## Documentation

All the steps to deploy Metark are described in the accessible wiki: [here](https://gitlab.com/ifb-elixirfr/fair/metark-api/-/wikis/home).

## Visuals

*In coming*

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Citation
If you use Metakr project, please cite us :

IFB-ElixirFr, Metark, (2023), GitLab repository, https://gitlab.com/ifb-elixirfr/fair/metark

*DOI from Zenodo in coming*

## Contributors

### From IFB

* [Thomas Denecker](https://gitlab.com/thomasdenecker) <a itemprop="sameAs" content="https://orcid.org/0000-0003-1421-7641" href="https://orcid.org/0000-0003-1421-7641" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>

## Acknowledgement

A big thank you to the platforms that beta tested the application. 

## Contributing

Please, see the [CONTRIBUTING](CONTRIBUTING.md) file.

## Contributor Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](https://www.contributor-covenant.org/). By participating in this project you agree to abide by its terms. See [CODE_OF_CONDUCT](code_of_conduct.md) file.

## License

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

Metark is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

## Project status

The project is under development 

## For developers

### Coverage 

```bash
docker-compose exec backend flake8 --exclude='venv/,*migrations*,gisaid_cli2/'  
docker-compose exec backend coverage run --omit='manage.py' manage.py test apps/
docker-compose exec backend coverage html  --omit='/home/app/.local/lib/python3.10/site-packages/requests/*'
```

### Code

```bash
docker-compose exec backend flake8 --exclude='venv/,*migrations*,gisaid_cli2/' 
```

### Import fake date

```bash
bash scripts/populate_exemple_test.sh 
```

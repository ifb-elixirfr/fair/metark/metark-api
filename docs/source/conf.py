# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

import os
import sys
import django

sys.path.insert(0, os.path.abspath('../..'))

os.environ['SECRET_KEY'] = '6)2t^zp59du9$_tl8vd@5l!cw5#11a_a$qfu-^w2m#5nb*rm7d'
os.environ['DJANGO_ALLOWED_HOSTS'] = 'localhost 127.0.0.1 [::1]'
os.environ['DJANGO_SETTINGS_MODULE'] = 'metark_api.settings'
django.setup()

# -- Project information -----------------------------------------------------

project = 'Metark'
copyright = '2023, IFB'
author = 'IFB'

# The full version, including alpha/beta/rc tags
release = 'alpha'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

extensions = ['sphinx.ext.autodoc', 'sphinx.ext.coverage',
              'sphinx.ext.napoleon']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.

language = 'fr'

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_sidebars = {
    '**': [
        "sidebar/brand.html",
        "sidebar/search.html",
        "sidebar/back.html",
        "sidebar/scroll-start.html",
        "sidebar/navigation.html",
        "sidebar/ethical-ads.html",
        "sidebar/scroll-end.html",
        "sidebar/social.html",
    ]
}

html_context = {'website': 'https://gitlab.com/ifb-elixirfr/fair/metark'}

html_favicon = "_static/img/logo.png"

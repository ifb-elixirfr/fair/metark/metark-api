*******************************************************************************
Application ISA
*******************************************************************************

The Metark API is built from the Django REST framework whose official
website  is `available here <https://www.django-rest-framework.org/>`_.


How to add an API?
===============================================================================

#. Step 1 - Create a serializer in the serializers.py
#. Step 2 - Create a view in :code:`view.py`
#. Step 3 - Create a url to access the API in the :code:`ulrs.py` ;
#. Step 4 - Test the API in the :code:`test.py` file. Many examples are already available in the file.


A complete example is available on the `official documentation <https://www.django-rest-framework.org/tutorial/1-serialization/>`_
and APIs are already available in the application. They can help you in the structure
to follow.

Key files of the "ISA" application
===============================================================================

api.py
-------------------------------------------------------------------------------

.. automodule:: apps.isa.apis
    :members:


serializers.py
-------------------------------------------------------------------------------


The Metark API is built from the Django REST framework. To format the results
returned by the API, serializers are created whose official documentation is
`here <https://www.django-rest-framework.org/tutorial/1-serialization/>`_ :


.. automodule:: apps.isa.serializers
    :members:


test.py
-------------------------------------------------------------------------------

.. automodule:: apps.isa.tests
    :members:

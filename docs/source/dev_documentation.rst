*******************************************************************************
Gestion de la documentation
*******************************************************************************

La documentation a été écrite à l'aide de `Sphinx <https://www.sphinx-doc.org/en/master/>`_ .
Les pages de documentation sont écrites en :code:`rst` ( exemple de `cheatsheet <https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst>`_).

La page de configuration
===============================================================================

La page de configuration :code:`conf.py` dans le dossier :code:`docs/source/`
(depuis la racine du projet) contient les informations importantes du projet,
les méthodes de rendu, ...

La page d'index
===============================================================================

La page d'accueil de la documentation :code:`index.rst` se trouve dans le dossier :code:`docs/source/`.
Dans cette page, vous pourrez trouver le paramétrage de la table des matières et organiser vos pages.


Les pages de documentation
===============================================================================

La documentation est composée de 4 parties :

- La vue générale du projet dont les fichiers commencent par le préfixe :code:`overview`
- Le manuel de l'utilisateur dont les fichiers commencent par le préfixe :code:`user`
- Le manuel de l'administrateur dont les fichiers commencent par le préfixe  :code:`admin`
- Le manuel du développeur dont les fichiers commencent par le préfixe  :code:`dev`

Chaque page dispose d'un fichier rst (dont le nom est utilisé dans la table des matières du fichier :code:`index.rst`).


Générer les pages HTML
===============================================================================

.. warning::
    Docker doit être en train de tourner ainsi que l'application Metark

1 - PDF
-------------------------------------------------------------------------------

.. code-block:: bash

    docker-compose exec backend bash -c "cd /home/app/web/docs && make latexpdf && cp /home/app/web/docs/build/latex/metark.pdf /home/app/web/docs/source/_static/pdf"

2 - EPUB
-------------------------------------------------------------------------------

.. code-block:: bash

    docker-compose exec backend bash -c "cd /home/app/web/docs && make epub && cp /home/app/web/docs/build/epub/metark.epub /home/app/web/docs/source/_static/epub"


3- HTML
-------------------------------------------------------------------------------
.. code-block:: bash

    docker-compose exec backend bash -c "cd /home/app/web/docs && make html"


4- Un script
-------------------------------------------------------------------------------

Ces commandes sont lancées à chaque construction de l'image Docker. Vous pouvez
si vous le souhaitez les lancer lors du développement pour observer les changements.
Nous avons aussi créé un script qui exécute ces 3 commandes :

.. code-block:: bash

    bash generate_docs.sh

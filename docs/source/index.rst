*******************************************************************************
Welcome to the Metark documentation
*******************************************************************************

The application can be accessed by clicking on the following link:
`https://metark.france-bioinformatique.fr/ <https://metark.france-bioinformatique.fr/>`_ or by clicking
in the left-hand bar on the :code:`Back to Metark`.


.. note::
    If any information is missing or unclear, please do not hesitate to please do
    not hesitate to send us a feedback on the support address (support-metark@groupes.france).
    We thank you in advance and will try to improve it as soon as possible.

Navigation
===============================================================================

.. toctree::
    :maxdepth: 2
    :caption: Overview

    overview_objectifs
    overview_points_forts

.. toctree::
    :maxdepth: 2
    :caption: Developer's Manual

    dev_requirements
    dev_dev
    dev_django
    dev_code_quality.rst
    dev_isa
    dev_documentation



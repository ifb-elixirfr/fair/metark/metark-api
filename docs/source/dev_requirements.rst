*******************************************************************************
Requirements
*******************************************************************************

Docker
===============================================================================

The Metark application was developed using a Docker containerization approach
which improves the reproducibility of the analyses.
We use Docker to develop and manage Metark. We invite you to verify that
the following requirements are satisfied before trying to bootstrap
the application:

* `Docker 20.10.5+ <https://docs.docker.com/get-docker/>`_

    We recommend you to follow Docker's official documentations to install
    required docker tools (see links above).To help you, explanatory videos for each
    operating system are available `here <https://www.bretfisher.com/installdocker/>`_

**Docker must be on for the duration of Metark use.**

Docker-compose
===============================================================================

The Metark application is composed of several services running in separate docker
containers. To make them work together, we use Docker-compose. Below is the link to the
installation page proposed by Docker :

* `Docker-compose 1.29.0+ <https://docs.docker.com/compose/install/>`_

GitLab
===============================================================================

https://gitlab.com/ifb-elixirfr/fair/metark

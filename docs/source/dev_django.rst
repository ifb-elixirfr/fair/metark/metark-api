*******************************************************************************
Django
*******************************************************************************

.. warning::
    Lors du développement, Docker doit être actif et une instance de développement doit être active (voir section dédiée).


Create a new app
===============================================================================

.. code-block:: bash

    mkdir apps/new-app
    docker-compose exec web python manage.py startapp new-app apps/new-app

Add in `settings.py` file in `INSTALLED_APPS`.


Composition of a Django application
===============================================================================


A Django application is mainly composed:

- A :code:`view.py` file which will be the functions that will allow to send data to web pages
- Template files: which will receive data and display them. These files are in HTML format
- A :code:`urls.py` file which gathers all the addresses of the application to make the templates accessible from a web browser
- A :code:`tests.py` file to write all the unit and functional test functions
- A :code:`models.py` file which contains the definition of the database tables (which will be translated into SQL in our case)
- An :code:`admins.py` file to add a data table in the Django administration page

Make migrations in database
===============================================================================

.. code-block:: bash

    docker-compose exec web python manage.py makemigrations
    docker-compose exec web python manage.py migrate
*******************************************************************************
Code quality
*******************************************************************************

Overview
===============================================================================
Adding code to GitLab is subject to a series of code quality checks through continuous
integration. We recommend that you run these checks before submission. Please note that
the coverage rate is set at 100% and no errors at Flake8.

Coverage
===============================================================================
If docker-compose is running in test mode (:code:`docker-compose up -d --build`)

.. code-block:: bash

    docker-compose exec web coverage run --omit='manage.py' manage.py test apps/
    docker-compose exec web coverage html

Flake8
===============================================================================

If docker-compose is running

Note : In order not to fail at the time of continuous integration, no errors should be detected.

Documentation : `Flake8 <https://flake8.pycqa.org/en/latest/index.html>`_

.. code-block:: bash

    docker-compose exec web flake8 --exclude='venv/,*migrations*,gisaid_cli2/'
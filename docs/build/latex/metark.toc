\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Navigation}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Objectives}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Why Metark ?}{3}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}The proposed solution}{3}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}Strengths of the project}{3}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Quality controls}{3}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}API}{4}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Reproducibility}{4}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Open source technologies}{4}{subsection.1.2.4}%
\contentsline {section}{\numberline {1.3}Requirements}{4}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Docker}{4}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Docker\sphinxhyphen {}compose}{4}{subsection.1.3.2}%
\contentsline {subsection}{\numberline {1.3.3}GitLab}{4}{subsection.1.3.3}%
\contentsline {section}{\numberline {1.4}Django}{4}{section.1.4}%
\contentsline {subsection}{\numberline {1.4.1}Create a new app}{5}{subsection.1.4.1}%
\contentsline {subsection}{\numberline {1.4.2}Composition of a Django application}{5}{subsection.1.4.2}%
\contentsline {subsection}{\numberline {1.4.3}Make migrations in database}{5}{subsection.1.4.3}%
\contentsline {section}{\numberline {1.5}Code quality}{5}{section.1.5}%
\contentsline {subsection}{\numberline {1.5.1}Overview}{5}{subsection.1.5.1}%
\contentsline {subsection}{\numberline {1.5.2}Coverage}{5}{subsection.1.5.2}%
\contentsline {subsection}{\numberline {1.5.3}Flake8}{6}{subsection.1.5.3}%
\contentsline {section}{\numberline {1.6}Application ISA}{6}{section.1.6}%
\contentsline {subsection}{\numberline {1.6.1}How to add an API?}{6}{subsection.1.6.1}%
\contentsline {subsection}{\numberline {1.6.2}Key files of the « ISA » application}{6}{subsection.1.6.2}%
\contentsline {subsubsection}{api.py}{6}{subsubsection*.3}%
\contentsline {subsubsection}{serializers.py}{7}{subsubsection*.33}%
\contentsline {subsubsection}{test.py}{8}{subsubsection*.54}%
\contentsline {section}{\numberline {1.7}Gestion de la documentation}{8}{section.1.7}%
\contentsline {subsection}{\numberline {1.7.1}La page de configuration}{8}{subsection.1.7.1}%
\contentsline {subsection}{\numberline {1.7.2}La page d’index}{8}{subsection.1.7.2}%
\contentsline {subsection}{\numberline {1.7.3}Les pages de documentation}{9}{subsection.1.7.3}%
\contentsline {subsection}{\numberline {1.7.4}Générer les pages HTML}{9}{subsection.1.7.4}%
\contentsline {subsubsection}{1 \sphinxhyphen {} PDF}{9}{subsubsection*.55}%
\contentsline {subsubsection}{2 \sphinxhyphen {} EPUB}{9}{subsubsection*.56}%
\contentsline {subsubsection}{3\sphinxhyphen {} HTML}{9}{subsubsection*.57}%
\contentsline {subsubsection}{4\sphinxhyphen {} Un script}{9}{subsubsection*.58}%
\contentsline {chapter}{Index des modules Python}{11}{section*.59}%
\contentsline {chapter}{Index}{13}{section*.60}%
